#################################
PSS.LMC Deployment Configuration
#################################

The Helm template defines the PSS devices deployment within the SKA-PSS-LMC system. The configuration dynamically adjusts based on the telescope instance, the number of subarrays, and the number of PSS beams (pipelines).

Parameterization
****************

Telescope and PSS Instance Configuration
========================================

.. code-block:: yaml

   {{- define "ska-pss-lmc.telescopeVars" -}}
     {{- $telescope := .Values.telescope | default "SKA-mid" | lower }}
     {{- if not (or (eq $telescope "ska-mid") (eq $telescope "ska-low")) }}
       {{- $telescope = "ska-mid" }}
     {{- end }}
     {{- $pss := printf "%s-pss" (index (split "-" $telescope) "_1") }}
     {{- printf "{\"telescope\": \"%s\", \"pss\": \"%s\"}" $telescope $pss }}
   {{- end }}


- The telescope name is derived from ``.Values.telescope`` and converted to lowercase.
- If the telescope name is not ``ska-mid`` or ``ska-low``, it defaults to ``ska-mid``.
- The PSS instance name (``$pss``) is generated by extracting the first part of the telescope name and appending ``-pss``:

  - ``ska-mid`` → ``mid-pss``
  - ``ska-low`` → ``low-pss``

- This information is formatted as a JSON string, which is later parsed in the main template.

Subarrays Configuration (numSubarrays)
======================================

.. code-block:: text

   - name: "PssSubarrays"
     values:
       {{- range $i, $e := until ((.Values.numSubarrays | default 1) | int) }}
         - "{{$pss}}/subarray/{{ printf "%02d" (add $i 1) }}"
       {{- end }}

- The number of subarrays is controlled by ``.Values.numSubarrays``.
- If not explicitly set, it defaults to 1.
- The loop (``range``) generates subarray names in the format:

  - ``mid-pss/subarray/01``
  - ``mid-pss/subarray/02``
  - etc., up to ``numSubarrays``.

Pipeline Configuration (numPssBeams)
====================================

.. code-block:: text

   - name: "PipelineFqdns"
     values:
       {{- range $i, $e := until ((.Values.numPssBeams | default 1) | int)}}
         - "{{$pss}}/pipeline/{{printf "%04d" (add $i 1) }}"
       {{- end }}

- The number of pipelines is controlled by ``.Values.numPssBeams``.
- If not explicitly set, it defaults to 1.
- The loop (``range``) generates pipeline names in the format:

  - ``mid-pss/pipeline/0001``
  - ``mid-pss/pipeline/0002``
  - etc., up to ``numPssBeams``.

Image Configuration
===================

.. code-block:: yaml

   image:
     registry: "{{ .Values.ska_pss_lmc.image.registry }}"
     image: "{{ .Values.ska_pss_lmc.image.image }}"
     tag: "{{ .Values.ska_pss_lmc.image.tag }}"
     pullPolicy: "{{ .Values.ska_pss_lmc.image.pullPolicy }}"

- The Docker image for the PSS Controller is dynamically configured using Helm values:

  - **Registry**: ``.Values.ska_pss_lmc.image.registry``
  - **Image name**: ``.Values.ska_pss_lmc.image.image``
  - **Tag**: ``.Values.ska_pss_lmc.image.tag``
  - **Pull policy**: ``.Values.ska_pss_lmc.image.pullPolicy``

Configuration: Where to Set These Values
========================================

The values that control the number of subarrays, pipelines, and the telescope configuration should be set in the Helm values file (typically ``values.yaml``) of the umbrella chart before deploying the Helm chart.

Example Configuration (values.yaml)
-----------------------------------

.. code-block:: yaml

   telescope: "ska-mid"  # Can be "ska-mid" or "ska-low"

   numSubarrays: 4  # Number of PSS Subarrays to deploy
   numPssBeams: 8  # Number of pipeline instances to deploy

   ska_pss_lmc:
     image:
       registry: "my-docker-registry.com"
       image: "ska-pss-lmc-controller"
       tag: "latest"
       pullPolicy: "IfNotPresent"

PSS LMC Chart Configuration Table
=================================

.. list-table::
   :widths: 15 25 15
   :header-rows: 1

   * - Parameter
     - Description
     - Default
   * - ``.Values.telescope``
     - Telescope name (``ska-mid`` or ``ska-low``)
     - ``ska-mid``
   * - ``$pss``
     - PSS instance name, derived from the telescope name
     - ``mid-pss``
   * - ``.Values.numSubarrays``
     - Number of subarrays to configure
     - ``1``
   * - ``.Values.numPssBeams``
     - Number of pipeline instances to configure
     - ``1``
   * - ``.Values.ska_pss_lmc.image.registry``
     - Docker image registry for the controller
     - N/A
   * - ``.Values.ska_pss_lmc.image.image``
     - Docker image name
     - N/A
   * - ``.Values.ska_pss_lmc.image.tag``
     - Docker image tag
     - N/A
   * - ``.Values.ska_pss_lmc.image.pullPolicy``
     - Image pull policy (e.g., ``IfNotPresent``)
     - ``ska-mid``


  