==================
Manager subpackage
==================

.. automodule:: ska_pss_lmc.manager

.. toctree::
       
   Pipeline Communication Manager<communication_manager_api>
   Pipeline Component Manager Configuration<manager_configuration_api>
   