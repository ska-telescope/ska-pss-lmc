.. PSS.LMC documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SKA PSS.LMC
===============

.. Architecture ===========================================================

.. toctree::
   :maxdepth: 1
   :caption: Architecture

   architecture/architecture.rst

.. TANGO devices ==========================================================

.. toctree::
   :maxdepth: 2
   :caption: Tango devices

   Controller <tango_devices/controller/index>
   Subarray <tango_devices/subarray/index>
   PipelineCtrl <tango_devices/pipeline/index>

.. Components =============================================================

.. toctree::
   :maxdepth: 1
   :caption: Component Description

   components/index

.. API ====================================================================

.. toctree::
   :maxdepth: 1
   :caption: PSS.LMC API
   
   api/index

.. OPERATION ==============================================================

.. toctree::
   :maxdepth: 1
   :caption: Operation

   PSS LMC Tango clients examples <operation/example>

.. RELEASES ==============================================================

.. toctree::
   :maxdepth: 1
   :caption: Releases

   CHANGELOG.rst

.. DEPLOYMENT ==============================================================

.. toctree::
   :maxdepth: 1
   :caption: Deployment

   PSS LMC Deployment configuration <deployment/chart_configuration>
