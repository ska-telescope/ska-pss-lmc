################################
PSS Controller Documentation
################################

Overview
========
The LMC schema emerging during the bridging phase will be progressively implemented 
inside the MVP prototype. It will comprise some emulation of main functions. 
It is foreseen also a minimal PSS pipeline functionality. 
The **PSS.LMC Controller** implements the standard **SKA Controller interface**, but
many of its functionalities are disabled.

After deployment, the initial state of the controller is **DISABLE**.

Controller Activation
=====================
To enable the controller, the **adminMode** attribute of the device must be set
to either **ONLINE** or **ENGINEERING**. Once this is done, the controller establishes
connections with:

- All **PSS Pipeline** devices
- All **PSS Subarray** devices

Device Configuration
====================
The Fully Qualified Domain Names (**FQDNs**) of these devices are specified in
two device properties:

- **PipelineFqdns**: Contains the FQDNs of all pipeline devices.
- **PssSubarrays**: Contains the FQDNs of all subarray devices.

These properties are configurable through the **Helm chart deployment parameters**.
See the *Deployment Configuration* section for more details.

State Transition
================
Once the controller successfully connects to all required devices, its state
transitions to **ON**.

Command Restrictions
=====================
The following interface commands are **always rejected**, as the controller remains
in either **ON** or **DISABLE** state:

- **On**
- **Off**
- **Standby**

The controller does not support transitions to these states.

Deployment Configuration
========================
Refer to the Helm chart configuration documentation for details on how to set
the required parameters for pipeline and subarray devices.
