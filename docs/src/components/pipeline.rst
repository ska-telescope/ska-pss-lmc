PssPipeline device - CTRL
=========================
We aim to collect the basic interaction modes with Cheetah in order to be able to 
control it by a simple device driver to be implemented in PI8. 
Basic information on Cheetah control has been reported in the AT4-362 documentation.
The Cheetah pipeline is a single executable which contains either the CPU and the 
CUDA version of PSS and SPS pipeline. It is a CLI program and can be controlled either 
by a configuration program and by command line switches. 
The command line switch overrides the configuration file ones. 
The proposed design interact with Cheetah by means of command line and, in future, 
by OS level signals.
The PssPipeline device (CTRL module as defined in PSS.DDD) inherits from base classes CspSubElementObsDevice device
In the following are not reported the properties and attributes inherited.

Properties
----------

Properties are set in the Tango Database.

+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| Attribute Name            | Type                           |  Note/Description                                                                                     |
+===========================+================================+=======================================================================================================+
| NodeIP                    | *Type*: 'DevString'            |  The IP address of the PSS node where the cheetah pipeline will be running                            |
|                           |                                |                                                                                                       |
|                           |                                |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| PipelineName              | *Type*: 'DevString'            | The pipeline name                                                                                     |
|                           |                                |                                                                                                       |
|                           |                                |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| CheetahOutputFile         | *Type*: 'DevString'            | The filename where cheetah stdout adn stderr will be stored                                           |
|                           |                                |                                                                                                       |
|                           |                                |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| CheetahConfigFile         | *Type*: 'DevString'            | The filename where cheetah input configuration data wil be stored                                     |
|                           |                                |                                                                                                       |
|                           |                                |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| CheetahExecutable         | *Type*: 'DevString'            | A string containing the command script to execute Cheetah.                                            |
|                           |                                |                                                                                                       |
|                           |                                |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| CheetahUserPasswd         | *Type*: ('DevString', )        | A Tuple containing the username and password for the remote connection to Cheetah                     |
|                           |                                |                                                                                                       |
|                           |                                |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+


Attributes
----------
            
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| Attribute Name            | Attribute Type                 |  Note/Description                                                                                     |
+===========================+================================+=======================================================================================================+
| lastScanConfiguration     | *Type*: 'DevString'            |  The last valid scan configuration.                                                                   |
|                           |                                |                                                                                                       |
|                           | *Access*: READ                 |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| pipelineProgress          | *Type*: 'DevUShort'            | The cheetah pipeline progress percentage                                                              |
|                           |                                |                                                                                                       |
|                           | *Access*: READ                 |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| cheetahVersion            | *Type*: 'DevString'            | The cheetah pipeline version                                                                          |
|                           |                                |                                                                                                       |
|                           | *Access*: READ                 |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| cheetahPid                | *Type*: 'DevLong'              | The filename where cheetah input configuration data wil be stored                                     |
|                           |                                |                                                                                                       |
|                           | *Access*: RE                   |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| cheetahLogLine            | *Type*: 'DevString'            | Cheetah pipeline log line                                                                             |
|                           |                                |                                                                                                       |
|                           | *Access*: RE                   |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| isCommunicating           | *Type*: 'DevBoolean'           | A Tuple containing the username and password for the remote connection to Cheetah                     |
|                           |                                |                                                                                                       |
|                           | *Access*: READ                 |                                                                                                       |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| NodeIP                    | *Type*: 'DevString'            | The PSS node IP address                                                                               |
|                           |                                |                                                                                                       |
|                           | *Access*: READ                 | NOTE: it return the corresponding property                                                            |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+
| PipelineName              | *Type*: 'DevString'            | The pipeline name                                                                                     |
|                           |                                |                                                                                                       |
|                           | *Access*: READ                 | NOTE: it return the corresponding property                                                            |
+---------------------------+--------------------------------+-------------------------------------------------------------------------------------------------------+


Commands 
---------

+---------------------------+----------------------------------------------------+-----------------------------------------------+
| Command Name              | Input/output args                                  | Note                                          |
+===========================+====================================================+===============================================+
| On                        | Input: None                                        | PSS pipeline: enable (?)                      |
|                           |                                                    |                                               |
|                           | *Output*: (resultCode, resultMsg)                  |                                               |
|                           |                                                    |                                               |
+---------------------------+----------------------------------------------------+-----------------------------------------------+
| Off                       | Input: None                                        | PSS pipeline: disable (?)                     |
|                           |                                                    |                                               |
|                           | *Output*: (resultCode, resultMsg)                  |                                               |
|                           |                                                    |                                               |
+---------------------------+----------------------------------------------------+-----------------------------------------------+
| ConfigureScan             | Input: DevString                                   |                                               |
|                           |                                                    |                                               |
|                           | JSON formatted with scan configuration             |                                               |
|                           |                                                    |                                               |
|                           | *Output*: (resultCode, resultMsg)                  |                                               |
|                           |                                                    |                                               |
+---------------------------+----------------------------------------------------+-----------------------------------------------+
| GoToIdle                  | Input: None                                        | PssPipeline transits to IDLE obsState         |
|                           |                                                    |                                               |
|                           | *Output*: (resultCode, resultMsg)                  |                                               |
|                           |                                                    |                                               |
+---------------------------+----------------------------------------------------+-----------------------------------------------+
| Scan                      | *Input*: scan ID (integer)                         | Start a Scan                                  |
|                           |                                                    |                                               |
|                           | *Output*: (resultCode, resultMsg)                  |                                               |
|                           |                                                    |                                               |
+---------------------------+----------------------------------------------------+-----------------------------------------------+
| EndScan                   | Input: None                                        | End a Scan                                    |
|                           |                                                    |                                               |
|                           | *Output*: (resultCode, resultMsg)                  |                                               |
|                           |                                                    |                                               |
+---------------------------+----------------------------------------------------+-----------------------------------------------+
| Abort                     | Input: None                                        | End a Scan                                    |
|                           |                                                    |                                               |
|                           | *Output*: (resultCode, resultMsg)                  | Signal abort (see below)                      |
+---------------------------+----------------------------------------------------+-----------------------------------------------+
| ObsReset                  | Input: None                                        | Reset Pipeline from FAULT/ABORTED             |
|                           |                                                    | to IDLE obsState.                             |
|                           | *Output*: (resultCode, resultMsg)                  |                                               |
|                           |                                                    |                                               |
+---------------------------+----------------------------------------------------+-----------------------------------------------+

Cheetah command line interface
==============================

Cheetah program can be started with a command line of the form

./cheetah/pipeline/cheetah\_pipeline --config cheetah.xml -p
Dedispersion --log-level log

The different pipelines which can be selected are:

-  Empty
-  Dedispersion
-  RfiDetectionPipeline
-  SinglePulseHandler

For the input stream (the data source) there are the possibility to
create synthetic internally generated data or external sources. We chose
to use a standard file generated by sigproc fake (*ska.dat* hardcoded
in the standard config file).
The output of cheetah is on the standard output and it has a standard
syslog format:

- [log][tid=140474636963584][/home/baffa/src/ska/cheetah/cheetah/../cheetah/pipeline/detail/BeamLauncher.cpp:148][1600767420]Creating Beams....
- [warn][tid=140474636963584][/home/baffa/src/ska/cheetah/cheetah/../cheetah/tdas/detail/Tdas.cpp:76][1600767420]No Time Domain Accelerated Search algorithm has been specified
- [log][tid=140474636963584][/home/baffa/src/ska/cheetah/cheetah/../cheetah/pipeline/detail/BeamLauncher.cpp:171][1600767420]Finished creating pipelines
- [log][tid=140474636963584][/home/baffa/src/ska/cheetah/cheetah/../cheetah/pipeline/detail/BeamLauncher.cpp:223][1600767420]Starting Beam: identifier to distinguish between other similar type blocks
- [log][tid=140474527926016][/home/baffa/src/ska/cheetah/cheetah/../cheetah/sps/detail/Sps.cpp:110][1600767420]setting dedispersion buffer size to 2048 spectra
- [log][tid=140474527926016][/home/baffa/src/ska/cheetah/cheetah/../cheetah/sps/detail/Sps.cpp:113][1600767420]setting buffer overlap to 1514 spectra
- [log][tid=140474527926016][/home/baffa/src/ska/cheetah/cheetah/sigproc/src/SigProcFileStream.cpp:334][1600767424]resizing to 1024
- [log][tid=140474527926016][/usr/local/include/panda/detail/Pipeline.cpp:63][1600767424]End of stream

Cheetah pipeline is a stand alone CLI program with its main output on
stdout. 
It runs on an different server of the Tango Device Server. We have implemented a class, called PipelineCommunicationManager, that have the skeleton of connection. 
It has to be specialized with the protocol to use with connection. At the present the connection is made via SSH.

This command line approach makes Cheetah program completely deaf while
running: it is not foreseen a communication channel from CONTROL to the
pipeline. The only possible channel is the use of Posix signals. We list
here few useful ones with default behaviour:

#. *SIGKILL 9 Kill signal → Terminate*
#.  SIGTERM 15 Termination signal → Terminate
#.  SIGUSR1 30,10,16 User-defined → Terminate
#.  SIGSTOP 17,19,23 Stop process → Terminate

The use of *SIGKILL*, *SIGTERM* and *SIGSTOP* are clear and related to
Abort (first and second) and to Stop (last). We have the opportunity to
implement a custom channel by the use of *SIGUSR1.*

*NOTE: At the present implementation both Abort and EndScan are sending a Kill Signal*
