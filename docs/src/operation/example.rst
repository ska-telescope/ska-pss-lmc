##################################
PSS LMC Tango Clients Examples
##################################

.. _pss-lmc-example-page:

In the following sections some itango interface examples are provided, to be easily applied to a generic python client.

Basic assumptions for each example are:

1. the system has been fresh initialized
2. all TANGO operations (read/write/command_inout) are always successful. No check on the results is done
   in the following examples.

To control PSS with itango, a proxy to PSS Pipeline Control Device has to be created: ::
    
    pss_ctrl = tango.DeviceProxy("mid-pss/pipeline/0001")

Please note that all commands on subarray follow the `ObsState state model <https://confluence.skatelescope.org/pages/viewpage.action?pageId=105416556>`_ defined in ADR-8. 
A PSS Pipeline Control Device doesn't allow commands from observing states other than those specified in this model. 

After the deployment, the PSS Pipeline Control device is in following conditions: ::

    - state DISABLE.
    - healthState UNKNOWN
    - adminMode OFFLINE
    - obstState IDLE

From now on, all the examples refers to the proxy objects specified above. 

*******************************
PSS LMC start communication
*******************************

The PSS LMC Devices *adminMode* is a memorized attribute. That means that it is stored in the TANGO DB and 
its value is written to the devices just after the initialization. If the *adminMode* is ENGINEERING(2)/ONLINE(0) the connection 
between PSS Pipeline Control device and the ssh server where the container with cheetah pipeline is running starts immediately.
Otherwise, to start the communication use the following command::

    pss_ctrl.adminMode = 2  #set to ENGINEERING

or

::

    pss_ctrl.adminMode = 0  #set to ONLINE

The new states are the following for all the devices::

- state ON
- healthState OK
- adminMode ENGINEERING(2)/ONLINE(0)
- obstState IDLE

**********************************
Power-on (off/standby) the PSS 
**********************************
PSS Pipeline Control device starts in the On state. 

On command may be issued:: 

    pss_ctrl.On()

The command returns immediately the following list:

::

    [array([5], dtype=int32), ['Device is already in ON state.']]   

*Off* and *Standby* commands are currently not implemented, therefore the system remains in ON state.
 
    pss_ctrl.state() -> ON 

********************************
Configure, issue and end a scan 
********************************

PSS Pipeline Control device starts in obsState IDLE therefore the resources do not need to be assigned explicitly. It is possible to directly configure the resources and then start a scan.


The following JSON string can be used to configure resources to the PSS Pipeline Control device. In order for cheetah to parse this, in the helm chart, the *CheetahPipelineSource* should be set to *udp_low*, and the *CheetahPipelineType* should be set to *Empty*. The string has to be assigned to a variable and sent as command input.::

    json_scan_configure = '{"beams":{"beam":{"active":true,"source":{"udp_low":{"number_of_threads":2,"samples_per_chunk":2048,"number_of_channels":7776,"max_buffers":10,"listen":{"port":9029,"ip_address":"0.0.0.0"}}},"id":1}},"id":1,"config_id":1}'

This will enable processing of a single beam, arriving from "localhost" via port 9029. Note that we do not provide any instructions for passing any beamformer data to cheetah. In this mode, cheetah will simply wait indefinitely for a datastream until it is killed. 

Alternatively, to control a Single Pulse Search (SPS) pipeline that will process data from a file and produce SPS candidates, we provide an `example config <https://gitlab.com/ska-telescope/ska-pss-lmc/-/blob/master/examples/cheetah_single_beam_sps.json?ref_type=heads>`_ that can be used *as is* if controlling a containerised cheetah. In this case, in the helm chart, the *CheetahPipelineSource* should be set to *sigproc*, and the *CheetahPipelineType* should be set to *SinglePulse*. 

Configure command has to be issued in the following way:: 

    pss_ctrl.configurescan(json_string_configure)

The observing state will be in CONFIGURING during the execution. After that, if the command is successful::

    pss_ctrl.longRunningCommandResult -> ('1719839772.536875_188232486395099_ConfigureScan', '0')
    pss_ctrl.obsstate -> READY

The device in READY observing state can be re-configured with a new configuration that overwrites the previous one.

When the device is READY, a scan can be started, issuing the *Scan* command::

    json_string_scan = '11'

    pss_ctrl.scan(json_string_scan)

If the command is successful::

    pss_ctrl.longRunningCommandResult ->  ('1719840017.9207077_226452471315414_Scan', '1')
    pss_ctrl.obsstate -> SCANNING

During the scan we may inspect several attributes that hold information about the pipeline run. ::

    pss_ctrl.pipelinename -> 'pipeline-00-00-01'
    pss_ctrl.pipelineProgress -> 72
    pss_ctrl.cheetahLogLine -> '[log][tid=140424071128832][/home/baffa/src/ska/cheetah/cheetah/sigproc/src/SigProcFileStream.cpp:334][1602501626]resizing to 1024\r\n'
    pss_ctrl.cheetahPid -> 124
    pss_ctrl.cheetahVersion -> 'x.y.z'

Note that the result code associated to the *Scan* command will remain '1' for all the duration of the 
scanning process. In fact, 
according to `ADR-8 <https://confluence.skatelescope.org/pages/viewpage.action?pageId=105416556>`_ a scan can 
be interrupted by the *EndScan* or the *Abort* command. 
The *Abort* command has to be intended as an emergency call that interrupts abruptly the scan process. 
On the other side, the *EndScan* first ensures that all the processes are correctly managed. 

To end a scan, just issue::

    pss_ctrl.endscan()

After *EndScan* is successful, the subarray *obsState* is READY, and another scan can be issued with the 
same configuration. 

On the other side, if the scan is aborted, the *obsState* will go (after a short time in ABORTING) to ABORTED state.
To perform a new scanning, the subarray observation should be restarted (via the *ObsReset* command) and a new configuration need to be 
sent (`ADR-8 <https://confluence.skatelescope.org/pages/viewpage.action?pageId=105416556>`_)

The sequence of operation is::

    pss_ctrl.abort()
    pss_ctrl.obsstate -> ABORTING
    pss_ctrl.obsstate -> ABORTED
    pss_ctrl.obsreset()
    pss_ctrl.obsstate -> RESETTING
    pss_ctrl.obsstate -> IDLE
