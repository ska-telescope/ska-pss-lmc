###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

Latest pre-release
------------------

0.2.0-rc.1
-----------
- Add log parser.
- Updated the ska-csp-lmc-base module to version 1.0.0 (BC 1.2.3).
- Developed the PSS Controller device. 
- Updated the Helm charts to deploy the PSS Controller in k8s.
- Developed the PSS Subarray device. 
- Updated the Helm charts to deploy the PSS Subarray in k8s.
- Update documentation
- Use simulcheetah for integration tests

Added
-----

- Lug 2022 - Added Pss CTRL Pipeline Device

- Empty Python project directory structure

- Oct 15 2020 CT-150 repository structure organization

- Oct 20 2020 CT-150:

  - added docker folder for docker-compose

  - added ssh in docker image, updated tango device, modified simulCheetah in Base_interface

  - updated scan method and added read message in threading

  - added obs_state

- Oct 21 2020 AT4-363 Instantiate the ObsDevice Class 

- Oct 30 2020 AT4-364 Implement Cheetah spawn and its STDOUT collection
- Feb-April 2021 AT4-272 Initial version of Control and Subarray 
