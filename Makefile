#
# Project makefile for a Tango project. You should normally only need to modify
# DOCKER_REGISTRY_USER and PROJECT below.
#
PROJECT = ska-pss-lmc

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-pss-lmc

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= test

# HELM_CHART the chart name
HELM_CHART ?= ska-pss-lmc-umbrella
HELM_CHARTS = ska-pss-lmc
HELM_CHARTS_TO_PUBLISH = ska-pss-lmc ska-cheetah

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
UMBRELLA_CHART_PATH ?= charts/ska-pss-lmc-umbrella/

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400
# Helm version
HELM_VERSION = v3.3.1
# kubectl version
KUBERNETES_VERSION = v1.19.2
PYTANGO_BUILDER_VERSION = 9.4.3

KUBE_CONFIG_BASE64 ?=  ## base64 encoded kubectl credentials for KUBECONFIG
KUBECONFIG ?= /etc/deploy/config ## KUBECONFIG location

XAUTHORITYx ?= ${XAUTHORITY}
THIS_HOST := $(shell ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY := $(THIS_HOST):0
JIVE ?= false# Enable jive
MINIKUBE ?= true ## Minikube or not
EXPOSE_All_DS ?= true ## Expose All Tango Services to the external network (enable Loadbalancer service)
SKA_TANGO_OPERATOR ?= true
CLUSTER_DOMAIN ?= cluster.local

# Test runner - run to completion job in K8s
# name of the pod running the k8s_tests
TEST_RUNNER = test-makefile-runner-$(CI_JOB_ID)-$(KUBE_NAMESPACE)-$(HELM_RELEASE)

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
# include OCI Images support 
include .make/oci.mk 

# include k8s support 
include .make/k8s.mk 

# include Helm Chart support 
include .make/helm.mk 

# Include Python support 
include .make/python.mk 

# include raw support 
include .make/raw.mk 

# include core make support 
include .make/base.mk

# include your own private variables for custom deployment configuration 
-include PrivateRules.mak 


# Chart for testing
K8S_CHART = ska-pss-lmc-umbrella
K8S_CHARTS = $(K8S_CHART)


K8S_TEST_IMAGE_TO_TEST = $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-itango:9.3.7 ## TODO: UGUR docker image that will be run for testing purpose
CI_JOB_ID ?= local##pipeline job id
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server
K8S_TEST_RUNNER = test-runner-$(CI_JOB_ID)##name of the pod running the k8s-test
CLUSTER_DOMAIN ?= cluster.local

OCI_IMAGES = ska-pss-lmc

ITANGO_DOCKER_IMAGE = $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-itango:9.3.7

#
# Defines a default make target so that help is printed if make is called
# without a target
#
.DEFAULT_GOAL := help

#PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src:/app/src/ska_pss_lmc KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=$(RELEASE_NAME) TANGO_HOST=$(TANGO_HOST)

MARK_UN ?= unit

PYTHON_VARS_AFTER_PYTEST = -k $(MARK_UN) --forked \
						--disable-pytest-warnings

PYTHON_BUILD_TYPE = non_tag_pyproject

PYTHON_LINT_TARGET = src/ tests/

PYTHON_SWITCHES_FOR_FLAKE8 =--per-file-ignores="src/simulcheetah.py:E501,tests/unit/pipeline/conftest.py:E501,\
        src/ska_pss_lmc/controller/controller_component_manager.py:E302,src/ska_pss_lmc/common/capability.py:E302,"
# C0103(invalid-name)	
# C0116(missing-function-docstring)
# C0103(invalid-name)
# R0401(cyclic-import),
# R0801(duplicate-code) not enough to disable in file
# R0902(too-many-instance-attributes)
# R0903(too-few-public-methods)

PYTHON_SWITCHES_FOR_PYLINT =--disable=C0103,C0112,C0114,C0115,C0116,R0801,R0902,R0903

VALUES_FILE ?= charts/ska-pss-lmc-umbrella/values-default.yaml
CUSTOM_VALUES =

ifneq ($(VALUES_FILE),)
CUSTOM_VALUES := --values $(VALUES_FILE)
else
endif

ifneq ($(CI_REGISTRY),)
K8S_TEST_TANGO_IMAGE = --set ska-pss-lmc.ska_pss_lmc.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
    --set ska-pss-lmc.ska_pss_lmc.image.registry=$(CI_REGISTRY)/ska-telescope/ska-pss-lmc 
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/ska-pss-lmc/ska-pss-lmc:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
K8S_TEST_TANGO_IMAGE = --set ska-pss-lmc.ska_pss_lmc.image.tag=$(VERSION)
K8S_TEST_IMAGE_TO_TEST =$(CAR_OCI_REGISTRY_HOST)/ska-pss-lmc:$(VERSION) 
endif

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
    --set global.tango_host=$(TANGO_HOST) \
    --set ska-tango-base.display=$(DISPLAY) \
    --set ska-tango-base.xauthority=$(XAUTHORITY) \
    --set ska-tango-base.jive.enabled=$(JIVE) \
    --set global.cluster_domain=$(CLUSTER_DOMAIN) \
    --set global.exposeAllDS=$(EXPOSE_All_DS) \
    --set global.operator=$(SKA_TANGO_OPERATOR) \
    ${K8S_TEST_TANGO_IMAGE} \
    $(CUSTOM_VALUES)



# override python.mk python-pre-test target
python-pre-test: PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src:/app/src/ska_pss_lmc 
python-pre-test:
	@echo "python-pre-test: running with: $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) pytest $(PYTHON_VARS_AFTER_PYTEST) \
	 --cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml $(PYTHON_TEST_FILE)"

ifeq ($(MARK), all)
 SEL_TEST=-m pipeline
else ifeq ($(MARK), nightly)
 SEL_TEST=
else 
 SEL_TEST=-m $(MARK)
endif

PYTHON_TEST_FILE ?= tests/unit
COUNT ?= 1
TEST_FOLDER ?= integration  

ADDITIONAL_AFTER_PYTEST ?= -n 4
PYTHON_VARS_AFTER_PYTEST = -k $(MARK_UN) --forked \
                        --disable-pytest-warnings $(ADDITIONAL_AFTER_PYTEST)

# set different switches for in cluster: --true-context
k8s-test: PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=./src:/app/src:/app/src/ska_pss_lmc KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=$(RELEASE_NAME) TANGO_HOST=$(TANGO_HOST)
k8s-test: PYTHON_VARS_AFTER_PYTEST := \
            --cucumberjson=build/reports/cucumber.json \
            --json-report --json-report-file=build/reports/report.json \
             --disable-pytest-warnings --count=$(COUNT) --timeout=600 --forked --log-cli-level=INFO $(SEL_TEST) -k $(TEST_FOLDER) 

k8s-pre-template-chart: k8s-pre-install-chart

requirements: ## Install Dependencies
	poetry install

.PHONY: all test up down help k8s show lint logs describe mkcerts localip namespace delete_namespace ingress_check kubeconfig kubectl_dependencies helm_dependencies rk8s_test k8s_test rlint

.PHONY: all test help k8s show lint deploy delete logs describe namespace delete_namespace kubeconfig kubectl_dependencies helm_dependencies rk8s_test k8s_test rlint install-chart uninstall-chart reinstall-chart upgrade-chart
