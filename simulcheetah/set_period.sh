#!/bin/bash

BASHRC="$HOME/.bashrc"

# Check if a value is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <numeric_value>"
    exit 1
fi

# Check if the input is a valid number (integer or decimal)
if ! [[ "$1" =~ ^[0-9]+([.][0-9]+)?$ ]]; then
    echo "Error: PERIOD must be a numeric value (integer or decimal)."
    exit 1
fi

NEW_PERIOD="$1"

# Check if PERIOD is already set in .bashrc
if grep -q "^export PERIOD=" "$BASHRC"; then
    # Replace the existing PERIOD value with the new one
    sed -i "s/^export PERIOD=.*/export PERIOD=$NEW_PERIOD/" "$BASHRC"
else
    # Append PERIOD if not found
    echo "export PERIOD=$NEW_PERIOD" >> "$BASHRC"
fi

# Apply the change immediately
export PERIOD=$NEW_PERIOD

echo "PERIOD is now set to $PERIOD (persisted in .bashrc)."
