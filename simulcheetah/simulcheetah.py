#!/usr/bin/python3

"""Cheetah emulator for popen test."""
#
# 2020 C.Baffa, E.Giani - Licence GPL 2
#
# library functions

import time
import subprocess
import sys

variables = {}
# Run the commands in an interactive bash shell and capture the output
for var in ["ERROR", "PERIOD"]:
    command = f"source ~/.bashrc && echo ${var}"
    result = subprocess.run(["bash",  "-c", command], capture_output=True, text=True)
    variables[var] = result.stdout.strip()
    if result.returncode != 0:
        print(f"Subprocess failed with exit code {result.returncode}", flush=True)
        sys.exit(result.returncode)

# pylint: disable=line-too-long
outputLib = [
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/"
    "../cheetah/pipeline/detail/BeamLauncher.cpp:148][1602501622]Creating"
    " Beams....",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/"
    "../cheetah/fldo/detail/Fldo.cpp:44][1602501622]fldo::"
    " CUDA algorithm activated",
    "[warn][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/tdas/detail/Tdas.cpp:76][1602501622]No"
    " Time Domain Accelerated Search algorithm has been specified",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/pipeline/detail/BeamLauncher.cpp:171][1602501622]Finished"
    " creating pipelines",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/pipeline/detail/BeamLauncher.cpp:223][1602501622]Starting"
    " Beam: identifier to distinguish between other similar type blocks",
    "[log][tid=140424071128832][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/sps/detail/Sps.cpp:110][1602501622]setting"
    " dedispersion buffer size to 2048 spectra",
    "[log][tid=140424071128832][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/sps/detail/Sps.cpp:113][1602501622]setting"
    " buffer overlap to 1514 spectra",
    "[log][tid=140424071128832][/home/baffa/src/ska/cheetah/cheetah/sigproc/"
    "src/SigProcFileStream.cpp:334][1602501626]resizing"
    " to 1024",
    "[log][tid=140424071128832][/usr/local/include/panda/detail/"
    "Pipeline.cpp:63][1602501626]End of stream",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/pipeline/detail/BeamLauncher.cpp:90][1602501626]Stopping"
    " Beam: identifier to distinguish between other similar type blocks",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/pipeline/detail/BeamLauncher.cpp:264][1602501626]threads"
    " joined",
    "[warn][tid=140424180166400][/usr/local/include/panda/detail/"
    "ResourcePoolImpl.cpp:59][1602501626]End jobs",
]

if variables["ERROR"] == "true": 
    outputLib += ["[error][tid=140424180166400][/usr/local/include/panda/detail/ResourcePoolImpl.cpp:59 test error][1602501626]End"]


while True:
    for outString in outputLib:
        print(outString, flush=True)
        time.sleep(float(variables["PERIOD"]))
