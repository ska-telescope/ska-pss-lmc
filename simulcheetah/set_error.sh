#!/bin/bash

if [ -z "$1" ]; then
    echo "No argument provided. Please provide a value for ERROR (true/false)."
    exit 1
fi

ERROR_VALUE=$1
BASHRC="$HOME/.bashrc"

if grep -q "^export ERROR=" "$BASHRC"; then
    sed -i '/^export ERROR=/d' "$BASHRC"
fi

echo "export ERROR=$ERROR_VALUE" >> "$BASHRC"

export ERROR=$ERROR_VALUE

echo "ERROR variable set to $ERROR_VALUE in .bashrc and current session."
