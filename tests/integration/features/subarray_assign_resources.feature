Feature: PSS Subarray Assign and Release resources
      I want to Assign and Release resources on PSS Subarray
      and change my state accordingly
    
    Background:
        Given PipelineCtrlDevice0001 is fresh initialized
        And PssController is fresh initialized
        And PssSubarray is fresh initialized
        When PssController set adminMode to ONLINE
        And PssSubarray set adminMode to ONLINE
        And PssController isCommunicating is True
        And PssController State is ON
        And PssController pipelinesHealthState is ('OK',)
        And PssController pipelinesObsState is ('IDLE',)
        And PssController pipelinesAdminMode is ('ONLINE',)
        And PssController healthState is OK
        And PssSubarray State is ON
        And PssSubarray ObsState is EMPTY
        And PssSubarray HealthState is UNKNOWN

    @pipeline 
    Scenario: Pss Subarray assign and release resources with pss-pipeline-01 
        
        When PssSubarray runs command AssignResources (argument from file: AssignResources_basic.json)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray pipelinesHealthState is ('OK',)
        And PssSubarray pipelinesObsState is ('IDLE',)
        And PssSubarray pipelinesAdminMode is ('ONLINE',)
        And PssSubarray assignedPipelinesIDs is [1]
        And PssSubarray ObsState is IDLE
        # And PssSubarray healthState is OK

        When PssSubarray runs command ReleaseAllResources (no argument)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray ObsState is EMPTY
        And PssSubarray assignedPipelinesIDs is []
