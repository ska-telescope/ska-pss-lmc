Feature: Basic functionalities of simulcheetah that is used in ci/cd tests
    
    @pipeline 
    Scenario: Simulcheetah logs an error

        Given simulcheetah is set to log an error
        When simulcheetah runs
        Then an error is logged in simulcheetah

    @pipeline 
      Scenario: PssCtrlPipeline device is able to detect an error from Cheetah logs
        Given PipelineCtrlDevice0001 is fresh initialized and connected
        And simulcheetah is set to log an error

        When PipelineCtrlDevice0001 runs command ConfigureScan with argument {"id": 1, "config": "test"}
        Then PipelineCtrlDevice0001 ObsState is READY
        And PipelineCtrlDevice0001 lastScanConfiguration is {"id": 1, "config": "test"}

        When PipelineCtrlDevice0001 runs command Scan with argument {"scan_id": "11"}
        Then PipelineCtrlDevice0001 ObsState is SCANNING

        # Then an error is logged in simulcheetah
        And PipelineCtrlDevice0001 ObsState is FAULT

        When PipelineCtrlDevice0001 runs command ObsReset (no argument)
        Then PipelineCtrlDevice0001 ObsState is IDLE
        And PipelineCtrlDevice0001 lastScanConfiguration is empty
        
        When PipelineCtrlDevice0001 set AdminMode to OFFLINE
        Then PipelineCtrlDevice0001 State is DISABLE

