Feature: Control that all the attributes of the pipeline are in the proper state at initialization

  @pipeline
  Scenario: PssCtrlPipeline device is properly initialized and connected
    
    Given PipelineCtrlDevice0001 is fresh initialized
    When PipelineCtrlDevice0001 set AdminMode to ONLINE
    Then PipelineCtrlDevice0001 State is ON
    And PipelineCtrlDevice0001 ObsState is IDLE
    And PipelineCtrlDevice0001 HealthState is OK
    When PipelineCtrlDevice0001 set AdminMode to OFFLINE
    Then PipelineCtrlDevice0001 State is DISABLE
    




