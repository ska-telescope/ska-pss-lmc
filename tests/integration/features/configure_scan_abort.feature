Feature: Control that basic operations are performed by PipelineCtrlDevice
  
  @pipeline
  Scenario: PssCtrlPipeline device is able to send configure->scan->abort->obsreset
    Given PipelineCtrlDevice0001 is fresh initialized and connected
    
    When PipelineCtrlDevice0001 runs command ConfigureScan with argument {"id": 1, "config": "test"}
    Then PipelineCtrlDevice0001 ObsState is READY
    And PipelineCtrlDevice0001 lastScanConfiguration is {"id": 1, "config": "test"}

    When PipelineCtrlDevice0001 runs command Scan with argument {"scan_id": "11"}
    Then PipelineCtrlDevice0001 ObsState is SCANNING

    When PipelineCtrlDevice0001 runs command Abort (no argument)
    Then PipelineCtrlDevice0001 ObsState is ABORTED

    When PipelineCtrlDevice0001 runs command ObsReset (no argument)
    Then PipelineCtrlDevice0001 ObsState is IDLE
    And PipelineCtrlDevice0001 lastScanConfiguration is empty

    When PipelineCtrlDevice0001 runs command ConfigureScan with argument {"id": 1, "config": "test"}
    Then PipelineCtrlDevice0001 ObsState is READY
    And PipelineCtrlDevice0001 lastScanConfiguration is {"id": 1, "config": "test"}

    When PipelineCtrlDevice0001 runs command GoToIdle (no argument)
    Then PipelineCtrlDevice0001 ObsState is IDLE
    And PipelineCtrlDevice0001 lastScanConfiguration is empty
    When PipelineCtrlDevice0001 set AdminMode to OFFLINE
    Then PipelineCtrlDevice0001 State is DISABLE
