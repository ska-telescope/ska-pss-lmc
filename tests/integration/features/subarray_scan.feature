Feature: PSS Subarray Scan
    
    Background:
        Given PipelineCtrlDevice0001 is fresh initialized
        And PssController is fresh initialized
        And PssSubarray is fresh initialized
        When PssController set adminMode to ONLINE
        And PssSubarray set adminMode to ONLINE
        And PssController isCommunicating is True
        And PssController State is ON
        And PssController pipelinesHealthState is ('OK',)
        And PssController pipelinesObsState is ('IDLE',)
        And PssController pipelinesAdminMode is ('ONLINE',)
        And PssController healthState is OK
        And PssSubarray State is ON
        And PssSubarray ObsState is EMPTY
        And PssSubarray HealthState is UNKNOWN
    
    @pipeline 
    Scenario: Pss Subarray configure and scan with pss-pipeline-01 
    
        When PssSubarray runs command AssignResources (argument from file: AssignResources_basic.json)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray pipelinesHealthState is ('OK',)
        And PssSubarray pipelinesObsState is ('IDLE',)
        And PssSubarray pipelinesAdminMode is ('ONLINE',)
        And PssSubarray assignedPipelinesIDs is [1]
        And PssSubarray ObsState is IDLE

        When PssSubarray runs command Configure (argument from file: Configure_basic.json)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray pipelinesHealthState is ('OK',)
        And PssSubarray pipelinesObsState is ('READY',)
        And PssSubarray pipelinesAdminMode is ('ONLINE',)
        And PssSubarray assignedPipelinesIDs is [1]
        And PssSubarray ObsState is READY

        When PssSubarray runs command Scan (argument from file: Scan_basic.json)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray pipelinesObsState is ('SCANNING',)
        And PssSubarray ObsState is SCANNING

        When PssSubarray runs command EndScan (no argument)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray pipelinesObsState is ('READY',)
        And PssSubarray ObsState is READY

        When PssSubarray runs command GoToIdle (no argument)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray pipelinesObsState is ('IDLE',)
        And PssSubarray ObsState is IDLE

        When PssSubarray runs command ReleaseAllResources (no argument)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray ObsState is EMPTY
        And PssSubarray assignedPipelinesIDs is []

    @pipeline @thisone2
    Scenario: Simulcheetah logs an error and PssSubarray obsstate goes to fault

        Given PipelineCtrlDevice0001 is fresh initialized
        And PssController is fresh initialized
        And PssSubarray is fresh initialized
        And simulcheetah is set to log an error

        When PssController set adminMode to ONLINE
        And PssSubarray set adminMode to ONLINE
        And PssController isCommunicating is True

        When PssSubarray runs command AssignResources (argument from file: AssignResources_basic.json)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray pipelinesHealthState is ('OK',)
        And PssSubarray pipelinesObsState is ('IDLE',)
        And PssSubarray pipelinesAdminMode is ('ONLINE',)
        And PssSubarray assignedPipelinesIDs is [1]
        And PssSubarray ObsState is IDLE

        When PssSubarray runs command Configure (argument from file: Configure_basic.json)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray pipelinesHealthState is ('OK',)
        And PssSubarray pipelinesObsState is ('READY',)
        And PssSubarray pipelinesAdminMode is ('ONLINE',)
        And PssSubarray assignedPipelinesIDs is [1]
        And PssSubarray ObsState is READY

        When PssSubarray runs command Scan (argument from file: Scan_basic.json)
        # Then PssSubarray longRunningCommandStatus is 'COMPLETED'
        Then PssSubarray pipelinesObsState is ('SCANNING',)
        # And PssSubarray ObsState is SCANNING
        # When an error is logged in simulcheetah
        # And PipelineCtrlDevice0001 ObsState is FAULT
        And PipelineCtrlDevice0001 ObsState is SCANNING

        # Then an error is logged in simulcheetah
        And PipelineCtrlDevice0001 ObsState is FAULT
        And PssSubarray ObsState is FAULT

        When PssSubarray runs command Restart (no argument)
        Then PssSubarray ObsState is EMPTY
        And PipelineCtrlDevice0001 ObsState is IDLE
        And PipelineCtrlDevice0001 lastScanConfiguration is empty
