Feature: Control that all the attributes of the Subarray are in the proper state at initialization

  @pipeline
  Scenario: Subarray device is properly initialized
    
    Given PssSubarray is fresh initialized
    When PssSubarray set adminMode to ONLINE
    Then PssSubarray State is ON
    And PssSubarray ObsState is EMPTY
    And PssSubarray HealthState is UNKNOWN
