Feature: Control that all the attribute of the pipeline are in the proper state at initialization

  @pipeline
  Scenario: Controller device is properly initialized and connected
    
    Given PipelineCtrlDevice0001 is fresh initialized
    And PssController is fresh initialized
    And PssSubarray is fresh initialized
    When PssController set adminMode to ONLINE
    Then PssController isCommunicating is True
    And PssController State is ON
    #And PssController pipelinesState is ('ON',)
    And PssController pipelinesHealthState is ('OK',)
    And PssController pipelinesObsState is ('IDLE',)
    And PssController pipelinesAdminMode is ('ONLINE',)
    And PssController healthState is OK




