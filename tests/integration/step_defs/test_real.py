import logging
import time

import paramiko
import pytest
from pytest_bdd import given, scenarios, then, when

from ska_pss_lmc.testing.test_bdd_utils import TestDevice

module_logger = logging.getLogger(__name__)

#  -------------- SCENARIOS --------------

scenarios("../features/initial_attributes.feature")
scenarios("../features/configure_scan_abort.feature")
scenarios("../features/controller.feature")
scenarios("../features/subarray.feature")
scenarios("../features/subarray_assign_resources.feature")
scenarios("../features/subarray_scan.feature")
scenarios("../features/simulcheetah.feature")

#  -------------- MARKERS --------------


def pytest_configure(config):
    config.addinivalue_line("markers", "pipeline", "thisone")


# -------------- FIXTURES --------------


list_of_pipeline_names = ["PipelineCtrlDevice0001"]
list_of_pss_devices = [
    "PssController",
    "PssSubarray",
]


@pytest.fixture(scope="session")
def all_pipelines():
    """Pipelines dictionary"""
    pipelines = {}
    for name in list_of_pipeline_names:
        idx = name
        pipelines[idx] = TestDevice(name)
    return pipelines


@pytest.fixture(scope="session")
def all_pss_devices():
    """PSS devices dictionary"""
    devices = {}
    for dev_name in list_of_pss_devices:
        devices[dev_name] = TestDevice(dev_name)
    return devices


simulcheetah_parameters = {
    "hostname": "cheetah-deployment-0.cheetah-service",
    "username": "cheetah",
    "password": "cheetah",
}


@pytest.fixture
def fixt_for_teardown():
    yield
    set_simulcheetah_error("false")


@given("simulcheetah is set to log an error")
def set_error_true(fixt_for_teardown):
    set_simulcheetah_error("true")


def set_simulcheetah_error(val):
    module_logger.info(f"Setting simulcheetah error to {val}")
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    command = f"./set_error.sh {val}"

    try:
        client.connect(
            simulcheetah_parameters["hostname"],
            username=simulcheetah_parameters["username"],
            password=simulcheetah_parameters["password"],
        )
        _, stdout, stderr = client.exec_command(command)
        error = stderr.read().decode()

        if error:
            raise RuntimeError(f"SSH command failed: {error}")

    finally:
        client.close()


@when("simulcheetah runs")
def run_simulcheetah():

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    command = "./simulcheetah.py | tee output.log"

    try:
        client.connect(
            simulcheetah_parameters["hostname"],
            username=simulcheetah_parameters["username"],
            password=simulcheetah_parameters["password"],
        )

        # Open a session with a pseudo-terminal (PTY) to allow signal handling
        channel = client.get_transport().open_session()
        channel.get_pty()  # Enables TTY-like behavior
        channel.invoke_shell()
        channel.send(command + "\n")

        time.sleep(3)
        # Send CTRL+C (SIGINT) to interrupt the infinite loop
        channel.send("\x03")  # Equivalent to pressing CTRL+C
        time.sleep(1)

        while channel.recv_ready():
            output = channel.recv(1024).decode()
            module_logger.info(output)  # Print live output

    finally:
        channel.close()
        client.close()


@then("an error is logged in simulcheetah")
def check_error_in_log():
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    error_line = (
        "[error][tid=140424180166400][/usr/local/include/panda/detail/"
        "ResourcePoolImpl.cpp:59 test error][1602501626]End"
    )
    command = f'cat output.log | grep "{error_line}"'

    try:
        client.connect(
            simulcheetah_parameters["hostname"],
            username=simulcheetah_parameters["username"],
            password=simulcheetah_parameters["password"],
        )
        _, stdout, stderr = client.exec_command(command)
        error = stderr.read().decode()

        if error:
            raise RuntimeError(f"SSH command failed: {error}")

    finally:
        client.close()
