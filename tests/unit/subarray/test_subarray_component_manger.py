# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
from __future__ import annotations

import logging
from concurrent.futures import Future
from unittest.mock import Mock

import mock
import pytest
from ska_control_model import (
    CommunicationStatus,
    HealthState,
    ObsState,
    TaskStatus,
)

# Create module logger that logs all levels
module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

# pylint: disable=protected-access


class TestSubarrayComponentManager:
    """Tests of the Subarray Component Manager."""

    def test_subarray_id(self, subarray_component_manager):
        """Test subarray id range"""
        subarray_component_manager.subarray_id = 3
        assert subarray_component_manager.subarray_id == 3
        with pytest.raises(ValueError):
            subarray_component_manager.subarray_id = 17
            assert subarray_component_manager.subarray_id == 3

    def test_establish_pipeline_connection_success(
        self, subarray_component_manager
    ):
        """
        Test for establish pipeline connection sub-method happy path
        inside start_communicating
        """
        subarray_component_manager._create_assigned_pipelines = Mock()
        subarray_component_manager._create_assigned_pipelines.return_value = [
            "mid-pss/pipeline/0001"
        ]
        subarray_component_manager._connect_and_subscribe = Mock()
        subarray_component_manager._connect_and_subscribe.return_value = (
            "mid-pss/pipeline/0001",
            CommunicationStatus.ESTABLISHED,
        )

        # Create a mock future
        future = Future()
        future.set_result(
            ("mid-pss/pipeline/0001", CommunicationStatus.ESTABLISHED)
        )
        subarray_component_manager._sub_executor.submit = Mock()
        subarray_component_manager._sub_executor.submit.return_value = future

        # Execute the function
        result = subarray_component_manager._establish_pipeline_connections(
            [1]
        )

        # Verify results
        assert result == {
            "mid-pss/pipeline/0001": CommunicationStatus.ESTABLISHED
        }
        assert subarray_component_manager._assigned_pipelines_fqdns == [
            "mid-pss/pipeline/0001"
        ]
        assert subarray_component_manager._assigned_pipelines_ids == [1]

    def test_establish_pipeline_connection_failed(
        self, subarray_component_manager
    ):
        """
        Test for establish pipeline connection sub-method unhappy path
        inside start_communicating
        """
        subarray_component_manager._create_assigned_pipelines = Mock()
        subarray_component_manager._create_assigned_pipelines.return_value = [
            "mid-pss/pipeline/0001"
        ]
        subarray_component_manager._connect_and_subscribe = Mock()
        subarray_component_manager._connect_and_subscribe.return_value = (
            "mid-pss/pipeline/0001",
            CommunicationStatus.NOT_ESTABLISHED,
        )

        # Create a mock future
        future = Future()
        future.set_result(
            ("mid-pss/pipeline/0001", CommunicationStatus.NOT_ESTABLISHED)
        )
        subarray_component_manager._sub_executor.submit = Mock()
        subarray_component_manager._sub_executor.submit.return_value = future

        # Execute the function
        result = subarray_component_manager._establish_pipeline_connections(
            [1]
        )

        # Verify results
        assert result == {
            "mid-pss/pipeline/0001": CommunicationStatus.NOT_ESTABLISHED
        }
        assert subarray_component_manager._assigned_pipelines_fqdns == []
        assert subarray_component_manager._assigned_pipelines_ids == []

    def test_stop_communicating(self, subarray_component_manager, callbacks):
        """
        Test for stopping communication.

        Checks that the CommunicationStatus is updated to DISABLED
        """
        subarray_component_manager._communication_state = (
            CommunicationStatus.ESTABLISHED
        )
        subarray_component_manager.stop_communicating()
        assert (
            subarray_component_manager.communication_state
            == CommunicationStatus.DISABLED
        )
        assert not subarray_component_manager.is_communicating

    def test_scan_preparation_valid_input(self, subarray_component_manager):
        """Test _scan_preparation with valid input JSON."""
        subarray_component_manager._assigned_pipelines_fqdns = [
            "mid-pss/pipeline/1234",
        ]
        test_input = {
            "common": {"subarray_id": 1},
            "pss": {
                "beam": [
                    {"beam_id": 1234, "other_data": "value1"},
                    {
                        "beam_id": 1234,
                        "other_data": "duplicate",
                    },  # Duplicate beam_id
                ]
            },
        }
        result = subarray_component_manager._scan_preparation(test_input)
        assert result is True
        assert len(subarray_component_manager.resources) == 1
        assert subarray_component_manager.resources[
            "mid-pss/pipeline/1234"
        ] == {"beam_id": 1234, "other_data": "value1"}

    def test_scan_preparation_no_matching_beams(
        self,
        subarray_component_manager,
    ):
        """Test _scan_preparation with no matching beam IDs."""
        subarray_component_manager._assigned_pipelines_fqdns = [
            "mid-pss/pipeline/1234",
        ]
        test_input = {
            "common": {"subarray_id": 1},
            "pss": {"beam": [{"beam_id": 9999, "other_data": "value1"}]},
        }

        result = subarray_component_manager._scan_preparation(test_input)

        assert result is True
        assert len(subarray_component_manager.resources) == 0

    def test_evaluate_subarray_communication_status_single_device(
        self,
        subarray_component_manager,
    ):
        """Test Subarray Communication status with a single device
        in different states."""
        # Test with single NOT_ESTABLISHED
        status_dict = {"device1": CommunicationStatus.NOT_ESTABLISHED}
        (
            comm_status,
            health,
        ) = subarray_component_manager._evaluate_subarray_communication_status(
            status_dict
        )
        assert comm_status == CommunicationStatus.NOT_ESTABLISHED
        assert health == HealthState.FAILED

        # Test with single ESTABLISHED
        status_dict = {"device1": CommunicationStatus.ESTABLISHED}
        (
            comm_status,
            health,
        ) = subarray_component_manager._evaluate_subarray_communication_status(
            status_dict
        )
        assert comm_status == CommunicationStatus.ESTABLISHED
        assert health == HealthState.OK

    def test_evaluate_subarray_communication_status_multi_devices(
        self,
        subarray_component_manager,
    ):
        """Test Subarray Communication status with
        devices in different states."""
        # All not established
        status_dict = {
            "device1": CommunicationStatus.NOT_ESTABLISHED,
            "device2": CommunicationStatus.NOT_ESTABLISHED,
            "device3": CommunicationStatus.NOT_ESTABLISHED,
        }
        (
            comm_status,
            health,
        ) = subarray_component_manager._evaluate_subarray_communication_status(
            status_dict
        )
        assert comm_status == CommunicationStatus.NOT_ESTABLISHED
        assert health == HealthState.FAILED

        # All established
        status_dict = {
            "device1": CommunicationStatus.ESTABLISHED,
            "device2": CommunicationStatus.ESTABLISHED,
            "device3": CommunicationStatus.ESTABLISHED,
        }
        (
            comm_status,
            health,
        ) = subarray_component_manager._evaluate_subarray_communication_status(
            status_dict
        )
        assert comm_status == CommunicationStatus.ESTABLISHED
        assert health == HealthState.OK

        # Mixed status
        status_dict = {
            "device1": CommunicationStatus.ESTABLISHED,
            "device2": CommunicationStatus.NOT_ESTABLISHED,
            "device3": CommunicationStatus.ESTABLISHED,
        }
        (
            comm_status,
            health,
        ) = subarray_component_manager._evaluate_subarray_communication_status(
            status_dict
        )
        assert comm_status == CommunicationStatus.ESTABLISHED
        assert health == HealthState.DEGRADED

    def test_evaluate_subarray_obsstate(
        self,
        subarray_component_manager,
    ):
        """Test Subarray _evaluate_subarray_obsstate"""
        obsstate_list = ["READY"]
        assert (
            subarray_component_manager._evaluate_subarray_obsstate(
                obsstate_list
            )
            == ObsState.READY
        )

    def test_evaluate_subarray_healthstate(
        self,
        subarray_component_manager,
    ):
        """Test Subarray _evaluate_subarray_healthstate"""
        health_list = ["OK"]
        assert (
            subarray_component_manager._evaluate_subarray_healthstate(
                health_list
            )
            == HealthState.OK
        )

    def test_incoherent_assignment(self, subarray_component_manager):
        """Test assignment of not-existing resources"""
        # Create a mock instance of the class

        # Mock pipeline_fqdns (deployed pipelines)
        subarray_component_manager.pipeline_fqdns = [
            "mid-pss/pipeline/0002",
        ]

        # Mock task_callback
        mock_task_callback = mock.MagicMock()

        # Mock _handle_command_error to capture its call
        with mock.patch.object(
            subarray_component_manager,
            "_handle_command_error",
            return_value="error_handled",
        ) as mock_handle_error:
            # Define incoherent resource assignment (pipelines_to_assign are
            # not in deployed_pipelines_ids)
            argin = {
                "common": {"subarray_id": 1},
                "pss": {"beams_id": [1]},  # Pipeline 9999 does not exist
            }

            # Call the method
            subarray_component_manager._assign(
                "AssignTask", argin, mock_task_callback
            )

            # Assert that _handle_command_error was called (meaning the error
            # was triggered)
            mock_handle_error.assert_called_once()

            # Assert that task_callback was called with IN_PROGRESS status
            mock_task_callback.assert_called_with(
                status=TaskStatus.IN_PROGRESS
            )
