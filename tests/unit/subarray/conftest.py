# -*- coding: utf-8 -*-
#
# Ported from the SKA Low MCCS project.
#
# Partially ported from the SKA Low MCCS project:
# https://gitlab.com/ska-telescope/ska-low-mccs/-/blob/main/testing/src/tests/conftest.py
#
# Distributed under the terms of the GPL license.
# See LICENSE for more info.

"""
This module defines elements of the pytest test harness shared by
subarray device tests.
"""
from __future__ import annotations

import logging
from typing import Generator, Iterator, Type

import mock
import pytest
from mock import MagicMock
from ska_control_model import (
    AdminMode,
    CommunicationStatus,
    HealthState,
    ObsState,
)
from ska_tango_testing import context
from ska_tango_testing.harness import TangoTestHarnessContext
from ska_tango_testing.integration import TangoEventTracer
from tango import DevState

from ska_pss_lmc.common.connector import Connector
from ska_pss_lmc.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_pss_lmc.subarray.subarray_component_manager import (
    PssSubarrayComponentManager,
)
from ska_pss_lmc.subarray.subarray_device import PssSubarray
from ska_pss_lmc.testing.mock.mock_device import MockDeviceBuilder

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

# pylint: disable = invalid-name


class DevProperty(ComponentManagerConfiguration):
    """
    Return a mock class for the ManagerConfiguration
    """

    def __init__(self):
        self.PipelineFqdns = [
            "mid-pss/pipeline/0001",
            # "mid-pss/pipeline/0002",
            # "mid-pss/pipeline/0003",
            # "mid-pss/pipeline/0004",
        ]
        self.PssController = "mid-pss/controller/0"
        self.ConnectionTimeout = 900
        self.PingConnectionTime = 5
        self.DefaultCommandTimeout = 10
        self.obsState = ObsState.EMPTY
        self.healthstate = HealthState.UNKNOWN
        self.resources = {}

    def get_device_properties(self):
        pass

    def add_attributes(self):
        pass


@pytest.fixture()
def subarray_component_manager(
    callbacks,
):
    """
    Return a mock PssSubarrayComponentManager.

    :param cm_configuration: the class with the device properties
    :param communication_status_changed_callback: callable invoked to
        update the communication status
    :param callbacks: callable invoked to
        update the device state
    :param update_device_property: callable invoked to update
        any device attribute
    """

    def mock_get_device(fqdn):
        _dict = {}
        for name in cm_configuration.PipelineFqdns:
            _dict[name] = mock.MagicMock
        return _dict[fqdn]

    cm_configuration = DevProperty()

    with mock.patch.object(
        Connector,
        "connect",
        return_value=CommunicationStatus.NOT_ESTABLISHED,
    ), mock.patch.object(
        Connector, "subscribe", side_effect=mock.MagicMock()
    ), mock.patch.object(
        Connector, "get_device", side_effect=mock_get_device
    ):
        yield PssSubarrayComponentManager(
            1,
            cm_configuration,
            callbacks["communication_state"],
            callbacks["component_state"],
            callbacks["update_attribute"],
            module_logger,
        )


@pytest.fixture(name="test_context")
def pss_subarray_test_context(
    patched_pss_subarray_class,
    initial_mocks: dict[str, MagicMock],
) -> Iterator[context.ThreadedTestTangoContextManager._TangoContext]:
    """
    Fixture that creates a test context for the PssSubarray.

    :param initial_mocks: A dictionary of device mocks to be added to
        the test context.
    :return: A test context for the PssController.
    """
    harness = context.ThreadedTestTangoContextManager()
    harness.add_device(
        device_class=patched_pss_subarray_class,
        device_name="mid-pss/subarray/01",
        PssController="mid-pss/controller/0",
        PipelineFqdns=[
            "mid-pss/pipeline/0001",
            # "mid-pss/pipeline/0002",
            # "mid-pss/pipeline/0003",
            # "mid-pss/pipeline/0004",
        ],
        ConnectionTimeout=900,
        PingConnectionTime=5,
        DefaultCommandTimeout=10,
    )
    for name, mock_dev in initial_mocks.items():
        harness.add_mock_device(device_name=name, device_mock=mock_dev)
    with harness as test_context:
        yield test_context


@pytest.fixture
def patched_pss_subarray_class() -> Type[PssSubarray]:
    """
    Device class with patched communication and properties.

    :return: a patched device class for testing
    """

    class PatchedPssSubarray(PssSubarray):
        """
        The patched lass used in tests.

        The ComponentManagerConfiguration as well as the SShAccess
        classes are patched.
        """

        def create_component_manager(
            self: PatchedPssSubarray,
        ) -> PssSubarrayComponentManager:
            """
            Return a partially mocked component manager instead of
            the usual one.

            :return: a mock component manager
            """
            cm_configuration = DevProperty()
            return PssSubarrayComponentManager(
                2,
                cm_configuration,
                self._communication_state_changed,
                self._component_state_changed,
                self.update_attribute,
                self.logger,
            )

    return PatchedPssSubarray


@pytest.fixture(name="device_under_test")
def device_under_test_fixture(
    test_context: TangoTestHarnessContext,
) -> context.DeviceProxy:
    """
    Fixture that returns the device under test.

    :param test_context: the context in which the tests run
    :return: the DeviceProxy for the device under test
    """
    return test_context.get_device("mid-pss/subarray/01")


@pytest.fixture(name="event_tracer", autouse=True)
def tango_event_tracer(
    device_under_test: context.DeviceProxy,
) -> Generator[TangoEventTracer, None, None]:
    """
    Fixture that returns a TangoEventTracer for pertinent devices.
    Takes as parameter all required device proxy fixtures for this test module.

    :param device_under_test: the DeviceProxy for the device under test
    :return: TangoEventTracer
    """
    tracer = TangoEventTracer()

    change_event_attr_list = [
        "longRunningCommandResult",
        "adminMode",
        "state",
        "healthstate",
        "obsstate",
    ]
    for attr in change_event_attr_list:
        tracer.subscribe_event(device_under_test, attr)

    return tracer


def mock_controller(device_name) -> mock.MagicMock:
    """Mock controller attributes"""
    builder = MockDeviceBuilder(device_name=device_name)
    builder.set_state(DevState.ON)
    builder.add_attribute("adminMode", AdminMode.OFFLINE)


def mock_pipeline(device_name) -> mock.MagicMock:
    """Mock pipeline attributes"""
    builder = MockDeviceBuilder(device_name=device_name)
    builder.set_state(DevState.OFF)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    builder.add_attribute("obsState", ObsState.IDLE)
    builder.add_attribute("subarrayMembership", 0)
    builder.add_attribute("isCommunicating", True)
    return builder()


@pytest.fixture()
def initial_mocks() -> dict[str, mock.MagicMock]:
    """
    A dictionary of proxy mocks to pre-register.
    """
    return {
        "mid-pss/controller/0": mock_controller("mid-pss/controller/0"),
        "mid-pss/pipeline/0001": mock_pipeline("mid-pss/pipeline/0001"),
        # "mid-pss/pipeline/0002": mock_pipeline("mid-pss/pipeline/0002"),
        # "mid-pss/pipeline/0003": mock_pipeline("mid-pss/pipeline/0003"),
        # "mid-pss/pipeline/0004": mock_pipeline("mid-pss/pipeline/0004"),
    }
