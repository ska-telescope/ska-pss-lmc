"""
Contains the tests for the PSS Subarray Tango
device_under_test prototype.
"""

from __future__ import annotations

import logging
from unittest.mock import MagicMock, patch

import pytest
import tango
from ska_control_model import (
    AdminMode,
    HealthState,
    ObsState,
    ResultCode,
    SimulationMode,
    TaskStatus,
)

from ska_pss_lmc.subarray.subarray_component_manager import (
    PssSubarrayComponentManager,
)
from ska_pss_lmc.testing.poller import probe_poller
from ska_pss_lmc.testing.test_utils import load_json_file

# from ska_tango_base.tests.conftest import Helpers

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)


class TestPssSubarray:
    """Tests of the Subarray device."""

    def test_subarray_device(
        self: TestPssSubarray,
        device_under_test,
    ) -> None:
        """
        Test for startup attributes and properties.

        :param device_under_test: fixture that provides a
            :py:class:'tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.healthState == HealthState.UNKNOWN
        assert device_under_test.simulationMode == SimulationMode.FALSE
        assert device_under_test.State() == tango.DevState.DISABLE
        assert device_under_test.adminMode == AdminMode.OFFLINE
        assert device_under_test.Status() == "The device is in DISABLE state."

    def test_initialize_subarray(
        self: TestPssSubarray,
        device_under_test,
    ) -> None:
        """
        Test for initialization.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        device_under_test.adminMode = AdminMode.ONLINE
        # Subarray device will establish communication on AssignResources
        probe_poller(device_under_test, "isCommunicating", False, time=2)
        probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
        assert device_under_test.healthstate == HealthState.UNKNOWN
        assert device_under_test.pipelinesFqdns == ("mid-pss/pipeline/0001",)

    def test_subarray_power_commands(
        self: TestPssSubarray,
        device_under_test,
    ) -> None:
        """
        Test power commands are rejected.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "isCommunicating", False, time=2)
        probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
        status, _ = device_under_test.On()
        assert status[0] == ResultCode.REJECTED
        status, _ = device_under_test.Standby()
        assert status[0] == ResultCode.REJECTED
        status, _ = device_under_test.Off()
        assert status[0] == ResultCode.REJECTED

    # --------------------------ASSIGN RESOURCES--------------------------

    def test_assignresources_with_empty_json(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray AssignResources with wrong configuration.

        Expected result: PSS Subarray goes into FAULT.
        """
        assert device_under_test.obsstate == ObsState.EMPTY
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "state", tango.DevState.ON, time=2)

        _, command_id = device_under_test.AssignResources("{}")
        probe_poller(
            device_under_test,
            "longRunningCommandStatus",
            (command_id[0], "FAILED"),
            time=5,
        )
        assert device_under_test.obsstate == ObsState.FAULT

    def test_assignresources_with_invalid_json(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray AssignResources with wrong configuration.

        Expected result: PSS Subarray maintains its observing state.
        """
        assert device_under_test.obsstate == ObsState.EMPTY
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "state", tango.DevState.ON, time=2)

        with pytest.raises(Exception):
            _, _ = device_under_test.AssignResources('{"subarray_id"}')
        assert device_under_test.obsstate == ObsState.EMPTY

    def test_assignresources_success(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray AssignResources on a happy path.
        """
        assert device_under_test.obsstate == ObsState.EMPTY
        assert device_under_test.healthstate == HealthState.UNKNOWN
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
        # Issue AssignResources
        _, command_id = device_under_test.AssignResources(
            load_json_file("AssignResources_basic.json", string=True)
        )
        probe_poller(
            device_under_test,
            "longRunningCommandStatus",
            (command_id[0], "COMPLETED"),
            time=5,
        )
        assert device_under_test.pipelinesobsstate == ("IDLE",)
        assert device_under_test.pipelinesiscommunicating.all()
        assert device_under_test.pipelinesstate == ("OFF",)
        assert device_under_test.pipelineshealthstate == ("OK",)
        assert device_under_test.pipelinesadminmode == ("ONLINE",)
        assert device_under_test.pipelinesFqdns == ("mid-pss/pipeline/0001",)
        assert device_under_test.healthstate == HealthState.OK
        assert device_under_test.obsstate == ObsState.IDLE
        assert device_under_test.assignedPipelinesIDs == [1]
        assert list(device_under_test.assignedPipelinesFqdns) == [
            "mid-pss/pipeline/0001"
        ]

    # --------------------------RELEASE ALL--------------------------

    def test_release_all_resources_success(
        self: TestPssSubarray,
        device_under_test,
    ):
        """Test the PSS Subarray Release-All Command happy path"""
        assert device_under_test.obsstate == ObsState.EMPTY
        assert device_under_test.healthstate == HealthState.UNKNOWN
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
        _, command_id1 = device_under_test.AssignResources(
            load_json_file("AssignResources_basic.json", string=True)
        )
        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)

        # issue the command
        _, command_id2 = device_under_test.ReleaseAllResources()
        # verify the results
        probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=5)
        probe_poller(
            device_under_test,
            "longRunningCommandStatus",
            (command_id1[0], "COMPLETED", command_id2[0], "COMPLETED"),
            time=10,
        )
        # Check empty lists
        assert not list(device_under_test.assignedPipelinesIDs)
        assert not list(device_under_test.assignedPipelinesFqdns)

    # --------------------------CONFIGURE--------------------------

    def test_configure_with_empty_json(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray Configure with wrong configuration.

        Expected result: PSS Subarray goes into FAULT.
        """
        assert device_under_test.obsstate == ObsState.EMPTY
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
        _, command_id1 = device_under_test.AssignResources(
            load_json_file("AssignResources_basic.json", string=True)
        )
        probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)
        _, command_id2 = device_under_test.configure("{}")

        probe_poller(
            device_under_test,
            "longRunningCommandStatus",
            (command_id1[0], "COMPLETED", command_id2[0], "FAILED"),
            time=10,
        )
        assert device_under_test.obsstate == ObsState.FAULT

    def test_configure_not_allowed_obsstate(
        self: TestPssSubarray,
        device_under_test,
    ):
        """Test PSS Subarray configure command when invoked from an obsstate
        not allowed."""
        assert device_under_test.obsstate == ObsState.EMPTY
        assert device_under_test.healthstate == HealthState.UNKNOWN
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=10)
        config_input = load_json_file("Configure_basic.json", string=True)
        with pytest.raises(tango.DevFailed):
            device_under_test.configure(config_input)

    def test_configure_success(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray Configure on a happy path.
        """
        mock_proxy = MagicMock()
        mock_pipelines_proxy = {"mid-pss/pipeline/0001": mock_proxy}
        mock_proxy.obsState = ObsState.READY

        # Set up the mock to call the callback with success status
        def fake_invoke_lrc(
            lrc_callback, proxy, command, command_args, logger=module_logger
        ):
            # Simulate successful completion
            lrc_callback(
                status=TaskStatus.COMPLETED,
                result=[ResultCode.OK, "Config Command completed"],
                logger=logger,
                task_name="configure",
            )
            # Return an empty list as the subscription object
            return [MagicMock()]

        # Patch where it's actually used in the component manager
        with patch(
            "ska_pss_lmc.subarray.subarray_component_manager.invoke_lrc",
            side_effect=fake_invoke_lrc,
        ), patch.object(
            PssSubarrayComponentManager,
            "_get_pipelines_proxy",
            return_value=mock_pipelines_proxy,
        ), patch.object(
            PssSubarrayComponentManager,
            "_evaluate_subarray_obsstate",
            return_value=ObsState.READY,
        ):
            assert device_under_test.obsstate == ObsState.EMPTY
            assert device_under_test.healthstate == HealthState.UNKNOWN
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
            _, command_id1 = device_under_test.AssignResources(
                load_json_file("AssignResources_basic.json", string=True)
            )
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=3)
            config_input = load_json_file("Configure_basic.json", string=True)
            _, command_id2 = device_under_test.configure(config_input)
            probe_poller(
                device_under_test,
                "longRunningCommandStatus",
                (command_id1[0], "COMPLETED", command_id2[0], "COMPLETED"),
                time=10,
            )
            # Verify transition to READY state
            probe_poller(device_under_test, "obsstate", ObsState.READY, time=2)

    # --------------------------SCAN--------------------------

    def test_scan_with_empty_json(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray Scan with wrong configuration.

        Expected result: PSS Subarray goes into FAULT.
        """
        mock_proxy = MagicMock()
        mock_pipelines_proxy = {"mid-pss/pipeline/0001": mock_proxy}
        mock_proxy.obsState = ObsState.SCANNING

        # Set up the mock to call the callback with success status
        def fake_invoke_lrc(
            lrc_callback, proxy, command, command_args, logger
        ):
            # Simulate successful completion
            lrc_callback(
                TaskStatus.COMPLETED,
                result=[ResultCode.OK, "Scan Command completed"],
                logger=logger,
                task_name="scan",
            )
            # Return an empty list as the subscription object
            return [MagicMock()]

        # Patch where it's actually used in the component manager
        with patch(
            "ska_pss_lmc.subarray.subarray_component_manager.invoke_lrc",
            side_effect=fake_invoke_lrc,
        ), patch.object(
            PssSubarrayComponentManager,
            "_get_pipelines_proxy",
            return_value=mock_pipelines_proxy,
        ):
            assert device_under_test.obsstate == ObsState.EMPTY
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
            _, command_id1 = device_under_test.AssignResources(
                load_json_file("AssignResources_basic.json", string=True)
            )
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)
            _, command_id2 = device_under_test.configure(
                load_json_file("Configure_basic.json", string=True)
            )
            probe_poller(
                device_under_test, "obsstate", ObsState.READY, time=10
            )
            _, command_id3 = device_under_test.scan("{}")

            probe_poller(
                device_under_test,
                "longRunningCommandStatus",
                (
                    command_id1[0],
                    "COMPLETED",
                    command_id2[0],
                    "COMPLETED",
                    command_id3[0],
                    "FAILED",
                ),
                time=10,
            )
            assert device_under_test.obsstate == ObsState.FAULT

    def test_scan_success(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray Scan on a happy path.
        """
        mock_proxy = MagicMock()
        mock_pipelines_proxy = {"mid-pss/pipeline/0001": mock_proxy}
        mock_proxy.obsState = ObsState.SCANNING

        # Set up the mock to call the callback with success status
        def fake_invoke_lrc(
            lrc_callback, proxy, command, command_args, logger
        ):
            # Simulate successful completion
            lrc_callback(
                TaskStatus.COMPLETED,
                result=[ResultCode.OK, "Scan Command completed"],
                logger=logger,
                task_name="scan",
            )
            # Return an empty list as the subscription object
            return [MagicMock()]

        # Patch where it's actually used in the component manager
        with patch(
            "ska_pss_lmc.subarray.subarray_component_manager.invoke_lrc",
            side_effect=fake_invoke_lrc,
        ), patch.object(
            PssSubarrayComponentManager,
            "_get_pipelines_proxy",
            return_value=mock_pipelines_proxy,
        ), patch.object(
            PssSubarrayComponentManager,
            "_evaluate_subarray_obsstate",
            return_value=ObsState.SCANNING,
        ):
            assert device_under_test.obsstate == ObsState.EMPTY
            assert device_under_test.healthstate == HealthState.UNKNOWN
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
            _, command_id1 = device_under_test.AssignResources(
                load_json_file("AssignResources_basic.json", string=True)
            )
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)
            config_input = load_json_file("Configure_basic.json", string=True)
            _, command_id2 = device_under_test.configure(config_input)
            probe_poller(
                device_under_test, "obsstate", ObsState.READY, time=10
            )
            scan_input = load_json_file("Scan_basic.json", string=True)
            _, command_id3 = device_under_test.scan(scan_input)
            probe_poller(
                device_under_test,
                "longRunningCommandStatus",
                (
                    command_id1[0],
                    "COMPLETED",
                    command_id2[0],
                    "COMPLETED",
                    command_id3[0],
                    "COMPLETED",
                ),
                time=10,
            )
            probe_poller(
                device_under_test, "obsstate", ObsState.SCANNING, time=10
            )

    # ------------------------END SCAN--------------------------

    def test_endscan_success(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray EndScan on a happy path.
        """
        mock_proxy = MagicMock()
        mock_pipelines_proxy = {"mid-pss/pipeline/0001": mock_proxy}
        mock_proxy.obsState = ObsState.SCANNING

        # Set up the mock to call the callback with success status
        def fake_invoke_lrc(
            lrc_callback, proxy, command, logger=module_logger
        ):
            # Simulate successful completion
            lrc_callback(
                TaskStatus.COMPLETED,
                result=[ResultCode.OK, "End Scan Command completed"],
                logger=logger,
            )
            # Return an empty list as the subscription object
            return [MagicMock()]

        # Patch where it's actually used in the component manager
        with patch(
            "ska_pss_lmc.subarray.subarray_component_manager.invoke_lrc",
            side_effect=fake_invoke_lrc,
        ), patch.object(
            PssSubarrayComponentManager,
            "_get_pipelines_proxy",
            return_value=mock_pipelines_proxy,
        ):
            assert device_under_test.obsstate == ObsState.EMPTY
            assert device_under_test.healthstate == HealthState.UNKNOWN
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
            _, command_id1 = device_under_test.AssignResources(
                load_json_file("AssignResources_basic.json", string=True)
            )
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)
            config_input = load_json_file("Configure_basic.json", string=True)
            _, command_id2 = device_under_test.configure(config_input)
            probe_poller(
                device_under_test, "obsstate", ObsState.READY, time=10
            )
            scan_input = load_json_file("Scan_basic.json", string=True)
            _, command_id3 = device_under_test.scan(scan_input)
            probe_poller(
                device_under_test, "obsstate", ObsState.SCANNING, time=10
            )
            _, command_id4 = device_under_test.endscan()
            probe_poller(
                device_under_test,
                "longRunningCommandStatus",
                (
                    command_id1[0],
                    "COMPLETED",
                    command_id2[0],
                    "COMPLETED",
                    command_id3[0],
                    "COMPLETED",
                    command_id4[0],
                    "COMPLETED",
                ),
                time=10,
            )
            probe_poller(
                device_under_test, "obsstate", ObsState.READY, time=10
            )

    # ------------------------GO TO IDLE--------------------------

    def test_gotoidle_success(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray GoToIdle on a happy path.
        """
        mock_proxy = MagicMock()
        mock_pipelines_proxy = {"mid-pss/pipeline/0001": mock_proxy}
        mock_proxy.obsState = ObsState.SCANNING

        # Set up the mock to call the callback with success status
        def fake_invoke_lrc(lrc_callback, proxy, command, command_args=None):
            # Simulate successful completion
            lrc_callback(
                TaskStatus.COMPLETED,
                result=[ResultCode.OK, "End Scan Command completed"],
            )
            # Return an empty list as the subscription object
            return [MagicMock()]

        # Patch where it's actually used in the component manager
        with patch(
            "ska_pss_lmc.subarray.subarray_component_manager.invoke_lrc",
            side_effect=fake_invoke_lrc,
        ), patch.object(
            PssSubarrayComponentManager,
            "_get_pipelines_proxy",
            return_value=mock_pipelines_proxy,
        ):
            assert device_under_test.obsstate == ObsState.EMPTY
            assert device_under_test.healthstate == HealthState.UNKNOWN
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
            _, command_id1 = device_under_test.AssignResources(
                load_json_file("AssignResources_basic.json", string=True)
            )
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)
            config_input = load_json_file("Configure_basic.json", string=True)
            _, command_id2 = device_under_test.configure(config_input)
            probe_poller(
                device_under_test, "obsstate", ObsState.READY, time=10
            )
            scan_input = load_json_file("Scan_basic.json", string=True)
            _, command_id3 = device_under_test.scan(scan_input)
            probe_poller(
                device_under_test, "obsstate", ObsState.SCANNING, time=10
            )
            _, command_id4 = device_under_test.endscan()
            probe_poller(
                device_under_test, "obsstate", ObsState.READY, time=10
            )
            _, command_id5 = device_under_test.gotoidle()
            probe_poller(
                device_under_test,
                "longRunningCommandStatus",
                (
                    command_id1[0],
                    "COMPLETED",
                    command_id2[0],
                    "COMPLETED",
                    command_id3[0],
                    "COMPLETED",
                    command_id4[0],
                    "COMPLETED",
                    command_id5[0],
                    "COMPLETED",
                ),
                time=10,
            )
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)

    # ------------------------RELEASE ALL--------------------------

    def test_releaseall_success(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray ReleaseAll on a happy path.
        """
        mock_proxy = MagicMock()
        mock_pipelines_proxy = {"mid-pss/pipeline/0001": mock_proxy}
        mock_proxy.obsState = ObsState.SCANNING

        # Set up the mock to call the callback with success status
        def fake_invoke_lrc(
            lrc_callback,
            proxy,
            command,
            command_args=None,
            logger=module_logger,
        ):
            # Simulate successful completion
            lrc_callback(
                TaskStatus.COMPLETED,
                result=[ResultCode.OK, "ReleaseAll Command completed"],
                logger=logger,
                task_name="releaseall",
            )
            # Return an empty list as the subscription object
            return [MagicMock()]

        # Patch where it's actually used in the component manager
        with patch(
            "ska_pss_lmc.subarray.subarray_component_manager.invoke_lrc",
            side_effect=fake_invoke_lrc,
        ), patch.object(
            PssSubarrayComponentManager,
            "_get_pipelines_proxy",
            return_value=mock_pipelines_proxy,
        ):
            assert device_under_test.obsstate == ObsState.EMPTY
            assert device_under_test.healthstate == HealthState.UNKNOWN
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
            _, command_id1 = device_under_test.AssignResources(
                load_json_file("AssignResources_basic.json", string=True)
            )
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)
            config_input = load_json_file("Configure_basic.json", string=True)
            _, command_id2 = device_under_test.configure(config_input)
            probe_poller(
                device_under_test, "obsstate", ObsState.READY, time=10
            )
            scan_input = load_json_file("Scan_basic.json", string=True)
            _, command_id3 = device_under_test.scan(scan_input)
            probe_poller(
                device_under_test, "obsstate", ObsState.SCANNING, time=10
            )
            _, command_id4 = device_under_test.endscan()
            probe_poller(
                device_under_test, "obsstate", ObsState.READY, time=10
            )
            _, command_id5 = device_under_test.gotoidle()
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=10)
            _, command_id6 = device_under_test.releaseallresources()
            probe_poller(
                device_under_test,
                "longRunningCommandStatus",
                (
                    command_id1[0],
                    "COMPLETED",
                    command_id2[0],
                    "COMPLETED",
                    command_id3[0],
                    "COMPLETED",
                    command_id4[0],
                    "COMPLETED",
                    command_id5[0],
                    "COMPLETED",
                    command_id6[0],
                    "COMPLETED",
                ),
                time=10,
            )
            probe_poller(
                device_under_test, "obsstate", ObsState.EMPTY, time=10
            )

    # ------------------------RESTART--------------------------

    def test_restart_success(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray Restart on a happy path.
        """
        mock_proxy = MagicMock()
        mock_pipelines_proxy = {"mid-pss/pipeline/0001": mock_proxy}
        mock_proxy.obsState = ObsState.READY

        # Set up the mock to call the callback with success status
        def fake_invoke_lrc(
            lrc_callback,
            proxy,
            command,
            command_args=None,
            logger=module_logger,
        ):
            # Simulate successful completion
            lrc_callback(
                TaskStatus.COMPLETED,
                result=[ResultCode.OK, "Restart Command completed"],
                logger=logger,
                task_name="obsreset",
            )
            # Return an empty list as the subscription object
            return [MagicMock()]

        # Patch where it's actually used in the component manager
        with patch(
            "ska_pss_lmc.subarray.subarray_component_manager.invoke_lrc",
            side_effect=fake_invoke_lrc,
        ), patch.object(
            PssSubarrayComponentManager,
            "_get_pipelines_proxy",
            return_value=mock_pipelines_proxy,
        ):
            assert device_under_test.obsstate == ObsState.EMPTY
            assert device_under_test.healthstate == HealthState.UNKNOWN
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
            _, command_id1 = device_under_test.AssignResources(
                load_json_file("AssignResources_basic.json", string=True)
            )
            probe_poller(device_under_test, "obsstate", ObsState.IDLE, time=5)
            assert device_under_test.assignedPipelinesIDs == [1]
            _, command_id2 = device_under_test.configure("{}")
            probe_poller(device_under_test, "obsstate", ObsState.FAULT, time=5)
            _, command_id3 = device_under_test.Restart()

            probe_poller(device_under_test, "obsstate", ObsState.EMPTY, time=3)
            assert not list(device_under_test.assignedPipelinesIDs)

    # --------------------------ABORT --------------------------

    def test_abort_success(
        self: TestPssSubarray,
        device_under_test,
    ):
        """
        Test the PSS Subarray Restart on a happy path.
        """
        mock_proxy = MagicMock()
        mock_pipelines_proxy = {"mid-pss/pipeline/0001": mock_proxy}
        mock_proxy.obsState = ObsState.ABORTED

        # Set up the mock to call the callback with success status
        def fake_invoke_lrc(
            lrc_callback,
            proxy,
            command,
            command_args=None,
            logger=module_logger,
        ):
            # Simulate successful completion
            lrc_callback(
                TaskStatus.COMPLETED,
                result=[ResultCode.OK, f"{command}completed"],
                logger=logger,
                task_name="abort",
            )
            # Return an empty list as the subscription object
            return [MagicMock()]

        # Patch where it's actually used in the component manager
        with patch(
            "ska_pss_lmc.subarray.subarray_component_manager.invoke_lrc",
            side_effect=fake_invoke_lrc,
        ), patch.object(
            PssSubarrayComponentManager,
            "_get_pipelines_proxy",
            return_value=mock_pipelines_proxy,
        ):
            assert device_under_test.obsstate == ObsState.EMPTY
            assert device_under_test.healthstate == HealthState.UNKNOWN
            device_under_test.adminMode = AdminMode.ONLINE
            assert device_under_test.state() == tango.DevState.ON
            device_under_test.AssignResources(
                load_json_file("AssignResources_basic.json", string=True)
            )
            probe_poller(device_under_test, "isCommunicating", True, time=5)
            config_input = load_json_file("Configure_basic.json", string=True)
            device_under_test.configure(config_input)
            probe_poller(device_under_test, "obsstate", ObsState.READY, time=5)
            device_under_test.Abort()

            probe_poller(
                device_under_test, "obsstate", ObsState.ABORTED, time=3
            )
