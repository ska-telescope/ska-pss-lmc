# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarray project
#
# INAF, Cosylab Switzerland - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""
Test PipelineCommunicationManager
"""

from __future__ import annotations

import io
import logging
import xml.etree.ElementTree as ET

import pytest
from mock import Mock, patch

from ska_pss_lmc.manager.communication_manager import SshAccess

module_logger = logging.getLogger(__name__)


@pytest.fixture(scope="function")
def ssh_access() -> SshAccess:
    """
    Fixture that returns ssh access object with mocked paramiko client using
    password authentication.

    :return: SSHAccess instance with mocked SSHClient
    """
    with patch("paramiko.client.SSHClient"):

        ssh_access = SshAccess(
            host="192.168.1.45",
            user="user",
        )
        ssh_access.client.exec_command.side_effect = exec_command_response
        return ssh_access


@pytest.fixture(scope="function")
def ssh_access_with_pkey() -> SshAccess:
    """
    Fixture that returns ssh access object with mocked paramiko client using
    private key authentication.

    :return: SSHAccess instance with mocked SSHClient
    """
    with patch("paramiko.client.SSHClient"):
        private_key_example = """\
        b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAA\
        MwAAAAtzc2gtZWQyNTUxOQAAACCcCvBqCDRVefOLAt/WZ6kfagd304UK\
        74S3/4s11gu6VAAAAJjmB0Es5gdBLAAAAAtzc2gtZWQyNTUxOQAAACCc\
        CvBqCDRVefOLAt/WZ6kfagd304UK74S3/4s11gu6VAAAAEBKh6fa/xa7\
        W5A4pQIITRcjxR2VtxiqHR2dQemMP3YdJJwK8GoINFV584sC39ZnqR9q\
        B3fThQrvhLf/izXWC7pUAAAAFHVidW50dUBic2hhdy1jaGVldGFoAQ=="""

        ssh_access = SshAccess(
            host="192.168.1.45",
            user="user",
            private_key=private_key_example,
        )
        ssh_access.client.exec_command.side_effect = exec_command_response
        return ssh_access


def exec_command_response(argin: str, get_pty: bool = None):
    """
    Method that returns the device under test.

    :param tango_harness: a test harness for Tango devices

    :return: the device under test
    """
    if argin == "cat pid.txt":
        return (None, io.StringIO("1234\n"), None)
    elif argin == "kill -s 0 1234 && echo RUNNING":
        return (None, io.StringIO("RUNNING\n"), None)
    elif argin == "kill 1234":
        return (None, Mock(), None)
    elif argin == "tail -f cheetah.log" and get_pty:
        return (Mock(), io.StringIO("loglog\n"), io.StringIO("\n"))


class TestPipelineCommunicationManagerSSH:
    """
    Test cheetah ssh access
    """

    def test_connect(
        self: TestPipelineCommunicationManagerSSH, ssh_access: SshAccess
    ) -> None:
        """
        Test connect to ssh server with password.

        :param ssh_access: a fixture that provides a :py:class:`SSHAccess`
            with mocked SSHClient
        """
        assert ssh_access.connect()
        assert ssh_access.cheetah_pid == 1234
        assert ssh_access.is_running()

    def test_connect_with_pkey(
        self: TestPipelineCommunicationManagerSSH,
        ssh_access_with_pkey: SshAccess,
    ) -> None:
        """
        Test connect to ssh server with private key.

        :param ssh_access: a fixture that provides a :py:class:`SSHAccess`
            with mocked SSHClient
        """
        assert ssh_access_with_pkey.connect()
        assert ssh_access_with_pkey.cheetah_pid == 1234
        assert ssh_access_with_pkey.is_running()

    def test_disconnect(
        self: TestPipelineCommunicationManagerSSH, ssh_access: SshAccess
    ) -> None:
        """
        Test disconnect from ssh server.

        :param ssh_access: a fixture that provides a :py:class:`SSHAccess`
            with mocked SSHClient
        """
        ssh_access.connect()
        ssh_access.disconnect()
        ssh_access.client.close.assert_called_once()

    def test_start(
        self: TestPipelineCommunicationManagerSSH, ssh_access: SshAccess
    ) -> None:
        """
        Test command to start cheetah.

        :param ssh_access: a fixture that provides a :py:class:`SSHAccess`
            with mocked SSHClient
        """
        ssh_access.connect()
        cmd = "tail -f /var/log/syslog"
        ssh_access.client.exec_command.reset_mock()
        ssh_access.start(cmd)
        ssh_access.client.exec_command.assert_any_call(
            f"nohup {cmd} > cheetah.log 2>&1 & echo $! > pid.txt; "
            f"wait $(cat pid.txt); echo $? > exit_status.txt"
        )

    def test_start_kill(
        self: TestPipelineCommunicationManagerSSH, ssh_access: SshAccess
    ) -> None:
        """
        Test command to kill cheetah.

        :param ssh_access: a fixture that provides a :py:class:`SSHAccess`
            with mocked SSHClient
        """
        ssh_access.connect()
        cmd = "tail -f /var/log/syslog"
        ssh_access.client.exec_command.reset_mock()
        ssh_access.start(cmd)
        assert ssh_access.is_running()
        ssh_access.kill()
        assert not ssh_access.is_running()
        ssh_access.disconnect()

    def test_get_logs(
        self: TestPipelineCommunicationManagerSSH, ssh_access: SshAccess
    ) -> None:
        """
        Test command to retrieve the logs.

        :param ssh_access: a fixture that provides a :py:class:`SSHAccess`
            with mocked SSHClient
        """
        ssh_access.connect()
        ssh_access.client.exec_command.reset_mock()
        line = next(ssh_access.get_logs())
        assert line == "loglog\n"

    @pytest.mark.skip
    # close logs is removed, new test for mechanism could be tested instead
    def test_close_logs(
        self: TestPipelineCommunicationManagerSSH, ssh_access: SshAccess
    ) -> None:
        """
        Test command to close the logs.

        :param ssh_access: a fixture that provides a :py:class:`SSHAccess`
            with mocked SSHClient
        """
        ssh_access.connect()
        ssh_access.client.exec_command.reset_mock()
        next(ssh_access.get_logs())
        ssh_access.close_logs()
        ssh_access.log_input.write.assert_called_once_with("\x03")
        ssh_access.log_input.flush.assert_called_once()

    def test_write_config(
        self: TestPipelineCommunicationManagerSSH, ssh_access: SshAccess
    ) -> None:
        """
        Test command to write configuration.

        :param ssh_access: a fixture that provides a :py:class:`SSHAccess`
            with mocked SSHClient
        """
        ssh_access.connect()
        sftp = Mock()
        ssh_access.client.open_sftp.return_value = sftp
        cfg = sftp.file.return_value = Mock()

        root = ET.parse("tests/resources/sample.xml").getroot()
        config = ET.tostring(root)
        ssh_access.write_config(
            config,
        )

        cfg.write.assert_called_once_with(config)
        cfg.flush.assert_called_once()
        sftp.close.assert_called_once()
