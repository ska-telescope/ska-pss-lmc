# -*- coding: utf-8 -*-
#
# Ported from the SKA Low MCCS project.
#
# Partially ported from the SKA Low MCCS project:
# https://gitlab.com/ska-telescope/ska-low-mccs/-/blob/main/testing/src/tests/conftest.py
#
# Distributed under the terms of the GPL license.
# See LICENSE for more info.

from __future__ import annotations

import logging

# Standard imports
from typing import Callable, Final, Type

import mock
import pytest
from ska_csp_lmc_base.obs.obs_device import (
    ComponentManagerT,
    CspSubElementObsDevice,
)
from ska_tango_base.base import CommandTracker

from ska_pss_lmc.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_pss_lmc.pipeline.pipeline_component_manager import (
    PipelineCtrlComponentManager,
)
from ska_pss_lmc.pipeline.pipeline_ctrl_device import PipelineCtrlDevice
from ska_pss_lmc.testing.pipeline.ssh_access import SshAccessPatched

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)


class DevProperty(ComponentManagerConfiguration):
    """
    Return a mock class for the ManagerConfiguration
    """

    def __init__(self):
        self.CheetahExecutable = "python3 simulcheetah.py"
        self.NodeIP = "192.168.17.1"
        self.PipelineName = "node-ctrl-00-01-a"
        self.CheetahOutputFile = "tmp/cheetah-00-01-a.log"
        self.CheetahPipelineSource = "udp_low"
        self.CheetahPipelineType = "Empty"
        self.CheetahLogLevel = "info"

    def get_device_properties(self):
        pass

    def add_attributes(self):
        pass


class PipelineComponentManagerPatch(PipelineCtrlComponentManager):
    """
    Return a PipelineComponentManager with a patched communication.
    """

    def create_communication_manager(self, logger):
        return SshAccessPatched()


@pytest.fixture
def patched_controller_device_class() -> Type[PipelineCtrlDevice]:
    """
    Device class with patched communication and properties.

    :return: a patched device class for testing
    """

    class PatchedControllerDevice(PipelineCtrlDevice):
        """
        The patched lass used in tests.

        The ComponentManagerConfiguration as well as the SShAccess
        classes are patched.
        """

        def create_component_manager(
            self: PatchedControllerDevice,
        ) -> PipelineCtrlComponentManager:
            """
            Return a partially mocked component manager instead of
            the usual one.

            :return: a mock component manager
            """
            cm_configuration = DevProperty()
            return PipelineComponentManagerPatch(
                2,
                cm_configuration,
                self._communication_state_changed,
                self._component_state_changed,
                self.update_attribute,
                self.logger,
            )

        class ConfigureScanCommand(
            CspSubElementObsDevice.ConfigureScanCommand
        ):
            """A class for SKASubarray's Configure() command."""

            SCHEMA: Final = {
                # pylint: disable=line-too-long
                "$schema": "https://json-schema.org/draft/2020-12/schema",
                "$id": "https://skao.int/ska-tango-base/ReferenceSkaObsDevice_Configure.json",  # noqa: E501
                "title": "ska-tango-base ReferenceSkaObsDevice Configure schema",
                "description": "Schema for ska-tango-base ReferenceSkaObsDevice Configure command",  # noqa: E501
                "type": "object",
                "properties": {
                    "config_id": {
                        "description": "Configuration id",
                        "type": "string",
                    }
                },
                "required": [
                    "config_id",
                ],
            }

            def __init__(
                self: PatchedControllerDevice.ConfigureScanCommand,
                command_tracker: CommandTracker,
                component_manager: ComponentManagerT,
                callback: Callable[[bool], None] | None = None,
                logger: logging.Logger | None = None,
            ) -> None:
                """
                Initialise a new instance.

                :param command_tracker: the device's command tracker
                :param component_manager: the device's component manager
                :param callback: an optional callback to be called when this
                    command starts and finishes.
                :param logger: a logger for this command to log with.
                """
                super().__init__(
                    command_tracker,
                    component_manager,
                    callback=callback,
                    logger=logger,
                    schema=self.SCHEMA,
                )

        class ScanCommand(CspSubElementObsDevice.ScanCommand):
            """A class for SKASubarray's Configure() command."""

            SCHEMA: Final = {
                # pylint: disable=line-too-long
                "$schema": "https://json-schema.org/draft/2020-12/schema",
                "$id": "https://skao.int/ska-tango-base/ReferenceSkaObsDevice_Scan.json",  # noqa: E501
                "title": "ska-tango-base ReferenceSkaObsDevice Configure schema",
                "description": "Schema for ska-tango-base ReferenceSkaObsDevice Scan command",  # noqa: E501
                "type": "object",
                "properties": {
                    "scan_id": {
                        "description": "Scan id",
                        "type": "integer",
                    }
                },
                "required": [
                    "scan_id",
                ],
            }

            def __init__(
                self: CspSubElementObsDevice.ScanCommand,
                command_tracker: CommandTracker,
                component_manager: ComponentManagerT,
                callback: Callable[[bool], None] | None = None,
                logger: logging.Logger | None = None,
            ) -> None:
                """
                Initialise a new instance.

                :param command_tracker: the device's command tracker
                :param component_manager: the device's component manager
                :param callback: an optional callback to be called when this
                    command starts and finishes.
                :param logger: a logger for this command to log with.
                """
                super().__init__(
                    command_tracker,
                    component_manager,
                    callback=callback,
                    logger=logger,
                    schema=self.SCHEMA,
                )

    return PatchedControllerDevice


def mock_create_communication(*args):
    """
    Return the mocked communication class.
    """
    m = SshAccessPatched()
    return m


@pytest.fixture()
def pipe_ctrl_comp_man(
    callbacks,
):
    """
    Return a mock PssCtrlPipeline component manager.

    :param cm_configuration: the class with the device properties
    :param communication_status_changed_callback: callable invoked to
        update the communication status
    :param callbacks: callable invoked to
        update the device state
    :param update_device_property: callable invoked to update
        any device attribute
    """

    cm_configuration = DevProperty()
    with mock.patch.object(
        PipelineCtrlComponentManager,
        "create_communication_manager",
        side_effect=mock_create_communication,
    ):
        yield PipelineCtrlComponentManager(
            2,
            cm_configuration,
            callbacks["communication_state"],
            callbacks["component_state"],
            callbacks["update_attribute"],
            module_logger,
        )
