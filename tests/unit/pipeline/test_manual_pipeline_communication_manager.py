# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarray project
#
# INAF, Cosylab Switzerland - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""
Manual Test PipelineCommunicationManager
"""
import xml.etree.ElementTree as ET

import pytest

from ska_pss_lmc.manager.communication_manager import SshAccess


@pytest.mark.skip(reason="does not mock connection")
class TestManualPipelineCommunicationManagerSSH:
    """
    Test cheetah ssh access
    """

    host = "192.168.1.45"
    user = "alexander"
    private_key_path = "/root/.ssh/id_rsa"

    """
    def test_connect_ssh_pw(self):
        pcm = SshAccess(host=self.host,
            user=self.user, password="")
        assert pcm.connect()
        pcm.start("tail -f /var/log/syslog")
        assert pcm.is_running()
        pcm.kill()
        assert not pcm.is_running()
        pcm.disconnect()
    """

    def test_connect_ssh_key(self):
        """
        Connect with ssh key
        """
        pcm = SshAccess(
            host=self.host,
            user=self.user,
            private_key_path=self.private_key_path,
        )
        assert pcm.connect()
        pcm.disconnect()

    def test_start_kill(self):
        """
        Test start and kill
        """
        pcm = SshAccess(
            host=self.host,
            user=self.user,
            private_key_path=self.private_key_path,
        )
        assert pcm.connect()
        pcm.start("tail -f /var/log/syslog")
        assert pcm.is_running()
        pcm.kill()
        assert not pcm.is_running()
        pcm.disconnect()

    def test_logs(self):
        """
        Test getting the logs via the iterator
        """
        pcm = SshAccess(
            host=self.host,
            user=self.user,
            private_key_path=self.private_key_path,
        )
        assert pcm.connect()
        pcm.start("tail -f /var/log/syslog")
        assert pcm.is_running()
        maximum = 5
        count = 0
        for line in pcm.get_logs():
            print(line, end="")
            count += 1
            if count > maximum:
                pcm.close_logs()
                break
        pcm.kill()
        assert not pcm.is_running()
        pcm.disconnect()

    def test_write_config(self):
        """
        Write config
        """
        pcm = SshAccess(
            host=self.host,
            user=self.user,
            private_key_path=self.private_key_path,
        )
        assert pcm.connect()
        root = ET.parse("tests/resources/sample.xml").getroot()
        file_data = ET.tostring(root)
        pcm.write_config(file_data)
        pcm.disconnect()

    def test_reestablish_connection(self):
        """
        Re-establish the connection, and check that process can be found
        """
        pcm = SshAccess(
            host=self.host,
            user=self.user,
            private_key_path=self.private_key_path,
        )
        assert pcm.connect()
        pcm.start("tail -f /var/log/syslog")
        assert pcm.is_running()
        # delete communication to simulate a restart
        del pcm
        pcm = SshAccess(
            host=self.host,
            user=self.user,
            private_key_path=self.private_key_path,
        )
        assert pcm.connect()
        assert pcm.is_running()
        pcm.kill()
        assert not pcm.is_running()
        pcm.disconnect()
