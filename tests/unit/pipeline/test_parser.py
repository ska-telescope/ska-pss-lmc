# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
from __future__ import annotations

import logging
import queue
import sys
import time

import pytest
from mock import MagicMock

from ska_pss_lmc.parser import LogParserConsumer

# Create module logger that logs all levels
module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

# Create handler to output to standard out
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
handler.setFormatter(formatter)
module_logger.addHandler(handler)


@pytest.fixture
def mock_parser_callback():
    """Fixture for a mock parser callback"""
    return MagicMock()


class TestLogParserConsumer:
    """Test the LogParserConsumer class"""

    def setUp(self, mock_parser_callback):
        # define parser consumer thread and queue
        self.log_queue = queue.Queue(100)
        self.mock_logger = MagicMock()
        self.parser = LogParserConsumer(
            log_queue=self.log_queue,
            log_parser_callback=mock_parser_callback,
            logger=self.mock_logger,
        )
        assert isinstance(mock_parser_callback, MagicMock)
        assert not self.parser.running
        self.parser.start()
        time.sleep(0.5)  # Allow the thread to initialize

    def test_thread_starts_and_stops(self, mock_parser_callback):
        self.setUp(mock_parser_callback)
        assert self.parser.running
        self.parser.stop()
        self.parser.join()
        assert not self.parser.running

    def test_parser_marker_identification(
        self,
        pipe_ctrl_comp_man,
        mock_parser_callback,
    ):
        self.setUp(mock_parser_callback)
        # send input queue error
        self.log_queue.put("[error]: test error detected")
        self.log_queue.join()
        self.parser._log_parser_callback.assert_called_once()

        self.log_queue.put(None)  # Stop signal
        self.parser.join()

    def test_process_log_line_without_marker(self, mock_parser_callback):
        self.setUp(mock_parser_callback)
        self.log_queue.put("This is a normal log line.")
        self.log_queue.join()
        self.parser._log_parser_callback.ack.assert_not_called()
        # self.mock_callback.assert_not_called()
        self.parser.stop()
        self.parser.join()

    def test_process_log_line_with_error_marker(self, mock_parser_callback):
        self.setUp(mock_parser_callback)
        self.log_queue.put("[error] Something went wrong!")
        self.log_queue.join()
        self.parser._log_parser_callback.assert_called_with(
            method_name="_handle_log_error"
        )
        # self.mock_callback.assert_called_with(method_name="_handle_log_error")
        self.parser.stop()
        self.parser.join()

    def test_stop_signal(self, mock_parser_callback):
        self.setUp(mock_parser_callback)
        self.log_queue.put(None)
        self.parser.join()
        assert not self.parser.running
