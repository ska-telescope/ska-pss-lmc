"""
Contains the tests for the PipelineCtrlDevice Tango
device_under_test prototype.
"""

from __future__ import annotations

import logging
import time

import pytest
import tango
from mock import patch
from paramiko import SSHException
from ska_control_model import (
    AdminMode,
    HealthState,
    ObsState,
    SimulationMode,
    TestMode,
)

from ska_pss_lmc.device_proxy import PssDeviceProxy
from ska_pss_lmc.testing.pipeline.ssh_access import SshAccessPatched
from ska_pss_lmc.testing.poller import probe_poller
from ska_pss_lmc.testing.tango_harness import DeviceToLoadType, TangoHarness

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)


@pytest.fixture()
def device_to_load(
    patched_controller_device_class,
) -> DeviceToLoadType:
    """
    Fixture that specifies the device to be loaded for testing.

    :param patched_controller_device_class: a controller device class
        that has been patched with a mock component manager

    :return: specification of the device to be loaded
    """
    return {
        "path": "charts/ska-pss-lmc/data/configuration.json",
        "package": "ska_pss_lmc",
        "device": "ctrl1",
        "proxy": PssDeviceProxy,
        "patch": patched_controller_device_class,
    }


@pytest.fixture()
def device_under_test(tango_harness: TangoHarness) -> PssDeviceProxy:
    """
    Fixture that returns the device under test.

    :param tango_harness: a test harness for Tango devices

    :return: the device under test
    """
    return tango_harness.get_device("mid-pss/pipeline/0001")


class TestPipelineCtrlDevice:
    """Tests of the PipelineCtrl device."""

    #
    # Test device Attributes
    #
    def test_cheetah_version(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test cheetah version

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.cheetahVersion == "x.y.z"

    def test_cheetah_pid(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test cheetah pid initial value.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.cheetahPid == -1

    def test_pipeline_name(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test the pipeline device id name.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.pipelineName == "node-ctrl-00-01-a"

    #
    # Test device commands
    #
    def test_InitDevice(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test for Initialisation.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.healthState == HealthState.UNKNOWN
        assert device_under_test.simulationMode == SimulationMode.FALSE
        assert device_under_test.testMode == TestMode.TEST
        assert device_under_test.State() == tango.DevState.DISABLE
        assert device_under_test.adminMode == AdminMode.OFFLINE
        assert device_under_test.Status() == "The device is in DISABLE state."

    def test_start_stop_start_communicating(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test the start and stop of the communication.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.adminMode == AdminMode.OFFLINE
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
        assert device_under_test.adminMode == AdminMode.ONLINE
        assert device_under_test.isCommunicating
        device_under_test.adminMode = AdminMode.OFFLINE
        probe_poller(
            device_under_test, "State", tango.DevState.DISABLE, time=5
        )
        assert not device_under_test.isCommunicating
        assert device_under_test.healthState == HealthState.UNKNOWN
        # start communicating again
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
        assert device_under_test.healthState == HealthState.OK
        assert device_under_test.isCommunicating

    def test_configure_cheetah_pipeline_with_failure(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test cheetah pipeline configuration.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        with patch.object(
            SshAccessPatched,
            "write_config",
            side_effect=IOError("write_config error"),
        ):
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
            probe_poller(device_under_test, "isCommunicating", True, time=2)
            device_under_test.ConfigureScan(
                '{"config_id": "1", "test": "dummy"}'
            )
            module_logger.info(
                f"long: {device_under_test.longRunningCommandStatus}"
            )
            probe_poller(device_under_test, "obsState", ObsState.FAULT, time=5)
            assert device_under_test.longRunningCommandStatus[1] == "FAILED"

    def test_configure_with_exception_in_json_convertion(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ):
        """
        Test for configuring cheetah process if an exception occurs.

        The exception is caught by the *_run()* method of the TaskExecutor:
        in this case the command TaskStatus is set to TaskStatus.FAILED.

        """
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
        assert device_under_test.isCommunicating
        json_configuration = '{"config_id": "XXXX","test": "dummy"}'
        with patch(
            "ska_pss_lmc.pipeline.pipeline_component_manager._from_json_to_xml"
        ) as mock_json:
            mock_json.side_effect = Exception

            device_under_test.ConfigureScan(json_configuration)
            probe_poller(device_under_test, "obsState", ObsState.FAULT, time=5)

    def test_abort_pipeline_and_obsreset(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test cheetah pipeline termination.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """

        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
        assert device_under_test.isCommunicating
        device_under_test.ConfigureScan('{"config_id": "1", "test": "dummy"}')
        probe_poller(device_under_test, "obsState", ObsState.READY, time=2)
        assert (
            device_under_test.lastScanConfiguration
            == '{"config_id": "1", "test": "dummy"}'
        )
        device_under_test.scan('{"scan_id":11}')
        probe_poller(device_under_test, "obsState", ObsState.SCANNING, time=2)
        assert device_under_test.cheetahPid == 8081
        device_under_test.abort()
        probe_poller(device_under_test, "obsState", ObsState.ABORTED, time=2)
        assert device_under_test.cheetahPid == -1
        command_status, command_id = device_under_test.obsreset()
        probe_poller(device_under_test, "obsState", ObsState.IDLE, time=5)

    def test_abort_pipeline_with_exception(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test cheetah pipeline termination when an exception is raised.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        with patch.object(
            SshAccessPatched,
            "kill",
            side_effect=SSHException("kill exception"),
        ):
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
            assert device_under_test.isCommunicating
            device_under_test.ConfigureScan(
                '{"config_id": "1", "test": "dummy"}'
            )
            probe_poller(device_under_test, "obsState", ObsState.READY, time=2)
            device_under_test.scan('{"scan_id":11}')
            probe_poller(
                device_under_test, "obsState", ObsState.SCANNING, time=2
            )
            assert device_under_test.cheetahPid == 8081
            _, command_id = device_under_test.abort()
            probe_poller(device_under_test, "obsState", ObsState.FAULT, time=2)
            assert device_under_test.cheetahPid == 8081
            lrc_status = device_under_test.longRunningCommandStatus
            ev_li = [
                ele for ele in lrc_status if (lrc_status.index(ele)) % 2 == 0
            ]
            od_li = [
                ele for ele in lrc_status if lrc_status.index(ele) % 2 != 0
            ]
            res = dict(zip(ev_li, od_li))
            assert res[command_id[0]] == "FAILED"

    def test_abort_with_pipeline_not_running(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test cheetah pipeline termination when an exception is raised.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        with patch.object(SshAccessPatched, "is_running", return_value=False):
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
            probe_poller(device_under_test, "isCommunicating", True, time=2)
            device_under_test.ConfigureScan(
                '{"config_id": "1", "test": "dummy"}'
            )
            assert device_under_test.cheetahPid == -1
            with pytest.raises(Exception):
                device_under_test.abort()
                probe_poller(
                    device_under_test, "obsState", ObsState.READY, time=2
                )
                assert (
                    device_under_test.longRunningCommandStatus[1] == "REJECTED"
                )

    def test_reconnection_with_device_in_scanning(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test for starting communication.

        Checks that the CommunicationStatus is updated to ESTABLISHED
        """
        with patch.object(
            SshAccessPatched,
            "is_running",
            return_value=True,
        ):
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
            assert device_under_test.obsState == ObsState.SCANNING

    def test_scan_logs(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test for starting communication.

        Checks that the CommunicationStatus is updated to ESTABLISHED
        """
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
        assert device_under_test.isCommunicating
        device_under_test.ConfigureScan('{"config_id": "1", "test": "dummy"}')
        module_logger.info(
            f"long: {device_under_test.longRunningCommandStatus}"
        )
        probe_poller(device_under_test, "obsState", ObsState.READY, time=5)
        assert device_under_test.longRunningCommandStatus[1] == "COMPLETED"
        command_status, command_id = device_under_test.scan('{"scan_id":11}')
        probe_poller(device_under_test, "obsState", ObsState.SCANNING, time=2)
        time.sleep(1)
        for _ in range(3):
            log_line = device_under_test.cheetahLogLine
            assert log_line.find("cheetah") != -1

        progress = device_under_test.longRunningCommandProgress
        # Note: progress is a list with 2 elements: the first is the command id
        # the second is the progress counter
        assert int(progress[1]) > 1
        device_under_test.abort()
        probe_poller(device_under_test, "obsState", ObsState.ABORTED, time=2)

    def test_end_scan_and_goto_idle(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test end scan command
        """
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
        assert device_under_test.isCommunicating
        device_under_test.ConfigureScan('{"config_id": "1", "test": "dummy"}')
        module_logger.info(
            f"long: {device_under_test.longRunningCommandStatus}"
        )
        probe_poller(device_under_test, "obsState", ObsState.READY, time=5)
        assert device_under_test.longRunningCommandStatus[1] == "COMPLETED"
        device_under_test.scan('{"scan_id":11}')
        probe_poller(device_under_test, "obsState", ObsState.SCANNING, time=2)
        device_under_test.EndScan()
        probe_poller(device_under_test, "obsState", ObsState.READY, time=2)
        device_under_test.GotoIdle()
        probe_poller(device_under_test, "obsState", ObsState.IDLE, time=2)

    def test_end_scan_with_exception_on_start(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test end scan when exception raised.

        Check that the final state is READY and the command status is FAILED.
        """
        with patch.object(
            SshAccessPatched, "start", side_effect=Exception("start exception")
        ):
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
            assert device_under_test.isCommunicating
            device_under_test.ConfigureScan(
                '{"config_id": "1", "test": "dummy"}'
            )
            module_logger.info(
                f"long: {device_under_test.longRunningCommandStatus}"
            )
            probe_poller(device_under_test, "obsState", ObsState.READY, time=5)
            assert device_under_test.longRunningCommandStatus[1] == "COMPLETED"
            _, command_id = device_under_test.scan('{"scan_id":11}')
            probe_poller(device_under_test, "obsState", ObsState.READY, time=1)
            # sleep for a while to let the polling command update the lrc
            # attributes
            time.sleep(0.5)
            status = device_under_test.CheckLongRunningCommandStatus(
                command_id[0]
            )
            assert status == "FAILED"

    def test_pipeline_progress(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test pipeline progress attribute
        """
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
        assert device_under_test.isCommunicating
        device_under_test.ConfigureScan('{"config_id": "1", "test": "dummy"}')
        module_logger.info(
            f"long: {device_under_test.longRunningCommandStatus}"
        )
        probe_poller(device_under_test, "obsState", ObsState.READY, time=5)
        assert device_under_test.longRunningCommandStatus[1] == "COMPLETED"
        device_under_test.scan('{"scan_id":11}')
        probe_poller(device_under_test, "obsState", ObsState.SCANNING, time=2)
        progress1 = device_under_test.pipelineProgress
        time.sleep(0.5)
        progress2 = device_under_test.pipelineProgress
        assert progress1 <= progress2
        device_under_test.abort()
        probe_poller(device_under_test, "obsState", ObsState.ABORTED, time=2)

    # test Parser:
    def test_scan_logs_parser_with_error(
        self: TestPipelineCtrlDevice,
        device_under_test: PssDeviceProxy,
    ) -> None:
        """
        Test a simulated Cheetah scan with a using a simulated log that
        contains an error

        Checks that plog parser detect the [error] marker and that the ObsState
        is set to ObsState.FAULT.
        """
        with patch.object(
            SshAccessPatched,
            "get_logs",
            new=lambda s: SshAccessPatched.mock_get_logs_with_error(s),
        ):
            device_under_test.adminMode = AdminMode.ONLINE
            probe_poller(device_under_test, "State", tango.DevState.ON, time=5)
            assert device_under_test.isCommunicating
            device_under_test.ConfigureScan(
                '{"config_id": "1", "test": "dummy"}'
            )
            module_logger.info(
                f"long: {device_under_test.longRunningCommandStatus}"
            )
            probe_poller(device_under_test, "obsState", ObsState.READY, time=5)
            assert device_under_test.longRunningCommandStatus[1] == "COMPLETED"
            command_status, command_id = device_under_test.scan(
                '{"scan_id":11}'
            )
            probe_poller(
                device_under_test, "obsState", ObsState.SCANNING, time=2
            )
            time.sleep(1)
            # the patched log data introduce an ERROR in the log.
            # When it is parsed, the obsState is set to ObsState.FAULT
            probe_poller(device_under_test, "obsState", ObsState.FAULT, time=2)
