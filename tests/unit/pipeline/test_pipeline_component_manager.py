# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
from __future__ import annotations

import logging
import sys
import xml.etree.ElementTree as ET
from unittest.mock import ANY

import pytest
from mock import MagicMock, patch
from paramiko import SSHException
from ska_control_model import CommunicationStatus, PowerState, TaskStatus
from ska_tango_base.commands import ResultCode

from ska_pss_lmc.pipeline.pipeline_component_manager import (
    PipelineCtrlComponentManager,
    _from_json_to_xml,
)
from ska_pss_lmc.testing.pipeline.ssh_access import SshAccessPatched
from ska_pss_lmc.testing.poller import probe_poller

# pylint: disable=invalid-name
# pylint: disable=no-self-use


# Create module logger that logs all levels
module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

# Create handler to output to standard out
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
handler.setFormatter(formatter)
module_logger.addHandler(handler)


def test_from_json_to_xml_simple():
    json_string = '{"test":"dummy"}'
    xml_string = _from_json_to_xml(json_string)
    assert xml_string == (
        '<?xml version="1.0" ?>\n<cheetah>\n  <test>dummy</test>\n</cheetah>\n'
    )


def test_from_json_to_xml_complex():
    json_string = """
    {
      "sps_events": {
          "active": "true",
          "sink": [
              {"id": "spccl_files"},
              {"id": "candidate_files"}
          ]
      },
      "ddtr": {
          "dedispersion": [
              {"start": "0.0", "end": "100.0", "step": "0.1"},
              {"start": "100.0", "end": "300.0", "step": "0.2"}
          ]
      }
    }
    """
    xml_string = _from_json_to_xml(json_string)
    root = ET.fromstring(xml_string)

    sinks = root.findall(".//sps_events/sink")
    assert len(sinks) == 2
    assert sinks[0].find("id").text == "spccl_files"
    assert sinks[1].find("id").text == "candidate_files"

    dm_blocks = root.findall(".//ddtr/dedispersion")
    assert len(dm_blocks) == 2
    assert dm_blocks[0].find("start").text == "0.0"
    assert dm_blocks[1].find("start").text == "100.0"


class TestPipelineCtrlComponentManager:
    def test_start_communicating_cbk_failure(
        self,
        pipe_ctrl_comp_man,
        callbacks,
    ):
        """
        Test the establishment of the communication.

        Test the establishment of communication between the component
        manager and the device under control (cheetah pipeline) when
        an exception is raised during the connection.

        Expected result:
        CommunicationStatus = NOT_ESTABLISHED
        State = DevState.FAULT
        adminMode = OFFLINE
        """

        with patch.object(
            PipelineCtrlComponentManager,
            "_start_communicating",
            side_effect=Exception("error in task to execute"),
        ):
            with patch(
                "ska_pss_lmc.pipeline.pipeline_component_manager"
                ".PipelineCtrlComponentManager."
                "_start_communicating_callback"
            ) as mock_func:
                mock_func.side_effect = MagicMock()
                pipe_ctrl_comp_man.start_communicating()
                mock_func.mock_calls[-1].assert_called_with(
                    status=TaskStatus.FAILED,
                    result=(
                        ResultCode.FAILED,
                        "Unhandled exception during execution: "
                        "error in task to execute",
                    ),
                    exception=Exception("error in task to execute"),
                )

    def test_start_communicating_with_success(
        self, pipe_ctrl_comp_man, callbacks
    ):
        """
        Test for starting communication.

        Checks that the CommunicationStatus is updated to ESTABLISHED
        """
        pipe_ctrl_comp_man.start_communicating()
        callbacks.assert_call("component_state", power=PowerState.ON)
        callbacks.assert_call(
            "communication_state", CommunicationStatus.ESTABLISHED
        )

        assert pipe_ctrl_comp_man.is_communicating

    def test_start_communicating_with_failure(
        self,
        pipe_ctrl_comp_man,
        callbacks,
    ):
        """
        Test for starting communication if an exception is raised

        Checks that the CommunicationStatus is updated to NOT_ESTABLISHED
        """
        with patch.object(
            SshAccessPatched,
            "connect",
            side_effect=Exception("error in task to execute"),
        ):
            pipe_ctrl_comp_man.start_communicating()
            callbacks.assert_call(
                "communication_state", CommunicationStatus.NOT_ESTABLISHED
            )

            assert not pipe_ctrl_comp_man.is_communicating

    def test_stop_communicating(self, pipe_ctrl_comp_man, callbacks):
        """
        Test for stopping communication.

        Checks that the CommunicationStatus is updated to DISABLED
        """
        pipe_ctrl_comp_man.start_communicating()
        callbacks.assert_call("component_state", power=PowerState.ON)
        callbacks.assert_call(
            "communication_state", CommunicationStatus.ESTABLISHED
        )

        # callbacks.assert_call('update_attribute','is_communicating',True)
        pipe_ctrl_comp_man.stop_communicating()
        callbacks.assert_call(
            "component_state", power=PowerState.UNKNOWN, lookahead=2
        )
        callbacks.assert_call(
            "communication_state", CommunicationStatus.DISABLED, lookahead=2
        )

        assert not pipe_ctrl_comp_man.is_communicating

    def test_subarray_id(self, pipe_ctrl_comp_man):
        pipe_ctrl_comp_man.subarray_id = 3
        assert pipe_ctrl_comp_man.subarray_id == 3
        with pytest.raises(ValueError):
            pipe_ctrl_comp_man.subarray_id = 17
            assert pipe_ctrl_comp_man.subarray_id == 3

    def test_scan_with_success(
        self,
        pipe_ctrl_comp_man,
        callbacks,
    ):
        """
        Test for launching the cheetah process.

        Checks that chetaah PID is updated with the value coming
        from Communication Manager
        """
        input = {"scan_id": 1}
        pipe_ctrl_comp_man.scan(callbacks["scan_task"], **input)
        callbacks.assert_call("scan_task", status=TaskStatus.QUEUED)
        callbacks.assert_call(
            "scan_task",
            status=TaskStatus.IN_PROGRESS,
            result=ResultCode.STARTED,
        )
        callbacks.assert_call("component_state", scanning=True)
        probe_poller(pipe_ctrl_comp_man, "cheetah_pid", 8081, time=2)
        pipe_ctrl_comp_man.end_scan(callbacks["endscan_task"])
        callbacks["endscan_task"].assert_call(
            status=TaskStatus.IN_PROGRESS, result=ResultCode.STARTED
        )
        callbacks.assert_call("component_state", scanning=False, lookahead=4)
        # time.sleep(0.1)
        callbacks.assert_call(
            "endscan_task",
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, ANY),
            lookahead=4,
        )

    @patch.object(SshAccessPatched, "start")
    def test_scan_with_failure(
        self, mock_start, pipe_ctrl_comp_man, callbacks
    ):  # sourcery skip: avoid-builtin-shadow
        """
        Test for launching the cheetah process if an exception is raised

        Checks that chetaah PID is returned back to default value (-1)
        """

        def raise_exception(cmd):
            raise SSHException("start exception")

        mock_start.side_effect = raise_exception
        input = {"scan_id": 1}
        pipe_ctrl_comp_man.scan(callbacks["scan_task"], **input)
        callbacks["scan_task"].assert_against_call(status=TaskStatus.QUEUED)
        callbacks["scan_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS, result=ResultCode.STARTED
        )
        assert pipe_ctrl_comp_man.cheetah_pid == -1
        callbacks["scan_task"].assert_against_call(
            status=TaskStatus.FAILED,
            result=(ResultCode.FAILED, ANY),
        )

    def test_abort(self, pipe_ctrl_comp_man, callbacks):
        # sourcery skip: avoid-builtin-shadow
        """
        Test for aborting cheetah process.

        Checks that cheetah PID is returned back to default value (-1)
        """
        input = {"scan_id": 1}
        pipe_ctrl_comp_man.scan(callbacks["scan_task"], **input)
        probe_poller(pipe_ctrl_comp_man, "cheetah_pid", 8081, time=1)
        pipe_ctrl_comp_man.abort()
        probe_poller(pipe_ctrl_comp_man, "cheetah_pid", -1, time=2)

    @patch.object(SshAccessPatched, "write_config")
    def test_obsreset_with_pipeline_in_fault(
        self,
        mock_write_config,
        pipe_ctrl_comp_man,
        callbacks,
    ):
        """
        Test ObsReset when the device is in obsStat=FAULT
        Force the configurescan command to fail and invoke
        component_state with obsfault=True.

        Checks that the commad and component state callbacks are
        called properly invoked and that the pipeline is not
        configured.
        """

        def side_effect(*args, **kwargs):
            callbacks["obsreset_task"].args.append(args)

        mock_write_config.side_effect = ValueError("Write failed")

        pipe_ctrl_comp_man.configure_scan(callbacks["configure_task"])
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED,
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS, result=ResultCode.STARTED
        )
        callbacks.assert_call("component_state", obsfault=True)
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.FAILED, result=(ResultCode.FAILED, ANY)
        )

        callbacks["obsreset_task"].args = ["12345_Obsreset"]
        callbacks["obsreset_task"].side_effect = side_effect
        pipe_ctrl_comp_man.obsreset(callbacks["obsreset_task"])
        callbacks["obsreset_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS,
            result=ResultCode.STARTED,
        )
        callbacks.assert_call("component_state", obsfault=False)
        callbacks["obsreset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, ANY),
            lookahead=5,
        )

        assert not pipe_ctrl_comp_man.scan_configuration

    def test_obsreset(
        self,
        pipe_ctrl_comp_man,
        callbacks,
    ):
        """
        Test for ObsReset

        Checks that the command callback is correctly invoked and
        that the pipeline remains unconfigured.

        Note: The component_state callbacks are not triggered because
        the component states remain unchanged. If the component state
        value does not change, the callback is not invoked (see the
        _update_component_state method in the BC).
        """

        def side_effect(*args, **kwargs):
            callbacks["obsreset_task"].args.append(args)

        callbacks["obsreset_task"].args = ["12345_Obsreset"]
        callbacks["obsreset_task"].side_effect = side_effect
        pipe_ctrl_comp_man.obsreset(callbacks["obsreset_task"])
        callbacks["obsreset_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS,
            result=ResultCode.STARTED,
        )

        callbacks["obsreset_task"].assert_against_call(
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, ANY),
            lookahead=5,
        )

        assert not pipe_ctrl_comp_man.scan_configuration

    def test_obsreset_with_excetion_in_deleting_file(
        self,
        pipe_ctrl_comp_man,
        callbacks,
    ):
        """
        Test exception during observing reset operation.

        Mock the delete_config method of the communication manager to
        raise an exception.
        Test verifies the command is completed, but it returns a
        failed result code.
        """

        def side_effect(*args, **kwargs):
            callbacks["obsreset_task"].args.append(args)

        with patch.object(
            SshAccessPatched,
            "delete_config",
            side_effect=Exception("error in task to execute"),
        ):
            callbacks["obsreset_task"].args = ["12345_Obsreset"]
            callbacks["obsreset_task"].side_effect = side_effect
            pipe_ctrl_comp_man.obsreset(callbacks["obsreset_task"])
            callbacks["obsreset_task"].assert_against_call(
                status=TaskStatus.IN_PROGRESS
            )

            callbacks["obsreset_task"].assert_against_call(
                status=TaskStatus.FAILED,
                result=(ResultCode.FAILED, ANY),
                lookahead=5,
            )

    def test_configure_with_success(self, pipe_ctrl_comp_man, callbacks):
        """
        Test for configuring cheetah process.
        """
        pipe_ctrl_comp_man.configure_scan(
            callbacks["configure_task"], kwargs={"test": "dummy"}
        )

        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.QUEUED
        )
        callbacks["configure_task"].assert_against_call(
            status=TaskStatus.IN_PROGRESS,
            result=ResultCode.STARTED,
        )
        callbacks.assert_call("component_state", configured=True)
        callbacks["configure_task"].assert_call(
            status=TaskStatus.COMPLETED, result=(ResultCode.OK, ANY)
        )

    def test_cheetah_version(self, pipe_ctrl_comp_man):
        """
        Test that Cheetah version is correctly stored

        Not implemented: only report mocked value
        """
        assert pipe_ctrl_comp_man.cheetah_version == "x.y.z"

    def test_pipeline_progress(self, pipe_ctrl_comp_man):
        """
        Test that pipeline_progress is correcly reported

        Not implemented: only report mocked value
        """
        assert pipe_ctrl_comp_man.pipeline_progress == 0

    def test_scan_log(
        self,
        pipe_ctrl_comp_man,
        callbacks,
    ):
        """
        Test for launching the cheetah process.

        Checks that chetaah PID is updated with the value coming
        from Communication Manager
        """
        input = {"scan_id": 1}
        pipe_ctrl_comp_man.scan(callbacks["scan_task"], **input)
        callbacks["scan_task"].assert_call(status=TaskStatus.QUEUED)
        callbacks.assert_call(
            "scan_task",
            status=TaskStatus.IN_PROGRESS,
            result=ResultCode.STARTED,
        )
        probe_poller(pipe_ctrl_comp_man, "pipeline_progress", 1, time=2)
        assert pipe_ctrl_comp_man.log_line.find("cheetah") != -1
        pipe_ctrl_comp_man.abort()

    def test_gotoidle_with_success(
        self,
        pipe_ctrl_comp_man,
        callbacks,
    ):
        """
        Test execution of the deconfigure command.

        Configure the pipeline and then invoke the
        deconfigure command and verify that:

        * the callback command has been invoked with the proper arguments
        * the scan configuration has been deleted.
        """

        def side_effect(*args, **kwargs):
            callbacks["deconfigure_task"].args.append(args)

        callbacks["deconfigure_task"]
        json_configuration = {"test": "dummy"}
        pipe_ctrl_comp_man.configure_scan(
            callbacks["configure_task"], **json_configuration
        )
        callbacks.assert_call("configure_task", status=TaskStatus.QUEUED)
        callbacks.assert_call(
            "configure_task",
            status=TaskStatus.IN_PROGRESS,
            result=ResultCode.STARTED,
        )
        callbacks.assert_call("component_state", configured=True)
        callbacks.assert_call(
            "configure_task",
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, ANY),
        )
        assert pipe_ctrl_comp_man.scan_configuration
        pipe_ctrl_comp_man.deconfigure(callbacks["deconfigure_task"])
        callbacks["deconfigure_task"].args = ["12345_GotoIdle"]
        callbacks["deconfigure_task"].side_effect = side_effect

        callbacks.assert_call("deconfigure_task", status=TaskStatus.QUEUED)
        callbacks.assert_call(
            "deconfigure_task",
            status=TaskStatus.IN_PROGRESS,
            result=ResultCode.STARTED,
        )
        callbacks.assert_call("component_state", configured=False)
        callbacks.assert_call(
            "deconfigure_task",
            status=TaskStatus.COMPLETED,
            result=(ResultCode.OK, "Pipeline reset to idle"),
        )
        assert not pipe_ctrl_comp_man.scan_configuration

    def test_gotoidle_with_excetion_in_deleting_file(
        self,
        pipe_ctrl_comp_man,
        callbacks,
    ):
        """
        Test exception during  deconfigure operation.

        Mock the delete_config method of the communication manager to
        raise an exception.
        Test verifies the command is completed, but it returns a
        failed result code.
        """

        def side_effect(*args, **kwargs):
            callbacks["deconfigure_task"].args.append(args)

        json_configuration = {"config_id": "XXXXX", "test": "dummy"}
        with patch.object(
            SshAccessPatched,
            "delete_config",
            side_effect=Exception("error in task to execute"),
        ):
            pipe_ctrl_comp_man.configure_scan(
                callbacks["configure_task"],
                **json_configuration,
            )
            callbacks.assert_call(
                "configure_task",
                status=TaskStatus.QUEUED,
            )
            callbacks.assert_call(
                "configure_task",
                status=TaskStatus.IN_PROGRESS,
                result=ResultCode.STARTED,
            )
            callbacks.assert_call("component_state", configured=True)
            callbacks.assert_call(
                "configure_task",
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, ANY),
            )
            callbacks["deconfigure_task"].args = ["12345_GotoIdle"]
            callbacks["deconfigure_task"].side_effect = side_effect
            pipe_ctrl_comp_man.deconfigure(callbacks["deconfigure_task"])

            callbacks.assert_call(
                "deconfigure_task",
                status=TaskStatus.QUEUED,
            )
            callbacks["deconfigure_task"].assert_against_call(
                status=TaskStatus.IN_PROGRESS,
                result=ResultCode.STARTED,
            )
            callbacks.assert_call(
                "component_state", obsfault=True, configured=False
            )
            callbacks.assert_call(
                "deconfigure_task",
                status=TaskStatus.FAILED,
                result=(ResultCode.FAILED, ANY),
            )
