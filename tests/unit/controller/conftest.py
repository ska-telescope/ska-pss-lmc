"""
This module defines elements of the pytest test harness shared by
all tests.
"""

from __future__ import annotations

import logging
from typing import Generator, Iterator, Type

import mock
import pytest
from mock import MagicMock
from ska_control_model import (
    AdminMode,
    CommunicationStatus,
    HealthState,
    ObsState,
)
from ska_tango_testing import context
from ska_tango_testing.harness import TangoTestHarnessContext
from ska_tango_testing.integration import TangoEventTracer
from tango import DevState

from ska_pss_lmc.common.connector import Connector
from ska_pss_lmc.controller.controller_component_manager import (
    PssControllerComponentManager,
)
from ska_pss_lmc.controller.controller_device import PssController
from ska_pss_lmc.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_pss_lmc.testing.mock.mock_device import MockDeviceBuilder

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)
# from ska_mid_cbf_mcs.testing.mock.mock_device import MockDeviceBuilder


class DevProperty(ComponentManagerConfiguration):
    """
    Return a mock class for the ManagerConfiguration
    """

    def __init__(self):
        self.PipelineFqdns = [
            "mid-pss/pipeline/0001",
            "mid-pss/pipeline/0002",
            "mid-pss/pipeline/0003",
            "mid-pss/pipeline/0004",
        ]
        self.PssSubarrays = [
            "mid-pss/subarray/01",
            "mid-pss/subarray/02",
            "mid-pss/subarray/03",
        ]

    def get_device_properties(self):
        pass

    def add_attributes(self):
        pass


@pytest.fixture()
def controller_component_manager(
    callbacks,
):
    """
    Return a mock PssControllerComponentManager.

    :param cm_configuration: the class with the device properties
    :param communication_status_changed_callback: callable invoked to
        update the communication status
    :param callbacks: callable invoked to
        update the device state
    :param update_device_property: callable invoked to update
        any device attribute
    """

    def mock_get_device(fqdn):
        _dict = {}
        for fqnd in (
            cm_configuration.PipelineFqdns + cm_configuration.PssSubarrays
        ):
            _dict[fqdn] = mock.MagicMock
        return _dict

    cm_configuration = DevProperty()

    with mock.patch.object(
        Connector,
        "connect",
        return_value=CommunicationStatus.ESTABLISHED,
    ), mock.patch.object(
        Connector, "subscribe", side_effect=mock.MagicMock()
    ), mock.patch.object(
        Connector, "get_device", side_effect=mock_get_device
    ):
        yield PssControllerComponentManager(
            1,
            cm_configuration,
            callbacks["communication_state"],
            callbacks["component_state"],
            callbacks["update_attribute"],
            module_logger,
        )


@pytest.fixture(name="test_context")
def pss_controller_test_context(
    patched_pss_controller_class,
    initial_mocks: dict[str, MagicMock],
) -> Iterator[context.ThreadedTestTangoContextManager._TangoContext]:
    """
    Fixture that creates a test context for the PssController.

    :param initial_mocks: A dictionary of device mocks to be added to
        the test context.
    :return: A test context for the PssController.
    """
    harness = context.ThreadedTestTangoContextManager()
    harness.add_device(
        device_class=patched_pss_controller_class,
        device_name="mid-pss/control/0",
        PssSubarrays=[
            "mid-pss/subarray/01",
            "mid-pss/subarray/02",
            "mid-pss/subarray/03",
        ],
        PipelineFqdns=[
            "mid-pss/pipeline/0001",
            "mid-pss/pipeline/0002",
            "mid-pss/pipeline/0003",
            "mid-pss/pipeline/0004",
        ],
    )
    for name, mock_dev in initial_mocks.items():
        harness.add_mock_device(device_name=name, device_mock=mock_dev)
    with harness as test_context:
        yield test_context


@pytest.fixture
def patched_pss_controller_class() -> Type[PssController]:
    """
    Device class with patched communication and properties.

    :return: a patched device class for testing
    """

    class PatchedPssController(PssController):
        """
        The patched lass used in tests.

        The ComponentManagerConfiguration as well as the SShAccess
        classes are patched.
        """

        def create_component_manager(
            self: PatchedPssController,
        ) -> PssControllerComponentManager:
            """
            Return a partially mocked component manager instead of
            the usual one.

            :return: a mock component manager
            """
            cm_configuration = DevProperty()
            return PssControllerComponentManager(
                2,
                cm_configuration,
                self._communication_state_changed,
                self._component_state_changed,
                self.update_attribute,
                self.logger,
            )

    return PatchedPssController


@pytest.fixture(name="device_under_test")
def device_under_test_fixture(
    test_context: TangoTestHarnessContext,
) -> context.DeviceProxy:
    """
    Fixture that returns the device under test.

    :param test_context: the context in which the tests run
    :return: the DeviceProxy for the device under test
    """
    return test_context.get_device("mid-pss/control/0")


@pytest.fixture(name="event_tracer", autouse=True)
def tango_event_tracer(
    device_under_test: context.DeviceProxy,
) -> Generator[TangoEventTracer, None, None]:
    """
    Fixture that returns a TangoEventTracer for pertinent devices.
    Takes as parameter all required device proxy fixtures for this test module.

    :param device_under_test: the DeviceProxy for the device under test
    :return: TangoEventTracer
    """
    tracer = TangoEventTracer()

    change_event_attr_list = [
        "longRunningCommandResult",
        "adminMode",
        "state",
    ]
    for attr in change_event_attr_list:
        tracer.subscribe_event(device_under_test, attr)

    return tracer


def mock_subarray(device_name) -> mock.MagicMock:
    builder = MockDeviceBuilder(device_name=device_name)
    builder.set_state(DevState.ON)
    builder.add_attribute("adminMode", AdminMode.OFFLINE)
    builder.add_attribute("healthState", HealthState.UNKNOWN)
    builder.add_attribute("obsState", ObsState.EMPTY)
    return builder()


def mock_pipeline(device_name) -> mock.MagicMock:
    builder = MockDeviceBuilder(device_name=device_name)
    builder.set_state(DevState.OFF)
    builder.add_attribute("adminMode", AdminMode.ONLINE)
    builder.add_attribute("healthState", HealthState.OK)
    builder.add_attribute("obsState", ObsState.IDLE)
    builder.add_attribute("subarrayMembership", 0)
    builder.add_attribute("isCommunicating", True)
    return builder()


@pytest.fixture()
def initial_mocks() -> dict[str, mock.MagicMock]:
    """ "mid-pss/pipeline/0001"
    Return a dictionary of proxy mocks to pre-register.

    :param mock_vcc: a mock Vcc that is powered off.
    :param mock_fsp: a mock Fsp that is powered off.
    :param mock_subarray: a mock CbfSubarray that is powered off.
    :param mock_talon_lru: a mock TalonLRU that is powered off.
    :param mock_slim_mesh: a mock SLIM Mesh that is powered off.

    :return: a dictionary of proxy mocks to pre-register.
    """
    return {
        "mid-pss/subarray/01": mock_subarray("mid-pss/subarray/01"),
        "mid-pss/subarray/02": mock_subarray("mid-pss/subarray/02"),
        "mid-pss/subarray/03": mock_subarray("mid-pss/subarray/03"),
        "mid-pss/pipeline/0001": mock_pipeline("mid-pss/pipeline/0001"),
        "mid-pss/pipeline/0002": mock_pipeline("mid-pss/pipeline/0002"),
        "mid-pss/pipeline/0003": mock_pipeline("mid-pss/pipeline/0003"),
        "mid-pss/pipeline/0004": mock_pipeline("mid-pss/pipeline/0004"),
    }
