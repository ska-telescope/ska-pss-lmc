"""
Contains the tests for the PipelineCtrlDevice Tango
device_under_test prototype.
"""

from __future__ import annotations

import logging

import tango
from ska_control_model import (
    AdminMode,
    HealthState,
    ResultCode,
    SimulationMode,
)

from ska_pss_lmc.testing.poller import probe_poller

module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)


class TestPssController:
    """Tests of the PipelineCtrl device."""

    #
    # Test device commands
    #
    def test_controller_device(
        self: TestPssController,
        device_under_test,
    ) -> None:
        """
        Test for Initialisation.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        assert device_under_test.healthState == HealthState.UNKNOWN
        assert device_under_test.simulationMode == SimulationMode.FALSE
        # assert device_under_test.testMode == TestMode.TEST
        assert device_under_test.State() == tango.DevState.DISABLE
        assert device_under_test.adminMode == AdminMode.OFFLINE
        assert device_under_test.Status() == "The device is in DISABLE state."

    def test_initialize_controller(
        self: TestPssController,
        device_under_test,
    ) -> None:
        """
        Test for Initialisation.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "isCommunicating", True, time=5)
        probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
        assert device_under_test.pipelinesstate == ("OFF", "OFF", "OFF", "OFF")
        assert device_under_test.healthstate == HealthState.OK
        assert device_under_test.pipelineshealthstate == (
            "OK",
            "OK",
            "OK",
            "OK",
        )
        assert device_under_test.pipelinesobsstate == (
            "IDLE",
            "IDLE",
            "IDLE",
            "IDLE",
        )
        assert device_under_test.pipelinesiscommunicating.all()
        assert device_under_test.pipelinesadminmode == (
            "ONLINE",
            "ONLINE",
            "ONLINE",
            "ONLINE",
        )
        assert device_under_test.pipelinesFqdns == (
            "mid-pss/pipeline/0001",
            "mid-pss/pipeline/0002",
            "mid-pss/pipeline/0003",
            "mid-pss/pipeline/0004",
        )

    def test_controller_power_commands(
        self: TestPssController,
        device_under_test,
    ) -> None:
        """
        Test power commands are rejected.

        :param device_under_test: fixture that provides a
            :py:class:`tango.DeviceProxy` to the device under test, in a
            :py:class:`tango.test_context.DeviceTestContext`.
        """
        device_under_test.adminMode = AdminMode.ONLINE
        probe_poller(device_under_test, "isCommunicating", True, time=5)
        probe_poller(device_under_test, "state", tango.DevState.ON, time=2)
        status, _ = device_under_test.On()
        assert status[0] == ResultCode.REJECTED
        status, _ = device_under_test.Standby()
        assert status[0] == ResultCode.REJECTED
        status, _ = device_under_test.Off()
        assert status[0] == ResultCode.REJECTED
