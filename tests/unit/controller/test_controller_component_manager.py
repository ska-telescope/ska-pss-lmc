# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
from __future__ import annotations

import logging
import sys

import mock
from ska_control_model import (
    CommunicationStatus,
    HealthState,
    PowerState,
    TaskStatus,
)

from ska_pss_lmc.controller.controller_component_manager import (
    PssControllerComponentManager,
)

# pylint: disable=invalid-name
# pylint: disable=no-self-use


# Create module logger that logs all levels
module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

# Create handler to output to standard out
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
handler.setFormatter(formatter)
module_logger.addHandler(handler)


class TestControllerComponentManager:
    def test_ctrl_start_communicating_with_exception(
        self,
        controller_component_manager,
    ):
        """
        Test the establishment of the communication.

        Test the establishment of communication between the component
        manager and the device under control (cheetah pipeline) when
        an exception is raised during the connection.

        Expected result:
        CommunicationStatus = NOT_ESTABLISHED
        State = DevState.FAULT
        adminMode = OFFLINE
        """

        with mock.patch.object(
            PssControllerComponentManager,
            "_forward_adminmode",
            side_effect=Exception("Error in task to execute"),
        ):
            with mock.patch(
                "ska_pss_lmc.controller.controller_component_manager"
                ".PssControllerComponentManager."
                "_start_communicating_callback"
            ) as mock_func:
                mock_func.side_effect = mock.MagicMock()
                controller_component_manager.start_communicating()

                mock_func.mock_calls[-1].assert_called_with(
                    status=TaskStatus.FAILED,
                    communication_status=CommunicationStatus.NOT_ESTABLISHED,
                    exception=Exception("error in task to execute"),
                )

    def test_ctrl_start_communicating_exception_cbk_not_mocked(
        self,
        controller_component_manager,
        callbacks,
    ):
        """
        Test the establishment of the communication.

        Test the establishment of communication between the component
        manager and the device under control (cheetah pipeline) when
        an exception is raised during the connection.

        Expected result:
        CommunicationStatus = NOT_ESTABLISHED
        State = DevState.FAULT
        adminMode = OFFLINE
        """

        with mock.patch.object(
            PssControllerComponentManager,
            "_forward_adminmode",
            side_effect=Exception("Error in task to execute"),
        ):

            controller_component_manager.start_communicating()

            callbacks["update_attribute"].assert_call(
                "healthstate", HealthState.FAILED, lookahead=4
            )
            assert (
                controller_component_manager.communication_state
                == CommunicationStatus.NOT_ESTABLISHED
            )

    def test_ctrl_start_communicating_with_success(
        self, controller_component_manager, callbacks
    ):
        """
        Test for starting communication.

        Checks that the CommunicationStatus is updated to ESTABLISHED
        """
        controller_component_manager.start_communicating()
        callbacks.assert_call(
            "communication_state", CommunicationStatus.ESTABLISHED, lookahead=5
        )
        callbacks.assert_call(
            "component_state", power=PowerState.ON, lookahead=5
        )
        assert controller_component_manager.is_communicating
