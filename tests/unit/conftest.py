# -*- coding: utf-8 -*-
#
# Ported from the SKA Low MCCS project.
#
# Partially ported from the SKA Low MCCS project:
# https://gitlab.com/ska-telescope/ska-low-mccs/-/blob/main/testing/src/tests/conftest.py
#
# Distributed under the terms of the GPL license.
# See LICENSE for more info.

from __future__ import annotations

import logging
import time
from typing import Any, Generator, Optional

import pytest
from ska_control_model import ResultCode, TaskStatus
from ska_tango_base.base import JSONData
from ska_tango_base.long_running_commands_api import LrcCallback
from ska_tango_testing.mock import MockCallableGroup
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from tango import DevError

from ska_pss_lmc.testing.tango_harness import (
    DevicesToLoadType,
    DeviceToLoadType,
)


@pytest.fixture()
def mock_callback_called_timeout() -> float:
    """
    Return the time to wait for a mock callback to be called when a
    call is expected.

    This is a high value because calls will usually arrive much much
    sooner, but we should be prepared to wait plenty of time before
    giving up and failing a test.

    :return: the time to wait for a mock callback to be called when a
        call is asserted.
    """
    return 10.0


@pytest.fixture()
def mock_callback_not_called_timeout() -> float:
    """
    Return the time to wait for a mock callback to be called when a
    call is unexpected.

    An assertion that a callback has not been called can only be passed
    once we have waited the full timeout period without a call being
    received. Thus, having a high value for this timeout will make such
    assertions very slow. It is better to keep this value fairly low,
    and accept the risk of an assertion passing prematurely.

    :return: the time to wait for a mock callback to be called when a
        call is unexpected.
    """
    return 0.5


@pytest.fixture()
def callbacks() -> MockCallableGroup:
    """
    Return a factory that returns a new mock callback each time it is called.

    Use this fixture in tests that need more than one mock_callback. If
    your tests only needs a single mock callback, it is simpler to use
    the :py:func:`mock_callback` fixture.

    :param mock_callback_called_timeout: the time to wait for a mock
        callback to be called when a call is expected
    :param mock_callback_not_called_timeout: the time to wait for a mock
        callback to be called when a call is unexpected

    :return: a factory that returns a new mock callback each time it is
        called.
    """
    return MockCallableGroup(
        "communication_state",
        "component_state",
        "obsreset_task",
        "configure_task",
        "deconfigure_task",
        "scan_task",
        "endscan_task",
        "abort_task",
        "update_attribute",
        timeout=7.0,
    )


def pytest_itemcollected(item: pytest.Item) -> None:
    """
    Modify a test after it has been collected by pytest.

    This pytest hook implementation adds the "forked" custom mark to all
    tests that use the ``tango_harness`` fixture, causing them to be
    sandboxed in their own process.

    :param item: the collected test for which this hook is called
    """
    if "tango_harness" in item.fixturenames:  # type: ignore[attr-defined]
        item.add_marker("forked")


@pytest.fixture()
def devices_to_load(
    device_to_load: Optional[DeviceToLoadType],
) -> Optional[DevicesToLoadType]:
    """
    Fixture that provides specifications of devices to load.

    In this case, it maps the simpler single-device spec returned by the
    "device_to_load" fixture used in unit testing, onto the more
    general multi-device spec.

    :param device_to_load: fixture that provides a specification of a
        single device to load; used only in unit testing where tests
        will only ever stand up one device at a time.

    :return: specification of the devices (in this case, just one
        device) to load
    """
    if device_to_load is None:
        return None

    device_spec: DevicesToLoadType = {
        "path": device_to_load["path"],
        "package": device_to_load["package"],
        "devices": [
            {
                "name": device_to_load["device"],
                "proxy": device_to_load["proxy"],
            }
        ],
    }
    if "patch" in device_to_load:
        assert device_spec["devices"] is not None  # for the type checker
        device_spec["devices"][0]["patch"] = device_to_load["patch"]

    return device_spec


@pytest.fixture()
def device_to_load() -> Optional[DeviceToLoadType]:
    """
    Fixture that specifies the device to be loaded for testing.

    This default implementation specified no devices to be loaded,
    allowing the fixture to be left unspecified if no devices are
    needed.

    :return: specification of the device to be loaded
    """
    return None


@pytest.fixture(name="successful_lrc_callback")
def successful_lrc_callback_fixture(
    logger: logging.Logger,
) -> Generator[LrcCallback, None, None]:
    """
    Use this callback with invoke_lrc when the LRC should complete successfully.

    :yields: successful_lrc_callback function.
    :raises AssertionError: if unexpected status, progress, result or error is received.
    """  # noqa DAR401,DAR402
    assert_errors: list[AssertionError] = []

    def _successful_lrc_callback(
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: JSONData | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ) -> None:
        try:
            if progress is not None:
                logger.info(f"lrc_callback(progress={progress})")
                assert progress in [33, 66], f"Unexpected progress: {progress}"
            if result is not None:
                logger.info(f"lrc_callback(result={result})")
                assert (
                    isinstance(result, list) and result[0] == ResultCode.OK
                ), {f"Unexpected result: {result}"}
            if status is not None:
                logger.info(f"lrc_callback(status={status.name})")
                assert status in [
                    TaskStatus.STAGING,
                    TaskStatus.QUEUED,
                    TaskStatus.IN_PROGRESS,
                    TaskStatus.COMPLETED,
                ], f"Unexpected status: {status.name}"
            if error is not None:
                logger.error(f"lrc_callback(error={error})")
                assert False, f"Received {error}"
            if kwargs:
                logger.error(f"lrc_callback(kwargs={kwargs})")
        except AssertionError as e:
            assert_errors.append(e)

    yield _successful_lrc_callback
    if assert_errors:
        raise assert_errors[0]


@pytest.fixture(name="aborted_lrc_callback")
def aborted_lrc_callback_fixture(
    logger: logging.Logger,
) -> Generator[LrcCallback, None, None]:
    """
    Use this callback with invoke_lrc when the LRC should be aborted after starting.

    :yields: aborted_lrc_callback function.
    :raises AssertionError: if unexpected status, progress, result or error is received.
    """  # noqa DAR401,DAR402
    assert_errors: list[AssertionError] = []

    def _aborted_lrc_callback(
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: JSONData | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ) -> None:
        try:
            if progress is not None:
                logger.info(f"lrc_callback(progress={progress})")
                assert False, f"Unexpected progress: {progress}"
            if result is not None:
                logger.info(f"lrc_callback(result={result})")
                assert (
                    isinstance(result, list)
                    and result[0] == ResultCode.ABORTED
                ), {f"Unexpected result: {result}"}
            if status is not None:
                logger.info(f"lrc_callback(status={status.name})")
                assert status in [
                    TaskStatus.STAGING,
                    TaskStatus.QUEUED,
                    TaskStatus.IN_PROGRESS,
                    TaskStatus.ABORTED,
                ], f"Unexpected status: {status.name}"
            if error is not None:
                logger.error(f"lrc_callback(error={error})")
                assert False, f"Received {error}"
            if kwargs:
                logger.error(f"lrc_callback(kwargs={kwargs})")
        except AssertionError as e:
            assert_errors.append(e)

    yield _aborted_lrc_callback
    if assert_errors:
        raise assert_errors[0]


@pytest.fixture(name="lrc_callback_log_only")
def lrc_callback_log_only_fixture(
    logger: logging.Logger,
) -> LrcCallback:
    """
    Use this callback with invoke_lrc only to log the arguments.

    :param logger: test logger
    :return: lrc_callback_log_only function.
    """

    def _lrc_callback_log_only(
        status: TaskStatus | None = None,
        progress: int | None = None,
        result: JSONData | None = None,
        error: tuple[DevError] | None = None,
        **kwargs: Any,
    ) -> None:
        if progress is not None:
            logger.info(f"lrc_callback(progress={progress})")
        if result is not None:
            logger.info(f"lrc_callback(result={result})")
        if status is not None:
            logger.info(f"lrc_callback(status={status.name})")
        if error is not None:
            logger.error(f"lrc_callback(error={error})")
        if kwargs:
            logger.error(f"lrc_callback(kwargs={kwargs})")

    return _lrc_callback_log_only


class Helpers:
    """Static helper functions for tests."""

    @staticmethod
    def assert_lrcstatus_change_event_staging_queued_in_progress(
        change_event_callbacks: MockTangoEventCallbackGroup, command: Any
    ) -> None:
        """
        Assert the longRunningCommandStatus attribute change event multiple
        times.

        :param change_event_callbacks: dictionary of mock change event
            callbacks
        :param command: name/id of command to assert change events
        """
        for status in ["STAGING", "QUEUED", "IN_PROGRESS"]:
            change_event_callbacks.assert_change_event(
                "longRunningCommandStatus", (command, status)
            )

    @staticmethod
    def print_change_event_queue(
        change_event_callbacks: MockTangoEventCallbackGroup,
        attr_name: str,
    ) -> None:
        """
        Print the change event callback queue of the given attribute for
        debugging.

        :param change_event_callbacks: dictionary of mock change event
            callbacks
        :param attr_name: attribute in the change event callback group
            to print
        """
        print(f"{attr_name} change event queue:")
        # pylint: disable=protected-access
        for node in change_event_callbacks[
            attr_name
        ]._callable._consumer_view._iterable:
            print(node.payload["attribute_value"])

    @staticmethod
    def assert_expected_logs(
        caplog: pytest.LogCaptureFixture,
        expected_logs: list[str],
        timeout: int = 2,
    ) -> None:
        """
        Assert the expected log messages are in the captured logs.

        The expected list of log messages must appear in the records in the
        same order.
        The captured logs are cleared before returning for subsequent
        assertions.

        :param caplog: pytest log capture fixture.
        :param expected_logs: to assert are in the log capture fixture.
        :param timeout: time to wait for the last log message to appear,
            default 2 secs.
        """
        start_time = time.time()
        while time.time() - start_time < timeout:
            if expected_logs[-1] in caplog.text:
                break
        else:
            pytest.fail(
                f"'{expected_logs}' not found in logs within {timeout} seconds"
            )
        test_logs = [record.message for record in caplog.records]
        assert test_logs == expected_logs
        caplog.clear()
