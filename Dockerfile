ARG BUILD_IMAGE="artefact.skao.int/ska-tango-images-pytango-builder:9.5.0"
ARG BASE_IMAGE="artefact.skao.int/ska-tango-images-pytango-runtime:9.5.0"
FROM $BUILD_IMAGE AS buildenv

FROM $BASE_IMAGE
# Install Poetry
USER root

ENV SETUPTOOLS_USE_DISTUTILS=stdlib

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update && apt-get install git pkg-config build-essential libboost-python-dev  -y

RUN poetry config virtualenvs.create false

# Install runtime dependencies and the app
RUN poetry install

USER tango

