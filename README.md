SKA PSS LMC
============

This project is developing the Local Monitoring and Control (LMC) software for the Pulsar Search Sub-element (PSS) within the Central Signal Processor (CSP) element of the Square Kilometre Array (SKA) Low and Mid telescopes.

At the present situation the PSS.LMC comprises only the PSS CTRL Pipeline Device, a Tango Device able to start and stop the Cheetah Pipeline, i.e. the software used for the data reduction needed for the search of the pulsar. In the future, also Controller and Subarray devices will be developed in order to be monitored and controlled by the [CSP.LMC](https://gitlab.com/ska-telescope/ska-csp-lmc-mid) 

There are two possible approaches currently available to the user for the purposes of running/testing the PSS.LMC. In both instances the PSS CTRL Pipeline Device is launched into a K8s namespace, and depending on how the user executes the deployment, will be capable of either:

* Controlling a containerised cheetah, contained within a separate K8s pod in the same namespace,
* Controlling a real cheetah pipeline, installed on a physical server.

Further details of the required commands can be found [here](docs/src/operation/example.rst)

## Table of contents
* [Documentation](#documentation)
* [Repository organization](#repository-organization)
* [Deployment in Kubernetes](#deployment-in-kubernetes)
* [Tests](#tests)
* [Known bugs](#known-bugs)
* [Troubleshooting](#troubleshooting)
* [License](#license)

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-pss-lmc/badge/?version=latest)](https://developer.skatelescope.org/projects/ska-pss-lmc/en/latest/?badge=latest)

The documentation for this project, including the package description, Architecture description and the API modules can be found at SKA developer portal: 

[PSS.LMC documentation](https://developer.skatelescope.org/projects/ska-pss-lmc/en/latest/?badge=latest)

## Repository Organisation
The repository has the following organization:

* project source (src): contains the Python code
* resources: contains both the POGO files of the TANGO Devices of the project and the Taranta Dashboards.
* cheetah: contains files to build a container with Cheetah, for testing purposes. Currently this image is supplied by [ska-pss-image](https://gitlab.com/ska-telescope/pss/ska-pss-image) 
* tests: contains the tests; there are unit tests for the code, and integration tests written as BDD tests. 
* docs: contains all the files to generate the documentation for the project.
* charts: stored the HELM charts to deploy the PSS.LMC under kubernets environment and the Cheetah pod used for testing. 

# Containerised PSS.LMC in Kubernetes

The TANGO devices of the PSS.LMC prototype run in a containerised environment.
Currently only the container for PSS CTRL Pipeline device is developed. 
A cheetah simulator is provided in a different container and can be accessed via SSH for testing purposes. 

The PSS.LMC containerised TANGO servers are managed via Kubernetes.
The system is setup so that each k8s Pod has only one Docker container that in turn
runs only one Tango Device Server application.

# Build the PSS.LMC docker image
  
The PSS.LMC project fully relies on the standard SKA CI/CD makefiles.

The local Makefile, in the root folder of the project, defines the setting and variables used to customize the docker image building and the deployment of the system.

To build the PSS.LMC docker image, issue the command
  
  ```bash
  make oci-build
  ```

Depending on which image builder your version of docker uses, you may need to run

  ```bash
  export DOCKER_BUILDKIT=0
  ```

if the above command fails. 

To use the built image in PSS.LMC system deployment, the environment should be configured to use the minikube's Docker daemon running with

  ```
  eval $(minikube docker-env)
  ```

## Deployment in Kubernetes

In the following there are the instructions to deploy the *PSS.LMC*. This system is composed by in order to test the Common functionalities in a TANGO environment. 


The SKA project [ska-cicd-deploy-minikube](#https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube) provides all the instructions to setup a minikube machine running in a virtual environment. The instructions are very detailed and cover many frequent issues. Be aware of heavy HW requirements: 4 cores or more,
and more than 8GB ram. 

### Controlling cheetah

The default behavior is to launch a PSS.LMC that can control a natively installed cheetah (rather than a containerised cheetah) that runs on a physical server (see above). To do this, we need to provide the details of the server (hostname, username, password, path to cheetah binary, pipeline, etc) by editing the relevant [values.yaml](charts/ska-pss-lmc/data/psspipelinectrl.yaml). The following fields will need to be set according to the user's requirements. 

* NodeIp - the IP address or hostname of the physical server on which cheetah is installed,
* CheetahExecutable - the path to the cheetah binary that will be executed,
* CheetahPipelineType - the specific PSS pipeline that will be launched (i.e., single pulse search, acceleration search),
* CheetahPipelineSource - the expected source of the data stream that will be passed to cheetah,
* CheetahLogLevel - the verbosity of the stdout messages from cheetah,
* CheetahUserPasswd - the username and password of the user on the remote host.

### Controlling a containerised

To instead launch the cheetah simulator we first set an environment variable according to the absolute path of a separate values.yaml file, e.g.,

  ```bash
  export VALUES_FILE=charts/ska-pss-lmc/data/psspipelinectrlsim.yaml
  ```

### Execute the deployment

After having a Minikube running, issue the command: 

```bash
make k8s-install-chart
```

The output of the command should be something like the following one:

```k8s-dep-update: updating dependencies
+++ Updating ska-pss-lmc-umbrella chart +++
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/gianluca/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/gianluca/.kube/config
Getting updates for unmanaged Helm repositories...
...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
...Successfully got an update from the "https://artefact.skao.int/repository/helm-internal" chart repository
Saving 9 charts
Downloading ska-tango-base from repo https://artefact.skao.int/repository/helm-internal
Downloading ska-tango-util from repo https://artefact.skao.int/repository/helm-internal
Downloading ska-tango-taranta from repo https://artefact.skao.int/repository/helm-internal
Downloading ska-taranta-auth from repo https://artefact.skao.int/repository/helm-internal
Downloading ska-tango-taranta-dashboard from repo https://artefact.skao.int/repository/helm-internal
Downloading ska-tango-tangogql from repo https://artefact.skao.int/repository/helm-internal
Downloading ska-tango-taranta-dashboard-pvc from repo https://artefact.skao.int/repository/helm-internal
Deleting outdated charts
Name:         ska-pss-lmc
Labels:       kubernetes.io/metadata.name=ska-pss-lmc
Annotations:  <none>
Status:       Active

No resource quota.

No LimitRange resource.
install-chart: install ./charts/ska-pss-lmc-umbrella/  release: test in Namespace: ska-pss-lmc with params: --set global.minikube=true  --set global.tango_host=tango-databaseds:10000 --set ska-tango-base.display=192.168.49.1:0 --set ska-tango-base.xauthority=/run/user/1000/gdm/Xauthority --set ska-tango-base.jive.enabled=false --set ska-pss-lmc.ska_pss_lmc.image.tag=0.1.0 --values charts/ska-pss-lmc-umbrella/values-default.yaml
helm upgrade --install test \
--set global.minikube=true  --set global.tango_host=tango-databaseds:10000 --set ska-tango-base.display=192.168.49.1:0 --set ska-tango-base.xauthority=/run/user/1000/gdm/Xauthority --set ska-tango-base.jive.enabled=false --set ska-pss-lmc.ska_pss_lmc.image.tag=0.1.0 --values charts/ska-pss-lmc-umbrella/values-default.yaml \
 ./charts/ska-pss-lmc-umbrella/  --namespace ska-pss-lmc
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /home/gianluca/.kube/config
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /home/gianluca/.kube/config
Release "test" does not exist. Installing it now.
NAME: test
LAST DEPLOYED: Thu Jul  7 12:08:14 2022
NAMESPACE: ska-pss-lmc
STATUS: deployed
REVISION: 1
TEST SUITE: None
```

To monitor the deployment progress, issue the command:

```
make k8s-watch
```

A successful deployment will show all the pods running as in the following:

```
NAME                       READY   STATUS              RESTARTS   AGE
cheetah-deployment-0       1/1     Running             0          24m
psspipelinectrl-ctrl1-0    1/1     Running             0          24m
ska-tango-base-tangodb-0   1/1     Running             0          24m
ska-tango-base-vnc-gui-0   1/1     Running             0          23m
ska-tango-base-vnc-gui-1   1/1     Running             0          23m
ska-tango-base-vnc-gui-2   1/1     Running             0          23m
tango-databaseds-0         1/1     Running             0          24m
tangotest-test-0           1/1     Running             0          24m

```

When all the pods are in running, you can access the
system via itango shell. This can be done from any pod running, using:

```
kubectl exec -it  <podname>  -n ska-pss-lmc -- itango3
```

then create a proxy to PSS.LMC device using, for example:

```
pss_pipeline = Device("common-csp/control/0")
...
```

When the proxy is created, it is possible to send commands and access attributes using a simple Python syntax. In the following there are some examples:

```
pss_pipeline.<command_name>(<command_argument>)

print(pss_pipeline.<attribute_name>)

csp_controller.<attribute_name> = <attribute_value>
```

To uninstall the system:

```
make k8s-uninstall-chart
```
## Tests

In this project integration tests are provided. To run them, after deploying the system (see section above), send the command:

```
make k8s-test
```
## Known bugs

## Troubleshooting

## License
See the [LICENSE](https://gitlab.com/ska-telescope/ska-pss-lmc/-/blob/master/LICENSE?ref_type=heads) file for details.
