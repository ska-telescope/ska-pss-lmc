{{- define "ska-pss-lmc.telescopeVars" -}}
  {{- $telescope := .Values.telescope | default "SKA-mid" | lower }}
  {{- if not (or (eq $telescope "ska-mid") (eq $telescope "ska-low")) }}
    {{- $telescope = "ska-mid" }}
  {{- end }}
  {{- $pss := printf "%s-pss" (index (split "-" $telescope) "_1") }}
  {{- printf "{\"telescope\": \"%s\", \"pss\": \"%s\"}" $telescope $pss }}
{{- end }}
