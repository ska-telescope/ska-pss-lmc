# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""This module implements a log parser. It is implemented as a consumer
in the producer consumer architecture.
"""

from __future__ import annotations  # allow forward references in type hints

import logging
import sys
import threading
from typing import Callable, Optional

# Create module logger that logs all levels
module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

# Create handler to output to standard out
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
handler.setFormatter(formatter)
module_logger.addHandler(handler)


class LogParserConsumer(threading.Thread):
    """Cheetah parser class"""

    def __init__(
        self,
        log_queue,
        log_parser_callback: Callable = None,
        logger: Optional[logging.Logger] = None,
    ):
        super().__init__(daemon=True)
        self.log_queue = log_queue
        self.running = False
        self.logger = logger or module_logger
        self._log_parser_callback = log_parser_callback
        # Map pipeline component manager methods to Log marker
        self.parser_action_methods = {
            "[error]": "_handle_log_error",
        }

    def run(self):
        """Parser thread (consumer) method that wait for the log queue message
        read by the component manager (producer)"""

        self.logger.info("Parser thread started")
        self.running = True
        while self.running:
            log_line = self.log_queue.get()  # Timeout to prevent blocking
            if log_line is None:
                self.logger.info(
                    "Detect a request to stop the log parser queue"
                )
                break  # Stop processing if termination signal is received

            # Execute specific methods for special keywords
            for keyword, method_name in self.parser_action_methods.items():
                if keyword in log_line:
                    self.logger.debug(
                        "Detected marker %s in log",
                        keyword,
                    )
                    self._log_parser_callback(method_name=method_name)

            self.log_queue.task_done()
        self.running = False

    def stop(self):
        """Stop the consumer queue sending a 'None' message"""
        self.running = False
        self.log_queue.put(None)  # Ensure the thread can exit
