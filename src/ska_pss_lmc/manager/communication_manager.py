# -*- coding: utf-8 -*-
#
# This file is part of the CspSubarray project
#
# INAF, Cosylab Switzerland - SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""
PSS Pipeline Communication Manager

Abstraction of communication methods with Cheetah pipeline application
"""
from __future__ import annotations

import abc
import enum
import io
import logging
import signal
import subprocess
import textwrap
import time
import xml.etree.ElementTree as ET
from typing import Iterator

import paramiko

module_logger = logging.getLogger(__name__)

# pylint: disable=broad-except


class Protocol(enum.IntEnum):
    """Enum class specifying available communication protocols for connection
    to the pipeline."""

    SSH = (0,)
    SHELL = (1,)
    HTTP = 2


class PipelineCommunicationManager(abc.ABC):
    """
    Communicate with Cheetah
    """

    _pid = None

    def __init__(
        self: PipelineCommunicationManager, logger: logging.Logger = None
    ):
        self.logger = logger or module_logger

    @property
    def cheetah_pid(self: PipelineCommunicationManager) -> int:
        """
        Return process id
        """
        return self._pid

    @abc.abstractmethod
    def connect(self: PipelineCommunicationManager) -> bool:
        """
        Establish connection

        :return: successful of connection
        """
        raise NotImplementedError

    @abc.abstractmethod
    def disconnect(self: PipelineCommunicationManager):
        """
        Close connection
        """
        raise NotImplementedError

    @abc.abstractmethod
    def start(self: PipelineCommunicationManager, cmd: str):
        """
        Start Cheetah pipeline

        :param cmd: command to be executed
        """
        raise NotImplementedError

    @abc.abstractmethod
    def is_running(self: PipelineCommunicationManager) -> bool:
        """
        Return if cheetah is currently running

        :return: if cheetah process is running
        """
        raise NotImplementedError

    @abc.abstractmethod
    def kill(self: PipelineCommunicationManager):
        """
        Kill Cheetah pipeline process
        """
        raise NotImplementedError

    def shutdown(self: PipelineCommunicationManager):
        """
        Gracefully shutdown cheetah, not yet supported in cheetah
        """
        raise NotImplementedError

    @abc.abstractmethod
    def write_config(
        self: PipelineCommunicationManager,
        config: ET,
    ):
        """
        Write cheetah configuration

        :param config: cheetah xml config
        :type config: xml.etree.ElementTree
        """
        raise NotImplementedError

    def reload_config(self: PipelineCommunicationManager):
        """
        Reload configuration while cheetah is running.
        Mentioned by chris as future potential addition to cheetahs signals
        Unclear if this actually can be mapped to the obs state machine
        Maybe we would just shut down cheetah, and reload with the new config
        """
        raise NotImplementedError

    def delete_config(self: PipelineCommunicationManager):
        """
        Delete the configuration file from the PSS node.
        To be invoked by ObsReset
        """
        raise NotImplementedError

    @abc.abstractmethod
    def get_logs(self: PipelineCommunicationManager) -> Iterator[str]:
        """
        Get logs from Cheetah process

        :return: iterator over lines of log entry
        """
        raise NotImplementedError


class SshAccess(PipelineCommunicationManager):
    """
    Access Cheetah over ssh

    NOTE: there are several exceptions that paramiko library might throw
    when trying to connect to the ssh server, executing commands, etc. They
    shall be handled at component manager level.
    """

    client = None
    host = None
    user = None
    password = None
    private_key_path = None
    log_input = None

    def __init__(
        self: SshAccess,
        host: str,
        user: str,
        password: str = None,
        private_key: str = None,
        logger: logging.Logger = None,
    ):  # pylint: disable=too-many-arguments
        """
        Create ssh access object to Cheetah
        :param host: host to connect to e.g. localhost, 192.168.1.3

        :param user: user to log in as

        :param password: password

        :param private_key_path: path to private key
        """
        super().__init__(logger=logger)
        self.client = paramiko.client.SSHClient()
        self.client.load_system_host_keys()
        self.host = host
        self.user = user
        self.password = password
        self.private_key = private_key

    def __del__(self: SshAccess):
        """
        Destructor, close ssh connection
        """
        self.disconnect()

    def connect(self: SshAccess) -> bool:
        """
        Establish connection
        """
        if self.private_key:
            # This function wraps the base64 string at 64 characters per line,
            # which is required by OpenSSH.
            wrapped_key = textwrap.fill(self.private_key, width=64)

            formatted_key = f"""-----BEGIN OPENSSH PRIVATE KEY-----
            {wrapped_key}
            -----END OPENSSH PRIVATE KEY-----"""
            private_key = paramiko.Ed25519Key.from_private_key(
                io.StringIO(formatted_key)
            )
        else:
            private_key = None
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(
            hostname=self.host,
            username=self.user,
            password=self.password,
            pkey=private_key,
        )
        # Check if there is a cheetah process running
        self.fetch_pid()
        if not self.is_running():
            self._pid = None

        return True

    def disconnect(self: SshAccess):
        """
        Close connection
        """
        self.client.close()

    def start(self: SshAccess, cmd: str):
        """
        Start Cheetah pipeline

        : return: the exit status of the cheetah process
        """
        # run the pipeline executable in background, get its PID and wait
        # for its end, saving the exitcode in a file
        self.client.exec_command(
            (
                f"nohup {cmd} > cheetah.log 2>&1 & echo $! > pid.txt; "
                f"wait $(cat pid.txt); echo $? > exit_status.txt"
            )
        )
        self.fetch_pid()

    def get_exit_status(self: SshAccess):
        """
        Retrieve the exit code of cheetah process
        """
        _, stdout, _ = self.client.exec_command("cat exit_status.txt")
        _exit_status = stdout.readline()
        self.logger.info(f"exit status: {_exit_status} {type(_exit_status)}")
        try:
            exit_status = int(_exit_status)
        except Exception as e:
            self.logger.info(f"FAILED exit status {e}")
            exit_status = -1
        return exit_status

    def fetch_pid(self: SshAccess):
        """
        Fetch pid stored on cheetah host
        """
        try:
            _, stdout, _ = self.client.exec_command("cat pid.txt")
            line = stdout.readline()
            self._pid = int(line)
            self.logger.info(f"Cheetah pid is {self._pid}")
        except Exception:
            self.logger.warning("File pid.txt is not present")

    def is_running(self: SshAccess) -> bool:
        """
        Check if the pipeline is running on the server

        return: a flag that states whether the pipeline is running
        """
        if self._pid is None:
            return False

        _, stdout, _ = self.client.exec_command(
            "kill -s 0 " + str(self._pid) + " && echo RUNNING"
        )
        line = stdout.readline()
        if line.replace("\n", "") == "RUNNING":
            return True
        return False

    def kill(self: SshAccess):
        """
        Kill Cheetah pipeline process
        """
        _, stdout, _ = self.client.exec_command("kill " + str(self._pid))
        # Sleep so it won't be detected as still running immediately after
        time.sleep(0.1)
        # wait until command finishes
        stdout.channel.recv_exit_status()
        self._pid = None
        self._close_logs()

    def get_logs(self: SshAccess):
        """
        Get logs from Cheetah process

        :return: iterator over lines of log entry
        """
        stdin, stdout, _ = self.client.exec_command(
            "tail -f cheetah.log", get_pty=True
        )
        self.log_input = stdin
        while True:
            line = stdout.readline()
            if not line:
                break
            yield line

    def _close_logs(self: SshAccess):
        """
        Send Ctrl+c to the tail process

        Close the log reading process
        """
        if self.log_input is not None:
            self.log_input.write("\x03")
            self.log_input.flush()
            self.log_input = None

    def write_config(
        self: SshAccess,
        config: ET,
    ):
        """
        Write cheetah configuration file.


        :param config: cheetah xml config
        :type config: xml.etree.ElementTree
        """
        sftp = self.client.open_sftp()
        cfg_file = sftp.file("config.xml", "a", -1)
        cfg_file.write(config)
        cfg_file.flush()
        sftp.close()

    def delete_config(self: SshAccess):
        """
        Delete cheetah configuration file.
        TODO: to be implemented
        """


# Early code for Subprocess access, to be implemented and tested later
class SubprocessAccess(PipelineCommunicationManager):
    """
    Access Cheetah via a subprocess
    """

    process = None

    def __init__(
        self: SubprocessAccess, pid=None, logger: logging.Logger = None
    ):  # pylint:disable=unused-argument
        """
        Create subprocess access object to Cheetah
        """
        super().__init__(logger=logger)

    def connect(self: SubprocessAccess):
        """Connect"""

    def disconnect(self: SubprocessAccess):
        """Disconnect"""

    def start(self: SubprocessAccess, cmd: str):
        """Start cheetah process"""
        stdin, stdout, stderr = None, None, None
        self.process = subprocess.Popen(  # pylint: disable=consider-using-with
            ["watch", cmd, "-l"],
            stdin=stdin,
            stdout=stdout,
            stderr=stderr,
            shell=False,
        )

    def kill(self: SubprocessAccess):
        """kill cheetah process"""
        # send SIGINT signal to cheetah
        self.process.send_signal(signal.SIGINT)
        # wait for process to terminate
        self.process.wait()

    def write_config(self: SubprocessAccess, config):
        """write cheetah config"""
        # file system write
        # pylint: disable=consider-using-with,unspecified-encoding
        cfg_file = open("config.xml", "w")
        cfg_file.write(config)
        cfg_file.close()

    def delete_config(self: SshAccess):
        """
        Delete cheetah configuration file.
        TODO: to be implemented
        """
