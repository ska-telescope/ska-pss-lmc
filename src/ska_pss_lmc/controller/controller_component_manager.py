# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""This module implements a functionality for the PSS
 Controller component manager.
 """

from __future__ import annotations  # allow forward references in type hints

import concurrent.futures
import logging
import sys
import threading
from functools import partial
from typing import Callable, Dict, List, Optional, Tuple

from ska_control_model import (
    AdminMode,
    CommunicationStatus,
    HealthState,
    PowerState,
    TaskStatus,
)
from ska_tango_base.base import TaskCallbackType
from ska_tango_base.executor import TaskExecutorComponentManager
from tango import EventData

from ska_pss_lmc.common.capability import PssCapability
from ska_pss_lmc.common.connector import Connector

# Create module logger that logs all levels
module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

# Create handler to output to standard out
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
handler.setFormatter(formatter)
module_logger.addHandler(handler)


# pylint: disable=too-many-arguments
# pylint: disable=unused-argument
# pylint: disable=broad-except
# pylint: disable=logging-fstring-interpolation

MAX_WORKERS = 5


class PssControllerComponentManager(TaskExecutorComponentManager):
    """A base component manager for PSS Pipeline Control Device."""

    def __init__(
        self: PssControllerComponentManager,
        max_workers: int,
        properties: List[str],
        communication_status_changed_callback: Callable,
        component_state_changed_callback: Callable,
        update_device_attribute_cbk: Callable,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """
        The Component Manager for the PSS controller software component.
        :param max_worker: the maximum number of worker threads for
            long running commands
        :param properties: A class instance whose properties are the
            Pss Controller device properties.
        :param communication_status_changed_callback: callback to be
            called when the status of the communication with the component
            changes
        :param component_state_changed_callback: callback to be called
            when the status of the component changes
        :param update_device_attribute_cbk: The Pipeline Controller method
        invoked to update the properties. Defaults to None.
        :param logger: a logger for this instance to use.
        """

        self.logger = logger or module_logger
        super().__init__(
            logger=self.logger,
            max_workers=max_workers,
            communication_state_callback=communication_status_changed_callback,
            component_state_callback=component_state_changed_callback,
            power=PowerState.UNKNOWN,
            fault=None,
        )
        self.admin_mode = AdminMode.OFFLINE
        self._proxies = []
        self.pipeline_fqdns = []
        self.subarray_fqdns = []
        self._update_device_attribute = update_device_attribute_cbk
        self._ctrl_executor = concurrent.futures.ThreadPoolExecutor(
            max_workers=MAX_WORKERS,
            thread_name_prefix="CtrlThreadPool",
        )

        self._init_properties(properties)
        self._init_connector()
        self._create_pss_capability()

    @property
    def is_communicating(self: PssControllerComponentManager) -> bool:
        """
        Return whether communication with the component is established.

        :return: whether there is currently a connection to the
            component
        """
        return self.communication_state == CommunicationStatus.ESTABLISHED

    @property
    def pipelines_json(self: PssControllerComponentManager) -> str:
        """
        Return a JSON string with the capability internal.
        """
        return self.pss_capability.json_str

    @property
    def pipelines_fqdn(self: PssControllerComponentManager) -> List[str]:
        """
        Return a list with the FQDNs of the connected devices.
        """
        return self.pss_capability.get_dict_entry("fqdn")

    @property
    def pipelines_state(self: PssControllerComponentManager) -> List[str]:
        """
        Return a list with the devices FQDNs.
        """
        return self.pss_capability.get_dict_entry("state")

    @property
    def pipelines_adminmode(self: PssControllerComponentManager) -> List[str]:
        """
        Return a list with the devices'adminmode.
        """
        return self.pss_capability.get_dict_entry("adminmode")

    @property
    def pipelines_healthstate(
        self: PssControllerComponentManager,
    ) -> List[str]:
        """
        Return a list with the devices FQDNs.
        """
        return self.pss_capability.get_dict_entry("healthstate")

    @property
    def pipelines_obsstate(self: PssControllerComponentManager) -> List[str]:
        """
        Return a list with the devices' obsState.
        """
        return self.pss_capability.get_dict_entry("obsstate")

    @property
    def pipelines_connected(self: PssControllerComponentManager) -> List[str]:
        """
        Return a list with the devices' connection status.
        """
        return self.pss_capability.get_dict_entry("iscommunicating")

    #
    # Public methods
    #
    def start_communicating(self: PssControllerComponentManager) -> None:
        """
        Try to open a connection with the PSS Node.

        Open a channel with this instance of the component manager
        and the software component (cheetah pipeline).
        Submit the _start_communicating method
        Initial communication status is DISABLED.
        """
        self.submit_task(
            self._start_communicating,
            task_callback=self._start_communicating_callback,
        )

    def stop_communicating(self: PssControllerComponentManager) -> None:
        """
        Close connection with the software component.

        Close the ssh channel between this instance of the component manager
        and the controlled software component (cheetah).
        The communication state variable is set to DISCONNECTED and
        the adminMode to OFFLINE.
        """
        if self.communication_state == CommunicationStatus.DISABLED:
            return
        self._forward_adminmode()

        # transition the state to UNKNOWN before disabling the device
        self._update_component_state(power=PowerState.UNKNOWN, fault=None)
        # Note: this method of the component manager invokes the
        # _communication_state_changed method of the device that
        # in turn calls the proper action on the operational state
        # model.
        self._update_communication_state(CommunicationStatus.DISABLED)
        self._update_device_attribute("healthstate", HealthState.UNKNOWN)

    def on(
        self: PssControllerComponentManager,
        task_callback: TaskCallbackType | None = None,
    ):
        self.logger.info("Method not implemented")
        return TaskStatus.REJECTED, "Controller is always ON."

    def off(
        self: PssControllerComponentManager,
        task_callback: TaskCallbackType | None = None,
    ):
        self.logger.info("Method not implemented")
        return TaskStatus.REJECTED, "Off command is not used"

    def standby(
        self: PssControllerComponentManager,
        task_callback: TaskCallbackType | None = None,
    ):
        self.logger.info("Method not implemented")
        return TaskStatus.REJECTED, "Standby command is not used"

    def reset(
        self: PssControllerComponentManager,
        task_callback: TaskCallbackType | None = None,
    ):
        self.logger.info("Method to implemented")

    #
    # Protected methods
    #

    def _init_properties(self: PssControllerComponentManager, properties):
        if hasattr(properties, "PipelineFqdns"):
            if isinstance(properties.PipelineFqdns, str):
                if properties.PipelineFqdns:
                    self.pipeline_fqdns = [
                        properties.PipelineFqdns,
                    ]
            else:
                # properties.PipelineFqdns is of StdStringVector
                # cast to list
                self.pipeline_fqdns = list(properties.PipelineFqdns)
        if hasattr(properties, "PssSubarrays"):
            if isinstance(properties.PssSubarrays, str):
                if properties.PssSubarrays:
                    self.subarray_fqdns = [
                        properties.PssSubarrays,
                    ]
            else:
                self.subarray_fqdns = list(properties.PssSubarrays)

    def _init_connector(self: PssControllerComponentManager):
        try:
            self._connector = Connector(
                self.logger,
            )
        except Exception as err:
            self.logger.error(
                "Error in instantiating Connector or Subarrays " f"{err}"
            )

    def _create_pss_capability(self: PssControllerComponentManager):
        try:
            self.pss_capability = PssCapability(
                self.pipeline_fqdns, self._update_device_attribute
            )
        except Exception as err:
            self.logger.error(
                "Error in instantiating PssCapability object: " f"{err}"
            )

    def _forward_adminmode(self: PssControllerComponentManager):
        """
        Forward the adminMode value to all the subordinate devices
        of the PSS Controller.
        """
        for fqdn, proxy in self._connector.proxies.items():
            try:
                proxy.adminmode = self.admin_mode
            except ValueError:
                self.logger.info(
                    "Error in setting the adminmode "
                    f"on {fqdn} to value {self.admin_mode}"
                )

    def _evaluate_ctrl_health(
        self: PssControllerComponentManager,
        subarrays_health: Dict[str, HealthState],
    ) -> HealthState:
        """
        Evaluate the Controller health state from the
        Subarray and pipelines state/healthstate

        Still to implement.

        All subarrays FAILED -> Ctrl health FAILED
        All devices OK -> Ctrl health OK
        otherwise Ctrl health DEGRADED
        """
        # read the state of the pipelines
        # read the state/healthstate of the subarrays
        # evaluate the Controller healthstate
        return HealthState.OK

    def _evaluate_ctrl_communication_status(
        self: PssControllerComponentManager, communication_status_lst
    ) -> Tuple[CommunicationStatus, HealthState]:
        """Evaluate the connection status of the PSS Controller device.

        :param communications_status_lst: a list with the communication
            status with the subordinate devices: subarrays and pipelines.

        :return: a tuple with the PSS Controller CommunicationStatus and
            its healthState.The healthState is reported as:

            - FAILED if all the communication status are NOT_ESATBLISH
            - OK if all the communication status are ESTABLISHED
            - DEGRADED if at least one of the communication status is
                NOT_ESATBLISH
        """
        health = HealthState.UNKNOWN
        if all(
            status == CommunicationStatus.NOT_ESTABLISHED
            for status in communication_status_lst.values()
        ):
            comm_status, health = (
                CommunicationStatus.NOT_ESTABLISHED,
                HealthState.FAILED,
            )
        elif all(
            status == CommunicationStatus.ESTABLISHED
            for status in communication_status_lst.values()
        ):
            comm_status, health = (
                CommunicationStatus.ESTABLISHED,
                HealthState.OK,
            )
        else:
            _health = HealthState.DEGRADED
            # if all the subarrays are not connected, the health state
            # is reported as FAILED
            if all(
                status == CommunicationStatus.NOT_ESTABLISHED
                for fqdn, status in communication_status_lst.items()
                if "subarray" in fqdn
            ):
                _health = HealthState.FAILED
            comm_status, health = (
                CommunicationStatus.ESTABLISHED,
                _health,
            )
        return comm_status, health

    def _connect_and_subscribe(
        self: PssControllerComponentManager, device_fqdn: str
    ) -> Tuple[str, CommunicationStatus]:
        """
        Establishes a connection to the specified device and subscribes to its
        relevant attributes.

        :param device_fqdn: The TANGO fully qualified domain name (FQDN) of
                the device.

        :return: A tuple containing the connection status and the device FQDN.
        """
        subarrays_health = {}

        def evt_callback(subarrays_health: Dict, event: EventData):
            """Callback invoked"""
            subarrays_health = {}
            if not event.err:
                attr_name = event.attr_value.name.lower()
                attr_value = event.attr_value.value
                device_fqdn = event.device.name()
                if attr_name.lower() == "healthstate":
                    subarrays_health[device_fqdn] = attr_value
                    ctrl_health = self._evaluate_ctrl_health(subarrays_health)
                    self.logger.debug(
                        f"Controller evaluate healthstate: {ctrl_health}"
                    )
                    # self._update_device_attribute("healthstate", ctrl_health)
            else:
                self.logger.warning(
                    f"Received event error on {event.attr_name}"
                )

        def get_subscription_data(device_fqdn) -> Tuple[List[str], Callable]:
            """
            Retrieves the list of attributes to subscribe to and the
            corresponding callback for handling change events.The subscription
            details vary based on the device family.

            :param device_fqdn: The TANGO FQDN of the device.

            :return: A tuple containing the list of attributes and the
                associated callback function.
            """
            attr_list, callback = [], None
            if "subarray" in device_fqdn:
                attr_list, callback = (
                    ["state", "healthstate"],
                    partial(evt_callback, subarrays_health),
                )
            elif "pipeline" in device_fqdn:
                attr_list, callback = (
                    self.pss_capability.pipeline_attributes,
                    self.pss_capability.update_pipeline_info,
                )
            else:
                self.logger.info(
                    "Nothing to subscribe for device %s", {device_fqdn}
                )
            return (attr_list, callback)

        status = self._connector.connect(device_fqdn)
        if status == CommunicationStatus.ESTABLISHED:
            # retrieve the list of attribute to subscribe and the callback
            # to invoke for the subarray and pipeline devices.
            attribute_list, callback = get_subscription_data(device_fqdn)
            proxy = self._connector.get_device(device_fqdn)
            if attribute_list and callback:
                self._connector.subscribe(proxy, attribute_list, callback)
        return device_fqdn, status

    def _start_communicating_callback(
        self: PssControllerComponentManager,
        status: TaskStatus,
        communication_status: CommunicationStatus = None,
        exception: Exception = None,
        # result=None,
        message: str = None,
    ) -> None:
        """
        Task callback passed when submitting the task of _start_communicating.

        :param: status: the status of the task
        :param: communicationStatus: the status of communication to be
            reported to the Pipeline device
        :param: exception: possible exception raised when submitting the task
        :param: message: possible message received when submitting the task
        """
        if status and status == TaskStatus.QUEUED:
            return
        self.logger.info(
            f"status: {status} communication_status:{communication_status}"
            f" exception: {exception}"
        )
        if exception:
            self.logger.error(
                f"Start communicating failed with exception: {exception}"
            )

        # Note: in this case the state of the device is DISABLE, and
        # it can transition to FAULT: next call can't be invoked.
        # self._update_component_state(fault=True)
        # If the connection fails when the device is in DISABLE, the
        # communication status has to be set to NOT_ESTABLISHED.
        # In this case the state of the device is set (by the operational
        # state machine) to UNKNOWN (with adminmode = ONLINE/MAINTENANCE)

        self.logger.info(
            f"Task status: {TaskStatus(status).name} communication_status: "
            f"{communication_status} {self.communication_state}"
        )
        if message:
            self.logger.info(message)

        if communication_status is not None:
            self._update_communication_state(communication_status)
        if status is not None:
            if status == TaskStatus.COMPLETED:
                self._update_device_attribute(
                    "isCommunicating", self.is_communicating
                )
                self._update_component_state(power=PowerState.ON)

            elif status == TaskStatus.FAILED:
                self._update_device_attribute(
                    "healthstate", HealthState.FAILED
                )
            else:
                pass

    def _start_communicating(
        self: PssControllerComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Task submitted by start_communicating.

        On failure the communication status is reported as NOT_ESTABLISHED
        and the adminMode as ONLINE/MAINTENANCE.
        The state of the device is set to UNKNOWN.
        Need to re-try the connection.

        :param: task_callback: Task callback passed during the
            submission of the task
        :param: task_abort_event: the abort event (still not implemented)
        """
        ctrl_communication_status = CommunicationStatus.NOT_ESTABLISHED
        task_status = TaskStatus.FAILED
        futures = []
        _communication_status = {
            fqdn: CommunicationStatus.NOT_ESTABLISHED
            for fqdn in self.pipeline_fqdns + self.subarray_fqdns
        }
        connect_exception = None
        try:
            futures = [
                self._ctrl_executor.submit(
                    # self._connector.connect_and_subscribe, fqdn
                    self._connect_and_subscribe,
                    fqdn,
                )
                for fqdn in self.subarray_fqdns + self.pipeline_fqdns
            ]
            # synchronize to wait for the end of connection
            concurrent.futures.wait(futures)
            # collect the communication status of the connections
            for future in futures:
                device_fqdn, comm_status = future.result()
                _communication_status[device_fqdn] = comm_status
            self._forward_adminmode()
            (
                ctrl_communication_status,
                ctrl_health_state,
            ) = self._evaluate_ctrl_communication_status(_communication_status)
            self.logger.debug(
                "Controller communication health state is "
                f"{HealthState(ctrl_health_state).name}"
            )
            self._update_device_attribute("healthstate", ctrl_health_state)
            if ctrl_communication_status == CommunicationStatus.ESTABLISHED:
                task_status = TaskStatus.COMPLETED
        except Exception as err:
            connect_exception = err
            self.logger.error(
                "Failure in connection to subordinate devices %s",
                str(connect_exception),
            )
        task_callback(
            status=task_status,
            communication_status=ctrl_communication_status,
            exception=connect_exception,
        )
