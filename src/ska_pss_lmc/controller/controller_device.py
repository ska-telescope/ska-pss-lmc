# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""PSS Controller Tango module."""
from __future__ import annotations  # allow forward references in type hints

from typing import Any, List

from ska_control_model import AdminMode
from ska_tango_base import SKABaseDevice
from tango import AttrWriteType
from tango.server import attribute, device_property, run

from ska_pss_lmc.controller.controller_component_manager import (
    PssControllerComponentManager,
)
from ska_pss_lmc.manager.manager_configuration import (
    ComponentManagerConfiguration,
)

# pylint: disable=too-many-arguments
# pylint: disable=logging-fstring-interpolation
# pylint: disable=unused-argument
# pylint: disable=broad-except


class PssController(SKABaseDevice):
    """
    PSS Controller Tango device.

        **Device Properties:**

            PipelineFqdns
                - The pipelines' FQDNs
                - Type:'DevVarStringArray'
            PssSubarrays
                - The Subarrays' FQDNs
                - Type:'DevVarStringArray'
    """

    # pylint: disable=attribute-defined-outside-init
    def _init_state_model(self: PssController) -> None:
        """
        Override base method.

        Configure some device attributes to push events from the code.
        """
        super()._init_state_model()
        self.set_change_event("isCommunicating", True, False)

    def create_component_manager(
        self: PssController,
    ) -> PssControllerComponentManager:
        """
        Create and return the Pss Pipeline Control Component Manager.
        """
        cm_configuration = ComponentManagerConfiguration(
            self.get_name(), self.logger
        )
        cm_configuration.add_attributes()
        return PssControllerComponentManager(
            1,
            cm_configuration,
            self._communication_state_changed,
            self._component_state_changed,
            self.update_attribute,
            self.logger,
        )

    def update_attribute(
        self: PssController, attribute_name: str, attribute_value: Any
    ) -> None:
        """
        General method invoked to push an event on a device attribute.


        :param attribute_name: the TANGO attribute name
        :param attribute_value: the attribute value
        """
        # add check on attribute configuration for events
        if attribute_name == "adminmode":
            self._update_admin_mode(attribute_value)
        elif attribute_name == "state":
            self._update_state(attribute_value)
            return
        elif attribute_name == "healthstate":
            self._update_health_state(attribute_value)
            return
        self.push_change_event(attribute_name, attribute_value)
        return

    # -----------------
    # Device Properties
    # -----------------

    PipelineFqdns = device_property(
        dtype="DevVarStringArray",
    )
    PssSubarrays = device_property(
        dtype="DevVarStringArray",
    )

    # ----------
    # Attributes
    # ----------

    @attribute(
        label="isCommunicating",
        dtype="DevBoolean",
        access=AttrWriteType.READ,
        doc="Whether the device is communicating with the component under"
        "control",
    )
    def isCommunicating(self: PssController) -> bool:
        """
        Return the device communicating status.

        Return a boolean flag indicating whether the TANGO device is
        communicating with the controlled component.
        """
        return self.component_manager.is_communicating

    adminMode = attribute(
        dtype=AdminMode,
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
        doc=(
            "The admin mode reported for this device. It may interpret the"
            "current device condition and condition of all managed devices"
            "to set this. Most possibly an aggregate attribute."
        ),
    )

    def read_adminMode(self: PssController) -> AdminMode:
        """
           Read the Admin Mode of the device.

           It may interpret the current device condition and condition of
           all managed devices to set this. Most possibly an aggregate
        attribute.

           :return: Admin Mode of the device
        """
        return self._admin_mode

    def write_adminMode(self: PssController, value: AdminMode) -> None:
        """
        Set the Admin Mode of the device.

        :param value: Admin Mode of the device.

        :raises ValueError: for unknown adminMode
        """
        self.component_manager.admin_mode = value
        if value == AdminMode.NOT_FITTED:
            self.admin_mode_model.perform_action("to_notfitted")
        elif value == AdminMode.OFFLINE:
            self.admin_mode_model.perform_action("to_offline")
            self.component_manager.stop_communicating()
        elif value == AdminMode.ENGINEERING:
            self.admin_mode_model.perform_action("to_engineering")
            self.component_manager.start_communicating()
        elif value == AdminMode.ONLINE:
            self.admin_mode_model.perform_action("to_online")
            self.component_manager.start_communicating()
        elif value == AdminMode.RESERVED:
            self.admin_mode_model.perform_action("to_reserved")
        else:
            raise ValueError(f"Unknown adminMode {value}")

    @attribute(
        label="pipelinesState",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=4,
        doc="The state of pipelines",
    )
    def pipelinesState(self: PssController) -> List[str]:
        """Return the state of the PSS pipelines"""
        return self.component_manager.pipelines_state

    @attribute(
        label="pipelinesHealthState",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=4,
        doc="The healthState of pipelines",
    )
    def pipelinesHealthState(self: PssController) -> List[str]:
        """Return the health state of the PSS pipelines"""
        return self.component_manager.pipelines_healthstate

    @attribute(
        label="pipelinesObsState",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=4,
        doc="The obsState of pipelines",
    )
    def pipelinesObsState(self: PssController) -> List[str]:
        """Return the observing state of the PSS pipelines"""
        return self.component_manager.pipelines_obsstate

    @attribute(
        label="pipelinesAdminMode",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=4,
        doc="The adminmode of pipelines",
    )
    def pipelinesAdminMode(self: PssController) -> List[str]:
        """Return the observing state of the PSS pipelines"""
        return self.component_manager.pipelines_adminmode

    @attribute(
        label="pipelinesIsCommunicating",
        dtype=("DevBoolean",),
        access=AttrWriteType.READ,
        max_dim_x=4,
        doc=(
            "Boolean list of pipelines indicating whether the PSS"
            " pipelines are communicating"
        ),
    )
    def pipelinesIsCommunicating(self: PssController) -> List[str]:
        """Return a boolean list indicating whether the PSS pipelines are
        communicating"""
        return self.component_manager.pipelines_connected

    @attribute(
        label="pipelinesFqdns",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=4,
        doc="The pipelines' FQDNs",
    )
    def pipelinesFqdns(self: PssController) -> List[str]:
        """Return the pipelines' TANGO FQDNs"""
        return self.PipelineFqdns

    @attribute(
        label="pipelinesJson",
        dtype=str,
        access=AttrWriteType.READ,
        doc="JSON string with all the pipelines' info",
    )
    def pipelinesJson(self: PssController) -> str:
        """Return a JSON string with all pipelines's info"""
        return self.component_manager.pipelines_json


def main(args=None, **kwargs):
    """Run PssController."""
    return run((PssController,), args=args, **kwargs)


if __name__ == "__main__":
    main()
