# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""
This module implements a functionality for component managers in PSS LMC.
"""

from __future__ import annotations  # allow forward references in type hints

import json
import logging
import queue
import signal
import sys
import threading
import time
import xml.dom.minidom
import xml.etree.ElementTree as ET
from typing import Callable, Optional, Tuple

from json2xml import utils
from paramiko import SSHException
from ska_control_model import (
    CommunicationStatus,
    PowerState,
    ResultCode,
    TaskStatus,
)
from ska_tango_base.executor import TaskExecutorComponentManager

from ska_pss_lmc.manager.communication_manager import SshAccess
from ska_pss_lmc.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_pss_lmc.parser import LogParserConsumer

# pylint: disable=abstract-method
# pylint: disable=unused-variable
# pylint: disable=unused-argument
# pylint: disable=logging-not-lazy
# pylint: disable=invalid-name
# pylint: disable=too-many-instance-attributes
# pylint: disable=too-many-arguments
# pylint: disable=broad-except
# pylint: disable=logging-fstring-interpolation
# pylint: disable=consider-using-dict-items
# pylint: disable=fixme

# Create module logger that logs all levels
module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

# Create handler to output to standard out
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
handler.setFormatter(formatter)
module_logger.addHandler(handler)


def _from_json_to_xml(json_input: str) -> str:
    """
    Convert json cheetah scan configuration to xml.
    """

    def build_xml_element(key, value, parent_element):
        """
        Recursively build xml. This is not infinitely
        generalised and requires a specific json layout.
        This will grow as we make the scan configuration
        more complicated. For example - this function will
        not convert a json configuration that has more than
        one beam.
        """
        if isinstance(value, dict):
            element = ET.Element(key)
            parent_element.append(element)
            for subkey, subvalue in value.items():
                build_xml_element(subkey, subvalue, element)
        elif isinstance(value, list):
            for item in value:
                if isinstance(item, dict):
                    if key == "sink":
                        build_xml_element("sink", item, parent_element)
                    elif key == "dedispersion":
                        build_xml_element("dedispersion", item, parent_element)
                    else:
                        element = ET.Element(key)
                        build_xml_element(key, item, element)
                        parent_element.append(element)
                else:
                    element = ET.Element(key)
                    element.text = str(item)
                    parent_element.append(element)
        else:
            element = ET.Element(key)
            element.text = str(value)
            parent_element.append(element)

    def prettify(elem):
        """
        Produce "pretty" version of xml string
        (instead of one-liner)
        """
        rough_string = ET.tostring(elem, "utf-8")
        parsed = ET.fromstring(rough_string)
        return ET.tostring(parsed, encoding="utf-8", method="xml").decode(
            "utf-8"
        )

    # Build XML
    root = ET.Element("cheetah")
    json_input_dict = utils.readfromstring(json_input)
    for key, value in json_input_dict.items():
        build_xml_element(key, value, root)

    # Pretty print
    xml_str = prettify(root)
    pretty_xml = xml.dom.minidom.parseString(xml_str).toprettyxml(indent="  ")

    return pretty_xml


class PipelineCtrlComponentManager(TaskExecutorComponentManager):
    """A base component manager for PSS Pipeline Control Device."""

    def __init__(
        self: PipelineCtrlComponentManager,
        max_workers: int,
        properties: ComponentManagerConfiguration,
        communication_status_changed_callback: Callable,
        component_state_changed_callback: Callable,
        update_device_attribute_cbk: Callable,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """
        The Component Manager for the PSS pipeline software component.
        :param max_worker: the maximum number of worker threads for
            long running commands
        :param properties: A class instance whose properties are the
            Pipeline Controller device properties.
        :param communication_status_changed_callback: callback to be
            called when the status of the communication with the component
            changes
        :param component_state_changed_callback: callback to be called
            when the status of the component changes
        :param update_device_attribute_cbk: The Pipeline Controller method
        invoked to update the properties. Defaults to None.
        :param logger: a logger for this instance to use.
        """
        self.device_properties = properties
        self.communication_manager = self.create_communication_manager(logger)
        self._update_device_attribute = update_device_attribute_cbk
        self.logger = logger or module_logger
        self._cheetah_pid = -1
        self._log_line = None
        self._json_config_script = ""

        self._pipeline_progress = 0
        self._subarray_id = 0
        self.abort_callback = None
        self.log_queue = queue.Queue(100)
        self.config_scan_lock = threading.Lock()
        self.parser = LogParserConsumer(
            log_queue=self.log_queue,
            log_parser_callback=self.log_parser_callback,
            logger=self.logger,
        )
        self.parser.start()

        super().__init__(
            logger=self.logger,
            max_workers=max_workers,
            communication_state_callback=communication_status_changed_callback,
            component_state_callback=component_state_changed_callback,
            power=PowerState.UNKNOWN,
            fault=None,
            resourced=False,
            configured=False,
            scanning=False,
            obsfault=False,
            force="IDLE",
        )

    def __del__(self):
        """Stop parser thread on exit"""
        self.parser.stop()

    # @property
    # def pipeline_configured(self: PipelineCtrlComponentManager) -> bool:
    #     """
    #     Return a flag that states if the pipeline is already configured. 3

    #     It is used in case of re-configuration by the _configure_scan
    #     method to properly trigger the state machine.
    #     """
    #     return self._pipeline_configured

    @property
    def is_communicating(self: PipelineCtrlComponentManager) -> bool:
        """
        Return whether communication with the component is established.

        :return: whether there is currently a connection to the
            component
        """
        return self.communication_state == CommunicationStatus.ESTABLISHED

    @property
    def log_line(self: PipelineCtrlComponentManager) -> str:
        """
        Stores the last line of pipeline log
        """
        return self._log_line

    @property
    def pipeline_progress(self: PipelineCtrlComponentManager) -> int:
        """
        Stores the last line of percentage progress of the pipeline
        """
        return self._pipeline_progress

    @property
    def cheetah_version(self: PipelineCtrlComponentManager) -> str:
        """
        Return the version of Cheetah pipeline in use.

        NOTE: to be implemented
        """
        return "x.y.z"

    @property
    def cheetah_pid(self: PipelineCtrlComponentManager) -> int:
        """
        Return the pid of the cheetah process
        """
        return self._cheetah_pid

    @property
    def scan_configuration(self: PipelineCtrlComponentManager) -> str:
        """
        Return the JSON script used to configure the pipeline.
        """
        return self._json_config_script

    @property
    def subarray_id(self: PipelineCtrlComponentManager) -> int:
        """
        Return the subarray_membership.
        """
        return self._subarray_id

    @subarray_id.setter
    def subarray_id(self: PipelineCtrlComponentManager, value):
        """
        Set the subarray_membership.

        :raises: ValueError if the value is invalid
        """
        if value < 1 or value > 16:
            raise ValueError(
                f"Invalid request: {value} is outside range [1,16]"
            )
        self._subarray_id = value

    # def set_pipeline_configure(self, value: bool):
    #     with self.config_scan_lock:
    #         self._pipeline_configured = value

    def log_parser_callback(self, method_name) -> None:
        """Callback method called by the log parser class to execute
        component manager methods

        :param method_name: component manager method to be called by the
            callback
        """
        try:
            method = getattr(self, method_name)
            method()
        except AttributeError as exc:
            self.logger.warning(
                f"Parser method name {method_name} is not defined in the"
                f"component manager class. {exc}"
            )

    def create_communication_manager(
        self: PipelineCtrlComponentManager, logger: logging.Logger
    ) -> SshAccess:
        """
        Instantiate the communication manager to access Cheetah.

        :param logger: a logger for this instance to use.
        """
        user_passwd = self.device_properties.CheetahUserPasswd
        pkey = getattr(self.device_properties, "LoginKey", None)

        ip = self.device_properties.NodeIP

        return SshAccess(
            host=ip,
            user=user_passwd[0],
            password=user_passwd[1],
            private_key=pkey,
            logger=logger,
        )

    def start_communicating(self: PipelineCtrlComponentManager) -> None:
        """
        Try to open a connection with the PSS Node.

        Open a channel with this instance of the component manager
        and the software component (cheetah pipeline).
        Submit the _start_communicating method
        Initial communication status is DISABLED.
        """
        self.submit_task(
            self._start_communicating,
            task_callback=self._start_communicating_callback,
        )

    def stop_communicating(self: PipelineCtrlComponentManager) -> None:
        """
        Close connection with the software component.

        Close the ssh channel between this instance of the component manager
        and the controlled software component (cheetah).
        The communication state variable is set to DISCONNECTED and
        the adminMode to OFFLINE.
        """
        if self.communication_state == CommunicationStatus.ESTABLISHED:
            self.communication_manager.disconnect()
        # transition the state to UNKNOWN before disabling the device
        self._update_component_state(power=PowerState.UNKNOWN, fault=None)
        # Note: this method of the component manager invokes the
        # _communication_state_changed method of the device that
        # in turn calls the proper action on the operational state
        # model.
        self._update_communication_state(CommunicationStatus.DISABLED)

    def end_scan(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> Tuple[TaskStatus, str]:
        """Shutdown pipeline."""
        # At the moment there is no graceful shutdown, so use abort
        threading.Thread(
            target=self._endscan, name="end_scan", args=(task_callback,)
        ).start()
        return ResultCode.STARTED, "End scan in progress"

    def abort(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> Tuple[TaskStatus, str]:
        """
        Terminate pipeline.

        If the cheetah pipeline is running, it is stopped.
        If the command is invoked when no observation is in running,
        the device transition to ABORTED.
        """
        threading.Thread(
            target=self._abort, name="abort", args=(task_callback,)
        ).start()
        return TaskStatus.IN_PROGRESS, "Abort in progress"

    def configure_scan(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
        **kwargs,
    ) -> Tuple[TaskStatus, str]:
        """
        Store the configuration file on PSS Node.

        The received configuration file is translated in XML format and stored
        on the PSS node to be used at pipeline startup.

        The original configuration in JSON format is also stored into an
        instance property.

        :param: json_configuration: a string containing the Json configuration
        :param: task_callback: the callback invoked when command ends

        """
        # if self.pipeline_configured:
        #     self.logger.info("Reconfiguring the pipeline")

        try:
            args = json.dumps(kwargs)
            task_status, message = self.submit_task(
                self._config_scan, (args,), task_callback=task_callback
            )
        except Exception as e:
            self.logger.error(e)

        self.logger.info(f"status {task_status} {message}")
        return task_status, message

    def scan(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
        **kwargs,
    ) -> None:
        """
        Start cheetah pipeline on the PSS Node.

        Run Cheetah pipeline This method is meant to run in its own worker
        thread, as it is a long running command.

        :param: scan_id: an integer the identify the scan
        :param: task_callback: the callback invoked when command ends
        """
        cheetah_exe = self.device_properties.CheetahExecutable
        data_source = self.device_properties.CheetahPipelineSource
        pipeline_type = self.device_properties.CheetahPipelineType
        log_level = self.device_properties.CheetahLogLevel

        cmd_string = (
            f"{cheetah_exe} --config config.xml "
            f"-p {pipeline_type} "
            f"-s {data_source} "
            f"--log-level {log_level}"
        )
        self._pipeline_progress = 0
        task_status, message = self.submit_task(
            self._scan,
            (cmd_string,),
            task_callback=task_callback,
        )
        return task_status, message

    def reset_to_idle(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> Tuple[str, str]:
        """
        Submit a task to reset the observing state to IDLE.

        This method is invoked both by the ObsReset and GoToIdle commands.

        :param task_callback: CommandTRacker method invoked on
            command completion.

        :return: a tuple with the task status and a message
        """

        threading.Thread(
            target=self._reset_to_idle, name="obsreset", args=(task_callback,)
        ).start()
        return ResultCode.STARTED, "Obsreset in progress"

        # task_status, message = self.submit_task(
        #     self._reset_to_idle,
        #     task_callback=task_callback,
        # )
        # return task_status, message

    def obsreset(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> Tuple[str, str]:
        """
        Reset the device from a FAULT/ABORT condition.

        :param task_callback: CommandTRacker method invoked on
            command completion.

        :return: a tuple with the task status and a message
        """
        return self.reset_to_idle(task_callback)

    def deconfigure(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
    ) -> Tuple[str, str]:
        """
        Transition the device from READY to IDLE.

        :return: a tuple with the task status and a message
        """
        task_status, message = self.submit_task(
            self._reset_to_idle,
            task_callback=task_callback,
        )
        return task_status, message

    def _handle_log_error(self):
        """Method called by the parser when a marker is detected in the Cheetah
        logs"""

        self._update_component_state(obsfault=True)

    def _start_communicating_callback(
        self: PipelineCtrlComponentManager,
        status: TaskStatus,
        communication_status: CommunicationStatus = None,
        exception: Exception = None,
        # result=None,
        message: str = None,
    ) -> None:
        """
        Task callback passed when submitting the task of _start_communicating.

        :param: status: the status of the task
        :param: communicationStatus: the status of communication to be
            reported to the Pipeline device
        :param: exception: possible exception raised when submitting the task
        :param: message: possible message received when submitting the task
        """
        self.logger.debug(
            f"status: {status} communication_status:{communication_status}"
            f" exception: {exception}"
        )
        if exception:
            self.logger.error(
                f"Start communicating failed with exception: {exception}"
            )

            # Note: in this case the state of the device is DISABLE, and
            # it can transition to FAULT: next call can't be invoked.
            # self._update_component_state(fault=True)
            # If the connection fails when the device is in DISABLE, the
            # communication status has to be set to NOT_ESTABLISHED.
            # In this case the state of the device is set (by the operational
            # state machine) to UNKNOWN (with adminmode = ONLINE/ENGINEERING)

        self.logger.debug(
            f"Task status: {status} communication_status: "
            f"{communication_status} {self.communication_state}"
        )

        if message:
            self.logger.info(message)

        if communication_status is not None:
            self._update_communication_state(communication_status)
            self._update_device_attribute(
                "isCommunicating", self.is_communicating
            )
        elif status == TaskStatus.FAILED and exception:
            self._update_communication_state(
                CommunicationStatus.NOT_ESTABLISHED
            )

    def _start_communicating(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Task submitted by start_communicating.

        On failure the communication status is reported as NOT_ESTABLISHED
        and the adminMode as ONLINE/ENGINEERING.
        The state of the device is set to UNKNOWN.
        Need to re-try the connection.

        :param: task_callback: Task callback passed during the
            submission of the task
        :param: task_abort_event: the abort event (still not implemented)
        """
        communication_status = CommunicationStatus.NOT_ESTABLISHED

        connection_timeout = 5
        # TODO: make connection_timeout configurable from device properties

        connect_exception = None
        start_time = time.time()
        while (time.time() - start_time) < connection_timeout:
            try:
                self.communication_manager.connect()

                communication_status = CommunicationStatus.ESTABLISHED
                self._update_component_state(power=PowerState.ON)
                # check if the cheetah pipeline is already running. This
                # could happen if the device server is restarted during
                # the observation.
                # FIXME: Need to add more checks if the time elapsed is greater
                #        than the observation updates.
                is_running = self.communication_manager.is_running()
                self.logger.info(f"is_running: {is_running}")
                if is_running:
                    # Force the observing state to SCANNING.
                    # Note: the transition from IDLE (default observing state
                    # at device restart) to SCANNING is not allowed.
                    self._update_component_state(force="SCANNING")
                    self._monitor_scan()
                break
            except Exception as e:
                self.logger.error(f"Error in starting communication: {str(e)}")
                # self._update_communication_state(communication_status)
                connect_exception = str(e)
                # wait for a while before retrying the connection
                # FIXME: this time can be configured as well ass the maximum
                # time to wait, currently hardcoded to 5 secs.
                time.sleep(1)
        # invoke the callback to update the command status
        if communication_status == CommunicationStatus.ESTABLISHED:
            task_callback(
                status=TaskStatus.COMPLETED,
                communication_status=communication_status,
            )
        else:
            communication_status = CommunicationStatus.NOT_ESTABLISHED
            task_callback(
                status=TaskStatus.FAILED,
                communication_status=communication_status,
                exception=connect_exception,
            )

    def _monitor_scan(self: PipelineCtrlComponentManager):
        """
        Method invoked at connection when the pipeline process is
        running.
        """
        self._cheetah_pid = self.communication_manager.cheetah_pid
        self._parse_logs(task_callback=None)
        if self.abort_callback:
            # scan ended: aborted
            self.abort_callback(
                status=TaskStatus.COMPLETED, result=ResultCode.OK
            )
            self.abort_callback = None
        else:
            self._update_component_state(scanning=True)

    def _endscan(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ):  # pylint: disable=unused-argument)
        task_callback(
            status=TaskStatus.IN_PROGRESS,
            result=ResultCode.STARTED,
        )
        try:
            if self.communication_manager.is_running():
                self._update_component_state(scanning=False)
                self.communication_manager.kill()
                cheetah_pid = self.communication_manager.cheetah_pid
                if not cheetah_pid:
                    self._cheetah_pid = -1
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Pipeline stopped"),
            )
        except (SSHException, Exception) as exc:
            self.logger.error(f"Endscan error: {exc}")
            # transitions to ObsState.FAULT
            self._update_component_state(obsfault=True)
            task_callback(
                status=TaskStatus.FAILED,
                result=(
                    ResultCode.FAILED,
                    f"Error in stopping pipeline {exc}",
                ),
            )

    def _abort(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:  # pylint: disable=unused-argument)
        """
        Task submitted by the task executor on abort request.

        Check if the pipeline is running and in this case, it sets the
        abort event and invokes the termination of the process.
        """
        try:
            if self.communication_manager.is_running():
                self._update_component_state(scanning=False)
                # the abort event is set only if the pipeline is running
                self.abort_callback = task_callback
                self.communication_manager.kill()
                cheetah_pid = self.communication_manager.cheetah_pid

                if not cheetah_pid:
                    self._cheetah_pid = -1

            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Pipeline aborted"),
            )

        except (SSHException, Exception) as exc:
            self.logger.error(f"Abort error: {exc}")
            # transition to aborted
            task_callback(
                status=TaskStatus.FAILED,
                result=(
                    ResultCode.FAILED,
                    f"Error in aborting pipeline {exc}",
                ),
            )
            # transitions to ObsState.FAULT if the pipeline is still running
            if self.communication_manager.is_running():
                self._update_component_state(obsfault=True)

    def _config_scan(
        self: PipelineCtrlComponentManager,
        json_configuration: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ):  # pylint: disable=unused-argument

        try:
            task_callback(
                status=TaskStatus.IN_PROGRESS,
                result=ResultCode.STARTED,
            )
            # json_configuration = json.dumps(json_configuration)
            self.logger.debug(
                f"JSON configuration script: {json_configuration}"
            )

            xml_config = _from_json_to_xml(json_configuration)
            self.communication_manager.write_config(xml_config)

            # transition from CONFIGURING_IDLE to CONFIGURING_READY
            self._update_component_state(configured=True)
            # transition from CONFIGURING_READY to READY
            self._json_config_script = json_configuration
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Pipeline configured"),
            )
        except (IOError, Exception) as exc:
            self.logger.error(f"config scan error: {exc}")
            # transition from CONFIGURING_IDLE to IDLE or from
            # CONFIGURING_READY to READY
            # The original Obstate is kept in case of failure
            self._update_component_state(obsfault=True)
            task_callback(
                status=TaskStatus.FAILED,
                result=(ResultCode.FAILED, "Pipeline configuration failed"),
            )

    def _scan(
        self: PipelineCtrlComponentManager,
        cmd_string: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:  # pylint: disable=unused-argument):
        """
        Task submitted by the active thread of the pool.

        :param cmd_string: the command line to run cheetah
        :param task_callback: the CommandTracker callback invoked when
            the command complete
        :param task_abort_event: the abort event
        """
        try:
            task_callback(
                status=TaskStatus.IN_PROGRESS, result=ResultCode.STARTED
            )
            self.communication_manager.start(cmd_string)
            self._cheetah_pid = self.communication_manager.cheetah_pid

            self._update_component_state(scanning=True)
            # Note: next call transitions the device from READY -> SCANNING
            # if the start method fails, the device remains in READY
            self._parse_logs(task_callback)
            exit_status = self.communication_manager.get_exit_status()
            if exit_status > 128:
                exit_status = exit_status - 128

            if exit_status not in [signal.SIGTERM, signal.SIGKILL]:
                # exit status > 0 are errors
                # exit status = 0 should not happen since the process is
                # infinite
                raise ValueError(
                    f"Cheetah terminated with exit status {exit_status}"
                )
            self.logger.info(f"Cheetah aborted with exit_status {exit_status}")

            if self.abort_callback:
                # scan ended: aborted
                task_callback(status=TaskStatus.ABORTED, result=ResultCode.OK)
                self.abort_callback = None
                # Note: the scanning flag set above, is reset when the
                # abort command ends.
            else:
                # Scan ended: endscan or elapsed integration
                # Note: the scanning flag set above, is reset when the
                # endscan command ends
                task_callback(
                    status=TaskStatus.COMPLETED,
                    result=(ResultCode.OK, "Pipeline scan is completed"),
                )
        except (SSHException, Exception) as exc:
            self.logger.error(f"Scan failed with exception: {exc}")
            # it maintains the original obsState= READY in case of failure
            # because the scanning=True has not been set!!
            task_callback(
                status=TaskStatus.FAILED, result=(ResultCode.FAILED, str(exc))
            )

    def _parse_logs(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """
        Parse and forward logs from the cheetah application.

        :param task_callback: CommandTracker method invoked on
            command completion.
        """
        for line in self.communication_manager.get_logs():
            self._log_line = line
            self.log_queue.put(line)  # Add log line to the parser queue
            self._update_device_attribute("cheetahLogLine", line)
            if self._pipeline_progress < 99:
                self._pipeline_progress += 1
                self._update_device_attribute(
                    "pipelineProgress", self._pipeline_progress
                )
                if task_callback:
                    task_callback(progress=self._pipeline_progress)
        self.logger.info("Parsing log ended")

    def _reset_to_idle(
        self: PipelineCtrlComponentManager,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ):
        """
        Task submitted by the reset_to_idle or deconfigure method.

        :param task_callback: CommandTracker method invoked on
            command completion.
        :param task_abort_event: the shared event to signal an abort request.
        """
        command_name = "obsreset"  # set default
        command_id = task_callback.args[0]
        pos = command_id.rfind("_") + 1
        if pos > -1:
            command_name = command_id[pos:]
            self.logger.info(f"reset_to_idle: command is {command_name}")
        try:
            task_callback(
                status=TaskStatus.IN_PROGRESS,
                result=ResultCode.STARTED,
            )
            # de-configure
            # reset the configured flag to enable state update on the device
            # next state change

            if command_name.lower() == "obsreset":
                if self.communication_manager.is_running():
                    self.logger.error("Pipeline is still running!!!")
                    # try to kill all the cheetah instances
                    self.communication_manager.kill()
                    cheetah_pid = self.communication_manager.cheetah_pid
                    if not cheetah_pid:
                        self._cheetah_pid = -1

                self._update_component_state(obsfault=False)
            self.communication_manager.delete_config()
            # transition device to IDLE
            self._update_component_state(configured=False)
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Pipeline reset to idle"),
            )

        except Exception as e:
            self.logger.error(f"Error in reset to idle: {e}")
            self._update_component_state(obsfault=True, configured=False)
            task_callback(
                status=TaskStatus.FAILED,
                result=(ResultCode.FAILED, f"error in resetting to idle {e}"),
            )

        self._json_config_script = ""
