# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""PSS CTRL Tango module."""
from __future__ import annotations  # allow forward references in type hints

from typing import Any, Tuple

from ska_control_model import (
    CommunicationStatus,
    HealthState,
    ObsState,
    ResultCode,
)
from ska_csp_lmc_base import CspSubElementObsDevice
from tango import AttrWriteType
from tango.server import attribute, device_property, run

from ska_pss_lmc.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_pss_lmc.model.health_state_model import HealthStateModel
from ska_pss_lmc.pipeline.pipeline_component_manager import (
    PipelineCtrlComponentManager,
)

# pylint: disable=unused-variable
# pylint: disable=redefined-outer-name
# pylint: disable=invalid-name
# pylint: disable=no-else-return
# pylint: disable=attribute-defined-outside-init
# pylint: disable=fixme
# pylint: disable=logging-fstring-interpolation
# pylint: disable=arguments-differ
# pylint: disable=protected-access
# pylint: disable=too-few-public-methods


class PipelineCtrlDevice(CspSubElementObsDevice):
    """
    PSS Pipeline Control Tango device.

        **Device Properties:**

            NodeIP
                - The IP address of the PSS node where the cheetah pipeline
                  will be running
                - Type:'DevString

            PipelineName
                - The pipeline name
                - Type:'DevString'

            CheetahOutputFile
                - The filename where cheetah stdout adn stderr will be stored
                - Type:'DevString'

            CheetahConfigFile
                - The filename where cheetah input configuration data will
                  be stored
                - Type:'DevString'

            CheetahExecutable
                - A string containing the command script to execute Cheetah
                - Type:'DevString'

            CheetahUserPasswd
                - A Tuple containing the username and password for the
                  remote connection to Cheetah
                - Type: ('DevString', )

    """

    def _init_state_model(self: PipelineCtrlDevice) -> None:
        """
        Override base method.

        Configure some device attributes to push events from the code.
        """
        super()._init_state_model()
        self._health_model = HealthStateModel(
            HealthState.UNKNOWN, self._update_health_state, self.logger
        )
        self.set_change_event("isCommunicating", True, False)
        self.set_change_event("pipelineProgress", True, False)
        self.set_change_event("cheetahLogLine", True, False)

    def create_component_manager(
        self: PipelineCtrlDevice,
    ) -> PipelineCtrlComponentManager:
        """
        Create and return the Pss Pipeline Control Component Manager.
        """
        cm_configuration = ComponentManagerConfiguration(
            self.get_name(), self.logger
        )
        cm_configuration.add_attributes()
        return PipelineCtrlComponentManager(
            2,
            cm_configuration,
            self._communication_state_changed,
            self._component_state_changed,
            self.update_attribute,
            self.logger,
        )

    # pylint: disable=too-many-arguments
    def _component_state_changed(
        self: PipelineCtrlDevice,
        fault: bool = False,
        power: bool = None,
        configured: bool = None,
        scanning: bool = None,
        obsfault: bool = None,
        **kwargs,
    ) -> None:
        """
        Update the state of the controlled component.

        The device reports only a sub-set of the possible states of the
        controlled component (cheetah pipeline). These are:

        - DISABLE: the device is not trying to connect to the component
          (default state at device startup)
        - UNKNOWN: the devices trying to connect to the  PSS node where
          the software component will be running but the connection is
          not established
        - ON: the device is connected with the PSS node; the software
          component might or not be in running.
        - FAULT: the controlled component is experiencing a fault condition
        """
        super()._component_state_changed(
            fault=fault, power=power, configured=configured, scanning=scanning
        )
        if obsfault:
            self.obs_state_model.perform_action("component_obsfault")
        if "force" in kwargs:
            self._force_transition_to_obs_state(kwargs["force"])

        self._health_model.component_fault(fault)

    def _force_transition_to_obs_state(self, obs_state: ObsState) -> None:
        """
        Force the observing state machine to a state.

        Use the state machine auto transitions to force the
        transition to a not allowed state.

        :param obs_state: the desired observing state
        """
        current_obs_state = self.obs_state_model.obs_state
        self.logger.info(
            f"Forcing the obsState from {ObsState(current_obs_state).name}"
            f"to {obs_state}"
        )
        if obs_state in ["SCANNING", "READY"]:
            obs_state.upper()
            self.obs_state_model.perform_action(f"to_{obs_state}")

    def _communication_state_changed(
        self: PipelineCtrlDevice, communication_state: CommunicationStatus
    ) -> None:
        """
        Update the PipelineCtrlDevice communication status.

        :param communication_state: the status of communication with the
            controlled component (cheetah pipeline)
        """
        self.logger.info(
            f"update communication status: current state {self.get_state()}"
            f" new comm status {communication_state}"
        )
        super()._communication_state_changed(communication_state)

        # CommunicationStatus.DISABLED: the component manager is not trying to
        #                               connect to the cheetah pipeline
        #                               (default state at device startup)
        # CommunicationStatus.NOT_ESTABLISHED: there are problems to connect
        #                               to the cheetah pipeline.
        # CommunicationStatus.ESTABLISHED: communication with cheetah pipeline
        #                               is active.

        disabled = bool(communication_state == CommunicationStatus.ESTABLISHED)
        self._health_model.is_disabled(not disabled)

    def update_attribute(
        self: PipelineCtrlDevice, attribute_name: str, attribute_value: Any
    ) -> None:
        """
        General method invoked to push an event on a device attribute.


        :param attribute_name: the TANGO attribute name
        :param attribute_value: the attribute value
        """
        # TODO: add check on attribute configuration for events
        if attribute_name == "adminmode":
            self.write_adminMode(attribute_value)
        elif attribute_name == "state":
            self._update_state(attribute_value)
            return
        self.push_change_event(attribute_name, attribute_value)
        return

    class InitCommand(CspSubElementObsDevice.InitCommand):
        """Class that implements device initialisation for the device."""

        def do(  # type: ignore[override]
            self: CspSubElementObsDevice.InitCommand,
        ) -> Tuple[ResultCode, str]:
            """
            Initialise the attributes and properties of the  device.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            """
            self._device._health_state = HealthState.UNKNOWN
            result_code, _ = super().do()
            self._device._health_model.is_disabled(True)
            return (result_code, "Pipeline Controller Device initialized")

    # -----------------
    # Device Properties
    # -----------------

    NodeIP = device_property(dtype="DevString", default_value="127.0.0.1")

    PipelineName = device_property(
        dtype="DevString", default_value="pipeline-00-00-01"
    )

    CheetahOutputFile = device_property(
        dtype="DevString", default_value="/tmp/cheetah.out"
    )

    CheetahConfigFile = device_property(
        dtype="DevString", default_value="/tmp/cheetah_config.xmi"
    )

    CheetahExecutable = device_property(
        dtype="DevString", default_value="python3 simulcheetah.py"
    )

    CheetahUserPasswd = device_property(
        dtype=("DevString",), default_value=["cheetah", "cheetah"]
    )

    LoginKey = device_property(dtype="DevString", default_value=None)

    # ----------
    # Attributes
    # ----------

    lastScanConfiguration = attribute(
        dtype="DevString",
        label="lastScanConfiguration",
        doc="The last valid scan configuration.",
    )

    def read_lastScanConfiguration(self):
        """Return the last programmed configuration."""
        return self.component_manager.scan_configuration

    @attribute(
        label="pipelineProgress",
        dtype="DevUShort",
        max_value=100,
        min_value=0,
        access=AttrWriteType.READ,
        doc="The cheetah pipeline progress percentage",
    )
    def pipelineProgress(self: PipelineCtrlDevice) -> int:
        """
        Return the cheetah pipeline progress.
        """
        return self.component_manager.pipeline_progress

    @attribute(
        label="Cheetah version",
        dtype="DevString",
        access=AttrWriteType.READ,
        doc="The cheetah pipeline version",
    )
    def cheetahVersion(self: PipelineCtrlDevice) -> int:
        """
        Return the cheetah version.
        """
        return self.component_manager.cheetah_version

    @attribute(
        label="cheetahPid",
        dtype="DevLong",
        doc="The pid of the cheetah process associated to this device.",
    )
    def cheetahPid(self: PipelineCtrlDevice) -> int:
        """
        Return the PID of the cheetah pipeline process.

        The attribute is stored into the TANGO DB as memorized attribute"
        so that on device restart it's possible to recover the connection "
        to the cheetah pipeline process
        """
        if self.component_manager:
            return self.component_manager.cheetah_pid
        return -1

    @attribute(
        label="cheetahLogLine",
        dtype="DevString",
        access=AttrWriteType.READ,
        doc="Cheetah pipeline log line",
    )
    def cheetahLogLine(self: PipelineCtrlDevice) -> str:
        """
        Return the cheetah log, updated line by line
        """
        return self.component_manager.log_line

    @attribute(
        label="isCommunicating",
        dtype="DevBoolean",
        access=AttrWriteType.READ,
        doc="Whether the device is communicating with the component under"
        "control",
    )
    def isCommunicating(self: PipelineCtrlDevice) -> bool:
        """
        Return the device communicating status.

        Return a boolean flag indicating whether the TANGO device is
        communicating with the controlled component.
        """
        return self.component_manager.is_communicating

    @attribute(
        label="nodeIP",
        dtype="DevString",
        access=AttrWriteType.READ,
        doc="The PSS node IP address",
    )
    def nodeIP(self: PipelineCtrlDevice) -> str:
        """
        Return the pss node IP.
        """
        return self.NodeIP

    @attribute(
        label="pipelineName",
        dtype="DevString",
        access=AttrWriteType.READ,
        doc="The cheetah pipeline name",
    )
    def pipelineName(self: PipelineCtrlDevice) -> str:
        """
        Return the cheetah pipeline name.
        """
        return self.component_manager.device_properties.PipelineName

    @attribute(
        label="subarrayId",
        dtype="uint16",
        access=AttrWriteType.READ_WRITE,
        doc="The subarray membership",
    )
    def subarrayId(self: PipelineCtrlDevice) -> int:
        """
        Return the subarray membership.
        """
        return self.component_manager.subarray_id

    @subarrayId.write
    def subarrayId(self: PipelineCtrlDevice, value: int):
        """
        Set the subarray membership of the device

        :param value: the subarray id
        """
        self.component_manager.subarray_id = value


def main(args=None, **kwargs):
    """Run PipelineCtrlDevice."""
    return run((PipelineCtrlDevice,), args=args, **kwargs)


if __name__ == "__main__":
    main()
