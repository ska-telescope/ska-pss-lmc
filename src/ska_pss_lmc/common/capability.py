# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""This module implements the functionality for the PSS Capability."""

from __future__ import annotations  # allow forward references in type hints

import copy
import json
import logging
import sys
import threading
from typing import Any, Callable, Dict, Optional

from ska_control_model import AdminMode, HealthState, ObsState
from tango import DevState, EventData

# Create module logger that logs all levels
module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

# Create handler to output to standard out
handler = logging.StreamHandler(sys.stdout)
handler.setLevel(logging.DEBUG)
formatter = logging.Formatter(
    "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
handler.setFormatter(formatter)
module_logger.addHandler(handler)


# pylint: disable=too-many-arguments
# pylint: disable=unused-argument
# pylint: disable=broad-except
class PssCapability:
    """
    Class maintaining the information of the PSS Pipeline Controller
    devices.
    """

    def __init__(
        self: PssCapability,
        pipeline_fqdns,
        update_device_attribute_cbk: Callable,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """
        The Component Manager for the PSS controller software component.

        :param pipeline_fqdns: the list with the pipelines' FQDNs.
        :param update_device_attriute_cbk: the PSS device callback invoked
            toupdate the device attributes.
        :param logger: a logger for this instance to use.
        """

        self.logger = logger or module_logger
        self.pipeline_fqdns = pipeline_fqdns
        self._update_device_attribute = update_device_attribute_cbk
        self._json_dict = {}
        self._json_dict["pipelines"] = []

        self.pss_pipelines_state = []
        self.reported_attributes = {}
        self._data_lock = threading.Lock()
        self.device_data = {
            "dev_id": 0,
            "fqdn": "",
            "state": str(DevState.UNKNOWN),
            "healthstate": HealthState.UNKNOWN.name,
            "adminmode": AdminMode.OFFLINE.name,
            "obsstate": ObsState.IDLE.name,
            "iscommunicating": False,
        }
        # map the tango attributes the key dictionary
        # this is necessary since the events come as integers,
        # not enum
        self.attrname_to_enum_class_map = {
            "healthstate": HealthState,
            "adminmode": AdminMode,
            "obsstate": ObsState,
        }

        self._initialize_json_dict()

    @property
    def json_dict(self: PssCapability) -> Dict:
        """
        Return the internal dictionary.
        """
        return self._json_dict

    @property
    def json_str(self: PssCapability) -> str:
        """
        Return the internal dictionary.
        """
        json_str = ""
        try:
            json_str = json.dumps(self._json_dict)
        except TypeError as err:
            self.logger.error(
                f"Error in serializaing the interna; dictionary: {err}"
            )
        return json_str

    @property
    def pipeline_attributes(self: PssCapability):
        """
        Return the list of attributes to monitor in a
        pipeline device
        """
        attribute_list = list(self.device_data.keys())
        # remove th eunused entry
        attribute_list.remove("dev_id")
        attribute_list.remove("fqdn")
        return attribute_list

    #
    # Public methods
    #

    def get_dict_entry(self: PssCapability, dict_key) -> Any:
        """
        Return the information stored in the dictionary for the
        given key.
        The access to the internal dictionary is regulated by a lock.

        :param dict_key: the dictionary key string whose value is requested.

        :return: dictionary value related to the input key
        """

        with self._data_lock:
            return self._get_dict_entry(dict_key)

    def update_dict_entry(
        self, attr_name, entry_value, device_pos: Any = None
    ) -> None:
        """Update the content of the internal dictionary.
        The access to the internal dictionary is controlled by a lock

        :param entry_key: the key name of the entry to update into the
        dictionary
        :param entry_value: the value to store
        :param device_pos: if not None, the index of the device record into the
        dictionary list
        """

        with self._data_lock:
            self._update_dict_entry_no_lock(attr_name, entry_value, device_pos)

    def update_pipeline_info(self: PssCapability, event: EventData):
        """
        The callback is registered for the events subscribed to the attributes
        of PSS Pipeline controller devices. This method handles change events
        for a device pipeline's attribute, updates the corresponding entry in
        the internal dictionary, and publishes the attribute's new value on
        the device.

        :param event: the TANGO EventData
        """
        if event.err:
            self.logger.error("Received error on event")
            return
        device_fqdn = event.device.dev_name()
        attr_name = event.attr_value.name.lower()
        attr_value = event.attr_value.value
        with self._data_lock:
            # retrieve the device record from the internal dictionary
            idx = self.pipeline_fqdns.index(device_fqdn)
            item = self._json_dict["pipelines"][idx]
            # update array element according to fqdn
            if device_fqdn == item["fqdn"]:
                if attr_name == "state":
                    attr_value = str(attr_value)
                self._update_dict_and_attribute(attr_name, attr_value, idx)
            else:
                self.logger.warning(
                    "Error in updating %s. Entry mismatch received: %s"
                    " extracted: %s",
                    attr_name,
                    device_fqdn,
                    item["fqdn"],
                )

        #

    # Protected methods to access/update the internal dictionary
    #
    def _initialize_json_dict(self: PssCapability):
        """
        Initialize the dictionary with the pipelines global status
        """
        self._json_dict["pipelines_deployed"] = len(self.pipeline_fqdns)
        for device_fqdn in self.pipeline_fqdns:
            device_data = copy.deepcopy(self.device_data)
            device_data["fqdn"] = device_fqdn
            self._json_dict["pipelines"].append(device_data)
        if self._update_device_attribute:
            self._update_device_attribute("pipelinesJson", self.json_str)

    def _get_dict_entry(self: PssCapability, dict_key) -> Any:
        """
        Return the information stored in the dictionary for the
        given key.
        The access to the internal dictionary is regulated by a lock.

        :param dict_key: the dictionary key whose value is requested.

        :return: dictionary value related to the input key on success,
            otherwise an empty list.
        """
        list_of_values = []
        if self._json_dict:
            for item in self._json_dict["pipelines"]:
                try:
                    list_of_values.append(item[dict_key])
                except KeyError as e:
                    self.logger.warning(f"Invalid device dictionary key: {e}")
        return list_of_values

    def _update_dict_entry_no_lock(
        self, attr_name, entry_value, device_pos: Any = None
    ) -> None:
        """
        Update the content of the internal dictionary without locking.

        :param entry_key: the key name of the entry to update into the
            dictionary
        :param entry_value: the value to store
        :param device_pos: if not None, the index of the device record into
            the dictionary list
        """
        try:
            if device_pos is not None:
                # for enum report the label
                if attr_name in self.attrname_to_enum_class_map:
                    entry_value = self.attrname_to_enum_class_map[attr_name](
                        entry_value
                    ).name
                self._json_dict["pipelines"][device_pos][
                    attr_name
                ] = entry_value
            else:
                self._json_dict["pipelines"][attr_name] = entry_value
        except KeyError as err:
            self.logger.warning(f"Failed to update device entry: {err}")

    def _update_dict_and_attribute(self, attr_name, attr_value, idx):
        """
        Update the internal dictionary to reflect the change in the
        attribute and publish the updated value of the attribute.

        :param attr_name: the name of the attribute to update in the
            internal dictionary and on the device.
        :param attr_value: the attribute's value.
        :param idx: the record index inside the dictionary.
        """

        self._update_dict_entry_no_lock(attr_name, attr_value, idx)
        self._update_attributes_on_device(attr_name)

    def _update_attributes_on_device(self, attr_name) -> None:
        """Method to update the correspondant attribute on the device"""
        if self._update_device_attribute:
            self._update_device_attribute("pipelinesJson", self.json_str)
            self._update_device_attribute(
                f"pipelines{attr_name}", self._get_dict_entry(attr_name)
            )
