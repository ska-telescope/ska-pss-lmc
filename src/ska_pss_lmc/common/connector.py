# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""This module implements the connection functionality.
 """
from __future__ import annotations

import threading
from typing import Callable, List

from ska_control_model import CommunicationStatus
from ska_tango_testing import context
from tango import (
    CommunicationFailed,
    ConnectionFailed,
    DevFailed,
    DeviceProxy,
    EventSystemFailed,
    EventType,
)


class Connector:
    """Handle the connection with a subordinate device"""

    def __init__(
        self: Connector,
        logger=None,
    ):
        self.logger = logger
        self._proxies = {}
        self._lock = threading.Lock()
        """
        Initialize the Connector class.
        :param logger: the logger for this class.
        """

    @property
    def proxies(self: Connector) -> DeviceProxy:
        """Return the list of proxies"""
        return self._proxies

    #
    # Public methods
    #
    def _set_device(self: Connector, device_fqdn: str, dev_proxy: DeviceProxy):
        with self._lock:
            self._proxies[device_fqdn] = dev_proxy

    def get_device(self: Connector, device_fqdn) -> DeviceProxy:
        with self._lock:
            return self._proxies[device_fqdn]

    def connect(self: Connector, device_fqdn: str) -> CommunicationStatus:
        """
        Initiates a connection to the specified device.

        :param device_fqdn: The fully qualified domain name (FQDN) of the
            device.

        :return: The communication status.
        """
        self.logger.debug(f"Connecting to device {device_fqdn}")
        comm_status = CommunicationStatus.NOT_ESTABLISHED
        try:
            if device_fqdn in self.proxies:
                self.logger.info(
                    f"Proxy to {device_fqdn} already present {self.proxies}"
                )
                return CommunicationStatus.ESTABLISHED

            dp = context.DeviceProxy(device_name=device_fqdn)
            self._set_device(device_fqdn, dp)
            comm_status = CommunicationStatus.ESTABLISHED
        except (ConnectionFailed, CommunicationFailed, DevFailed) as err:
            self.logger.error(
                f"Failure in connecting to device {device_fqdn}: {str(err)}"
            )
        self.logger.debug(
            f"Connection status for {device_fqdn} is "
            f"{CommunicationStatus(comm_status).name}"
        )
        return comm_status

    def subscribe(
        self: Connector, proxy, attribute_list: List[str], callback: Callable
    ) -> dict:
        """
        Registers a subscription to a device for the specified attributes,
        triggering the given callback on change events.

        :param proxy: The device proxy.
        :param attribute_list: A list of attributes to subscribe to for
            change event notifications.
        :param callback: The function to be called when a change event occurs.

        :return: a dictionary with key=attribute_name and item=event_id
        """
        event_dict = {}
        for attribute in attribute_list:
            attribute = attribute.lower()
            try:
                event_id = proxy.subscribe_event(
                    attribute,
                    EventType.CHANGE_EVENT,
                    callback,
                )
                event_dict[attribute] = event_id

            except EventSystemFailed as err:
                self.logger.warning(
                    f"Failure in subscribing attribute {attribute} "
                    f"on device {proxy.dev_name()}: {str(err)}"
                )
        return event_dict

    def unsubscribe_attribute(
        self: Connector, event_id: int, proxy: DeviceProxy
    ):
        """
        Unsubscribes a client from receiving the event specified by
        event_id.

        :param event_id: int. Event id of the subscription
        :param proxy: The device proxy.

        :return: None
        """

        try:
            proxy.unsubscribe_event(event_id)
            self.logger.debug(
                f"unsubscribed event" f" {event_id} on {proxy.dev_name()}"
            )
        except EventSystemFailed as err:
            self.logger.warning(
                f"Failure in unsubscribing event {event_id} "
                f"on device {proxy.dev_name()}: {str(err)}"
            )

    # def run(self: Connector,
    #         command:str,
    #         input_json: str
    #         ):

    #     for pipeline in self.pipelines:
    #         command_input = input_json[pipeline]
    #         invoke_lrc(partial(self.command_callback, d
    #               evices_num=len(self.pipelines), device_name=pipeline),
    #                    self.proxies[pipeline], command, command_input,
    #                   self.logger)

    # def command_callback(
    #     self: Connector,
    #     status: TaskStatus | None = None,
    #     progress: int | None = None,
    #     result: JSONData | None = None,
    #     error: tuple[DevError] | None = None,
    #     **kwargs):

    #     device_name = kwargs["device_name"]
    #     devices_num = kwargs['devices_num']
    #     with _lock:
    #       if status:
    #           index = self.pipelines.index(device_name)
    #           self.list_of_command_status[index] = status

    #
    #       if result:
    #           self.list_of_command_result[index] = result
    #       if error:
    #           raise error
    #  if devices_num == len((list(self.list_of_command_status.keys))):
    #     self.calculate_command_status()
    #     self.calculate_command_result()

    # def calculate_command_status(self:Connector):

    #     if TaskStatus.IN_PROGRESS in self.list_of_command_status:
    #         # 1 in progress -> in progress
    #         command_status = TaskStatus.IN_PROGRESS
    #     elif all(status == TaskStatus.COMPLETED for status in attr_value):
    #         # tutti completed -> completed
    #         command_status = TaskStatus.COMPLETED
    #     elif TaskStatus.FAILED in self.list_of_command_status:
    #         # 1 failed -> failed
    #         command_status = TaskStatus.FAILED

    #     self.update_device_attribute("LongRunningCommandStatus", status)

    # def calculate_command_status(self):
    #  pass
