# -*- coding: utf-8 -*-
#
# Code derived from the HealthModel of the MCCS project.
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.
"""This module implements infrastructure for health management in CSP.LMC sub-
system."""

from __future__ import annotations  # allow forward references in type hints

import logging
from typing import Callable, Optional

from ska_tango_base.control_model import HealthState

module_logger = logging.getLogger(__name__)

__all__ = ["HealthStateModel"]


class HealthStateModel:
    """A simple health model the supports.

    * HealthState.OK -- when the component is fully operative.

    * HealthState.UNKNOWN -- when communication with the component is not
        established.

    * HealthState.FAILED -- when the component has faulted
    """

    def __init__(
        self: HealthStateModel,
        init_state: HealthState,
        health_changed_callback: Callable[[HealthState], None],
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """Initialise a new instance.

        :param init_state: The health state of the component under control
            at initialization.
        :param health_changed_callback: callback to be called whenever
            there is a change to this this health model's evaluated
            health state.
        :param logger: a logger for this instance
        """
        self.logger = logger or module_logger
        self._disabled = False
        self._faulty = False
        self._health_state = init_state
        self._health_changed_callback = health_changed_callback
        self._health_changed_callback(self._health_state)

    @property
    def faulty(self: HealthStateModel):
        """Return whether the componend is experiencing a faulty condition or
        not.

        :return: whether the component is in fault.
        """
        return self._faulty

    @property
    def disabled(self):
        """Return whether the component is disabled or not.

        :return: whether the component is disabled
        """
        return self._disabled

    @property
    def health_state(self: HealthStateModel) -> HealthState:
        """Return the health state of the component under control.

        :return: the health state.
        """
        return self._health_state

    def update_health(self: HealthStateModel) -> None:
        """Update health state.

        This method calls the :py:meth:``evaluate_health`` method to figure
        out what the new health state should be, and then updates the
        ``health_state`` attribute, calling the callback if required.
        """
        health_state = self.evaluate_health()
        if self._health_state != health_state:
            self._health_state = health_state
            self._health_changed_callback(health_state)

    def evaluate_health(self: HealthStateModel) -> HealthState:
        """Re-evaluate the health state.

        This method contains the logic for evaluating the health.

        This method should be extended by subclasses in order to
        define how health is evaluated by their particular device.

        :return: the new health state.
        """
        if self._faulty:
            return HealthState.FAILED
        if self._disabled:
            return HealthState.UNKNOWN
        return HealthState.OK

    def component_fault(self: HealthStateModel, fault: bool) -> None:
        """Handle a component experiencing or recovering from a fault.

        This is a callback hook that is called when the component goes
        into or out of FAULT state.

        :param fault: whether the component has faulted or not
        """
        self._faulty = fault
        self.update_health()

    def is_disabled(self: HealthStateModel, disabled: bool) -> None:
        """Handle change in communication with the component.

        :param disabled: whether communications with the component
            is established.
        """
        self._disabled = disabled
        self.update_health()
