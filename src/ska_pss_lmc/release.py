# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC Classes
#
#
#
"""Release information for ska-pss-lmc Python Package."""

# pylint: disable=redefined-builtin
# pylint: disable=invalid-name

name = """ska-pss-lmc"""
version = "0.2.0-rc.1"
version_info = version.split(".")
description = """A set of classes of SKA PSS LMC"""
author = "SKA Inaf"
author_email = "gianluca.marotta at inaf.it"
license = """BSD-3-Clause"""
url = """https://www.skatelescope.org/"""
copyright = """INAF"""
