# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""This module implements a functionality for the PSS
 Subarray TANGO Device.
 """
from __future__ import annotations  # allow forward references in type hints

from typing import Any, List

import tango
from ska_control_model import AdminMode, HealthState, ObsMode, ObsState
from ska_csp_lmc_base import CspSubElementSubarray
from ska_tango_base.faults import StateModelError
from tango import DevState
from tango.server import AttrWriteType, attribute, device_property, run

from ska_pss_lmc.manager.manager_configuration import (
    ComponentManagerConfiguration,
)
from ska_pss_lmc.subarray.subarray_component_manager import (
    PssSubarrayComponentManager,
)

# pylint: disable=logging-fstring-interpolation
# pylint: disable=no-value-for-parameter
# pylint: disable=protected-access
# pylint: disable=attribute-defined-outside-init
# pylint: disable=too-many-public-methods
# pylint: disable=invalid-name


class PssSubarray(CspSubElementSubarray):
    """PSS Subarray TANGO Device"""

    NUM_PSS_PIPELINES = 4  # Number of PSS Pipelines

    def init_device(self: PssSubarray) -> None:
        """Override the Base Classes *init_device* method to change the
        asynchronous callback sub-model from pull to push sub-model."""
        # to use the push model in command_inout_asynch (the one with the
        # callback parameter), change the global TANGO model to PUSH_CALLBACK.
        apiutil = tango.ApiUtil.instance()
        apiutil.set_asynch_cb_sub_model(tango.cb_sub_model.PUSH_CALLBACK)
        super().init_device()
        self._obs_mode = ObsMode.IDLE

    def _init_state_model(self: PssSubarray) -> None:
        """"""
        super()._init_state_model()
        self.set_change_event("isCommunicating", True, False)
        list_of_attributes = [
            "pipelinesstate",
            "pipelinesobsstate",
            "pipelineshealthstate",
            "pipelinesadminmode",
            "pipelinesiscommunicating",
            "commandTimeout",
            "scanID",
            "obsState",
            "healthState",
        ]
        for attr in list_of_attributes:
            self.set_change_event(attr, True, False)
            self.set_archive_event(attr, True)

    def create_component_manager(
        self: PssSubarray,
    ) -> PssSubarrayComponentManager:
        """
        Create and return the PssSubarray Component Manager.
        """
        cm_configuration = ComponentManagerConfiguration(
            self.get_name(), self.logger
        )
        cm_configuration.add_attributes()
        return PssSubarrayComponentManager(
            1,
            cm_configuration,
            self._communication_state_changed,
            self._component_state_changed,
            self.update_attribute,
            self.logger,
        )

    ##################
    # Class  methods
    ##################

    def update_subarray_state(self: PssSubarray, value: DevState) -> None:
        """Update state attribute"""
        self.logger.info(f"Update state: from {self.get_state()} to {value}")
        super()._update_state(value)

    def update_subarray_health_state(
        self: PssSubarray, value: HealthState
    ) -> None:
        """Update healthState attribute"""
        self.logger.info(
            f"Update healthState from "
            f"{HealthState(self._health_state).name} to "
            f"{HealthState(value).name}"
        )
        super()._update_health_state(value)

    def update_subarray_obs_state(self: PssSubarray, value: ObsState) -> None:
        """Update obsState attribute"""
        # pylint: disable=access-member-before-definition
        self.logger.info(
            f"Update obsState device from "
            f"{ObsState(self._obs_state).name} to {ObsState(value).name}"
        )
        self._obs_state = ObsState(value)
        self.push_change_event("obsState", self._obs_state)
        super()._update_obs_state(ObsState(value))

    def update_attribute(self: PssSubarray, attr_name: str, attr_value: Any):
        """Update component attributes and handle state changes."""
        self.logger.debug(
            f"Update device attribute {attr_name} to {attr_value}"
        )
        # Handle special cases
        if attr_name == "adminmode":
            self.write_adminMode(attr_value)
        elif attr_name in {"healthstate", "healthState"}:
            self.update_subarray_health_state(attr_value)
        elif attr_name == "obsState":
            self.update_subarray_obs_state(attr_value)
        elif attr_name == "state":
            self.update_subarray_state(attr_value)
        else:
            # Handle events
            self.push_change_event(attr_name, attr_value)
            attr = getattr(self, attr_name)
            if attr.is_archive_event():
                self.push_archive_event(attr_name, attr_value)

    # -----------------
    # Device Properties
    # -----------------

    PssController = device_property(
        dtype="DevString",
    )

    ConnectionTimeout = device_property(dtype="DevUShort", default_value=60)

    PingConnectionTime = device_property(dtype="DevUShort", default_value=5)

    DefaultCommandTimeout = device_property(
        dtype="DevUShort", default_value=10
    )

    PipelineFqdns = device_property(
        dtype="DevVarStringArray",
    )

    # ----------
    # Attributes
    # ----------

    adminMode = attribute(
        dtype=AdminMode,
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
        label="adminMode",
        doc=(
            "The admin mode reported for this device. It may interpret the"
            "current device condition and condition of all managed devices"
            "to set this. Most possibly an aggregate attribute."
        ),
    )

    scanID = attribute(
        dtype="DevULong64",
        label="scanID",
        access=AttrWriteType.READ_WRITE,
    )

    validScanConfiguration = attribute(
        dtype="DevString",
        label="validScanConfiguration",
        doc="Store the last valid scan configuration.",
    )

    commandTimeout = attribute(
        dtype="DevUShort",
        access=AttrWriteType.READ_WRITE,
        max_value=100,
        min_value=0,
        label="commandTimeout",
        doc=("Report the set command timeout"),
    )

    isCommunicating = attribute(
        dtype="DevBoolean",
        access=AttrWriteType.READ,
        doc=(
            "Whether the device is communicating with the component under"
            " control"
        ),
    )

    @attribute(
        dtype="DevString",
        label="configurationID",
        doc="The configuration identifier (string)",
    )
    def configurationID(self: PssSubarray) -> str:
        """Report the configuration ID string.

        return: a string with the ID of the received configuration
            when correctly validated, otherwise an empty string.
        """
        return self.component_manager.config_id

    obsMode = attribute(
        dtype=(ObsMode,),
        max_dim_x=16,
        label="obsmode",
        doc="Active observation modes",
    )

    @attribute(
        label="pipelinesstate",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=NUM_PSS_PIPELINES,
        doc="The pipelines' state",
    )
    def pipelinesstate(self: PssSubarray) -> List[str]:
        """Return the pipelines' state"""
        return self.component_manager.pipelines_state

    @attribute(
        label="pipelinesobsstate",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=NUM_PSS_PIPELINES,
        doc="The pipelines' obs_state",
    )
    def pipelinesobsstate(self: PssSubarray) -> List[str]:
        """Return the pipelines' obs_state"""
        return self.component_manager.pipelines_obsstate

    @attribute(
        label="pipelineshealthstate",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=NUM_PSS_PIPELINES,
        doc="The pipelines' health_state",
    )
    def pipelineshealthstate(self: PssSubarray) -> List[str]:
        """Return the pipelines' health_state"""
        return self.component_manager.pipelines_healthstate

    @attribute(
        label="pipelinesadminmode",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=NUM_PSS_PIPELINES,
        doc="The pipelines' admin_mode",
    )
    def pipelinesadminmode(self: PssSubarray) -> List[str]:
        """Return the pipelines' admin_mode"""
        return self.component_manager.pipelines_adminmode

    @attribute(
        label="pipelinesiscommunicating",
        dtype=("DevBoolean",),
        access=AttrWriteType.READ,
        max_dim_x=NUM_PSS_PIPELINES,
        doc="The pipelines' communication status",
    )
    def pipelinesiscommunicating(self: PssSubarray) -> List[str]:
        """Return the pipelines' communication status"""
        return self.component_manager.pipelines_connected

    @attribute(
        label="pipelinesJson",
        dtype=str,
        access=AttrWriteType.READ,
        doc="JSON string with all the pipelines' info",
    )
    def pipelinesJson(self: PssSubarray) -> str:
        """Return a JSON string with all pipelines's info"""
        return self.component_manager.pipelines_json

    @attribute(
        label="pipelinesFqdns",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=NUM_PSS_PIPELINES,
        doc="The pipelines' FQDNs",
    )
    def pipelinesFqdns(self: PssSubarray) -> List[str]:
        """Return the pipelines' TANGO FQDNs"""
        return self.PipelineFqdns

    @attribute(
        label="assignedPipelinesIDs",
        dtype=("DevUShort",),
        access=AttrWriteType.READ,
        max_dim_x=NUM_PSS_PIPELINES,
        doc="List of Pipeline IDs assigned to the PSS sub-array",
    )
    def assignedPipelinesIDs(self: PssSubarray) -> List[int]:
        """Return the assigned pipelines' IDs"""
        return self.component_manager._assigned_pipelines_ids

    @attribute(
        label="assignedPipelinesFqdns",
        dtype=("str",),
        access=AttrWriteType.READ,
        max_dim_x=NUM_PSS_PIPELINES,
        doc="List of Pipeline FQDNs assigned to the PSS sub-array",
    )
    def assignedPipelinesFqdns(self: PssSubarray) -> List[str]:
        """Return the assigned pipelines' FQDNs"""
        return self.component_manager._assigned_pipelines_fqdns

    # ------------------
    # Attributes methods
    # ------------------

    def read_adminMode(self: PssSubarray):
        """
        Read the Admin Mode of the device.

        :return: Admin Mode of the device
        :rtype: AdminMode
        """
        return self._admin_mode

    def is_adminMode_allowed(
        self: PssSubarray, request_type: tango.AttReqType
    ):
        """Verify the adminMode can be modified ."""
        if request_type == tango.AttReqType.READ_REQ or (
            self._obs_state == ObsState.EMPTY
            and request_type == tango.AttReqType.WRITE_REQ
        ):
            return True
        raise StateModelError(
            "adminMode can't be modified when the CSP.PSS Subarrays is"
            f" in observing state {self._obs_state.name}"
        )

    def write_adminMode(self: PssSubarray, value: AdminMode) -> None:
        """Set the administration mode for the whole CSP element.

        :param value: one of the administration mode value (ON-LINE,\
            OFF-LINE, ENGINEERING, NOT-FITTED, RESERVED).
        :type value: AdminMode
        """
        self.logger.debug(f"Set device adminMode to {AdminMode(value).name}")
        self.component_manager._store_admin_mode = (
            self.component_manager._admin_mode
        )
        self.component_manager._admin_mode = value
        try:
            if value == AdminMode.NOT_FITTED:
                self.admin_mode_model.perform_action("to_notfitted")
            elif value == AdminMode.OFFLINE:
                self.admin_mode_model.perform_action("to_offline")
                if self.component_manager.is_communicating:
                    self.component_manager.stop_communicating()
            elif value == AdminMode.ENGINEERING:
                self.admin_mode_model.perform_action("to_engineering")
                self.component_manager.start_communicating()
            elif value == AdminMode.ONLINE:
                self.admin_mode_model.perform_action("to_online")
                self.update_attribute("state", DevState.ON)
            elif value == AdminMode.RESERVED:
                self.admin_mode_model.perform_action("to_reserved")
            else:
                raise ValueError(f"Unknown adminMode {value}")
        # pylint: disable-next=broad-except
        except Exception as exc:
            self.logger.error(exc)

    def read_scanID(self: PssSubarray) -> int:
        """Return the scanID attribute.

        :return: the Subarray scanID
        """
        return self.component_manager._scan_id

    def write_scanID(self: PssSubarray, value: int) -> None:
        """Set the scanID attribute.

        :param The Subarray scanID
        """
        self.component_manager._scan_id = value

    def read_validScanConfiguration(self) -> str:
        """Return the validScanConfiguration attribute.

        :return: the JSON string containing the last valid scan configuration.
        """
        return self.component_manager.valid_scan_configuration

    def read_commandTimeout(self) -> int:
        """Return the commandTimeout attribute.

        :return: The duration of the command timeout.

        """
        return self.component_manager.command_timeout

    def write_commandTimeout(self, value):
        """
        Set the value of the command timeout.

        Configure the timeout applied to each command. The specified
        value shall be > 0.

        Negative values raise a *TypeError* exception. This is the expected
        behavior because the attribute has been defined as DevUShort.
        A zero value raises a *ValueError* exception.

        :raises: TypeError or tango.DevFailed depending on whether the
                specified value is negative or zero if a negative value.
        """
        self.component_manager.command_timeout = value

    def read_isCommunicating(self: PssSubarray) -> bool:
        """Whether the TANGO device is communicating with the controlled
        component.

        :return: Whether the device is communicating with the component under
            control.
        """
        return self.component_manager.is_communicating

    def read_obsMode(self: PssSubarray) -> List[ObsMode]:
        """Return the obsModes attribute."""
        return self.component_manager._obs_modes


# ----------
# Run server
# ----------


# pylint: disable-next=missing-function-docstring
def main(args=None, **kwargs):
    return run((PssSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
