# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""This module implements a functionality for the PSS
 Subarray Component Manager.
 """
from __future__ import annotations  # allow forward references in type hints

import concurrent.futures
import inspect
import json
import logging
import threading
import traceback
from functools import partial

# from functools import partial
from typing import Any, Callable, Dict, List, Optional, Tuple

# SKA imports
from ska_control_model import (
    AdminMode,
    CommunicationStatus,
    HealthState,
    ObsMode,
    ObsState,
    PowerState,
    ResultCode,
    TaskStatus,
)
from ska_tango_base.base import TaskCallbackType
from ska_tango_base.executor import TaskExecutorComponentManager
from ska_tango_base.long_running_commands_api import invoke_lrc
from tango import DeviceProxy

from ska_pss_lmc.common.capability import PssCapability
from ska_pss_lmc.common.connector import Connector

# Create module logger that logs all levels
module_logger = logging.getLogger(__name__)
module_logger.setLevel(logging.DEBUG)

MAX_WORKERS = 5
COMMAND_TIMEOUT = 15


# pylint: disable=too-many-arguments
# pylint: disable=too-many-locals
# pylint: disable=eval-used
# pylint: disable=too-many-function-args
# pylint: disable=too-many-public-methods
# pylint: disable=unused-argument
# pylint: disable=broad-except
# pylint: disable=protected-access
# pylint: disable=too-many-lines
# pylint: disable=logging-fstring-interpolation


def _call_task_callback(
    task_callback: Callable | None,
    **kwargs: Any,
) -> None:
    """Call the callback if it exists and add Kwargs arguments to it

    :param task_callback: the callback to be called when the status
        or progress of the task execution changes
    :param kwargs: keyword arguments passed to the function
    """
    if task_callback is not None:
        task_callback(**kwargs)


def lrc_callback(
    status: Optional[TaskStatus] = None,
    result: list[Any] = None,
    **kwargs,
):
    """
    Function called by the invoke_lrc method.

    :param status: the command TaskStatus
    :param result: the command ResultCode
    :param kwargs: keyword arguments passed to the function
    """
    logger, lrc_done = kwargs.get("logger"), kwargs.get("event_done")
    check_status = [TaskStatus.COMPLETED, TaskStatus.FAILED]
    # the scan command is considered executed when the subarray receives
    # the task status IN_PROGRESS.
    if "cmd_name" in kwargs:
        cmd_name = kwargs.get("cmd_name")
        if cmd_name == "Scan":
            check_status = [TaskStatus.IN_PROGRESS, TaskStatus.FAILED]
    if status is not None and status in check_status:
        logger.debug(f"status: {status} result {result}")
        lrc_done.set()


class PssSubarrayComponentManager(TaskExecutorComponentManager):
    """Component Manager for PSS Subarray."""

    def __init__(
        self: PssSubarrayComponentManager,
        max_workers: int,
        properties: List[str],
        communication_state_changed_callback: Callable = None,
        component_state_changed_callback: Callable = None,
        update_device_attribute_cbk: Callable = None,
        logger: Optional[logging.Logger] = None,
    ) -> None:
        """
        The Component Manager for the PSS subarray software component.
        """
        self._update_device_attribute = update_device_attribute_cbk
        self.logger = logger or module_logger
        super().__init__(
            logger=self.logger,
            max_workers=max_workers,
            communication_state_callback=communication_state_changed_callback,
            component_state_callback=component_state_changed_callback,
            power=PowerState.ON,
            fault=None,
            resourced=False,
            configured=False,
            scanning=False,
            obsfault=None,
        )

        self.pipeline_fqdns = []
        self._assigned_pipelines_ids = []
        self._assigned_pipelines_fqdns = []

        self.resources = {}
        self._subarray_id = 1
        self._scan_id = 0
        self._valid_scan_configuration = ""
        self._config_id = ""
        self._obs_modes = [ObsMode.IDLE]
        self._obs_state = ObsState.EMPTY
        self._health_state = HealthState.UNKNOWN
        self._admin_mode = AdminMode.OFFLINE
        self._store_admin_mode = AdminMode.OFFLINE
        self._sub_executor = concurrent.futures.ThreadPoolExecutor(
            max_workers=MAX_WORKERS,
            thread_name_prefix="SubarrayThreadPool",
        )
        self._event_id = {}
        self._scan_evt_id = {}
        self._access_lock = threading.Lock()

        self._init_properties(properties)
        self._init_connector()
        self._create_pss_capability()

    #########################
    # Properties
    #########################

    @property
    def is_communicating(self: PssSubarrayComponentManager) -> bool:
        """
        Return whether communication with the component is established.

        :return: whether there is currently a connection to the
            component
        """
        return self.communication_state == CommunicationStatus.ESTABLISHED

    @property
    def pipelines_json(self: PssSubarrayComponentManager) -> str:
        """
        Return a JSON string with the capability internal.
        """
        return self.pss_capability.json_str

    @property
    def pipelines_fqdn(self: PssSubarrayComponentManager) -> List[str]:
        """
        Return a list with the FQDNs of the connected devices.
        """
        return self.pss_capability.get_dict_entry("fqdn")

    @property
    def pipelines_state(self: PssSubarrayComponentManager) -> List[str]:
        """
        Return a list with the devices FQDNs.
        """
        return self.pss_capability.get_dict_entry("state")

    @property
    def pipelines_adminmode(self: PssSubarrayComponentManager) -> List[str]:
        """
        Return a list with the devices' adminmode.
        """
        return self.pss_capability.get_dict_entry("adminmode")

    @property
    def pipelines_healthstate(
        self: PssSubarrayComponentManager,
    ) -> List[str]:
        """
        Return a list with the devices' healthState.
        """
        return self.pss_capability.get_dict_entry("healthstate")

    @property
    def pipelines_obsstate(self: PssSubarrayComponentManager) -> List[str]:
        """
        Return a list with the devices' obsState.
        """
        return self.pss_capability.get_dict_entry("obsstate")

    @property
    def pipelines_connected(self: PssSubarrayComponentManager) -> List[str]:
        """
        Return a list with the devices' connection status.
        """
        return self.pss_capability.get_dict_entry("iscommunicating")

    @property
    def obs_modes(self: PssSubarrayComponentManager) -> List[ObsMode]:
        """Return the obsmodes supported by the subarray.

        :return: The list of obsModes supported by the subarray.
        """
        return self._obs_modes

    @property
    def obs_state(self: PssSubarrayComponentManager) -> ObsState:
        """Return the subarray obsState

        :return: The subarray obsState.
        """
        return self._obs_state

    @property
    def health_state(self: PssSubarrayComponentManager) -> ObsState:
        """Return the subarray HealthState

        :return: The subarray healthState.
        """
        return self._health_state

    @property
    def subarray_id(self: PssSubarrayComponentManager) -> int:
        """Return the subarray identification number.

        :return: The identification number of the PSS Subarray handled by the
            manager.
        """
        return self._subarray_id

    @subarray_id.setter
    def subarray_id(self: PssSubarrayComponentManager, value):
        """
        Set the subarray_membership.

        :raises: ValueError if the value is invalid
        """
        if value < 1 or value > 16:
            raise ValueError(
                f"Invalid request: {value} is outside range [1,16]"
            )
        self._subarray_id = value

    @property
    def valid_scan_configuration(self: PssSubarrayComponentManager) -> str:
        """Return the programmed configuration."""
        return self._valid_scan_configuration

    @property
    def config_id(
        self: PssSubarrayComponentManager,
    ) -> str:
        """Return the ID of the received configuration.

        :return: a string with the ID of the received configuration.
        """
        return self._config_id

    @property
    def assigned_pipelines_ids(
        self: PssSubarrayComponentManager,
    ) -> List[int]:
        """Return the list of assigned pipeline IDs."""
        return self._assigned_pipelines_ids

    @assigned_pipelines_ids.setter
    def assigned_pipelines_ids(
        self: PssSubarrayComponentManager, value: List[int]
    ) -> None:
        """Configure the list of assigned pipeline IDs.

        This value is configured at CM initialization.
        The subarray_id of the beam component is set to the corresponding
        subarray ID.

        :param value: list of assigned pipelines ids
        """
        self.logger.debug(f"Set assigned_pipelines_ids to {value}")
        self._assigned_pipelines_ids = value

    #########################
    # Class protected methods
    #########################
    def update_attribute(
        self: PssSubarrayComponentManager, attr_name: str, attr_value: Any
    ):
        """Verify if the callback is defined and lock the access."""
        with self._access_lock:
            if self._update_device_attribute:
                self._update_device_attribute(attr_name, attr_value)

    def _task_submitter(
        self: PssSubarrayComponentManager,
        task_name: str,
        task_callback: Callable = None,
        argin: Optional[Any] = None,
        **kwargs: Any,
    ) -> Tuple[TaskStatus, str]:
        """
        Common method to submit a command

        :param task_name: the name of the task to be submitted
        :param task_callback: the callback to be called when the status
            or progress of the task execution changes
        :param argin: input argument of the task
        :param kwargs: keyword arguments passed to the function

        :return: a tuple with *TaskStatus* and an informative msg
        """
        # NOTE: task_callback and argin are optional in the command,
        # we have to build the list accordingly
        args = [task_name, argin]
        args = [args[i] for i in range(2) if args[i] is not None]
        task_status, msg = self._task_executor.submit(
            eval(f"self._{task_name}"),
            args=args,
            kwargs=kwargs,
            task_callback=task_callback,
        )
        self.logger.info(
            f"Task {task_name} status {task_status}. Message: {msg}"
        )
        return task_status, msg

    def _init_properties(self: PssSubarrayComponentManager, properties):
        """
        Store the device properties in instance attribute

        :param properties: the class with the device properties
        """
        if hasattr(properties, "PipelineFqdns"):
            if isinstance(properties.PipelineFqdns, str):
                if properties.PipelineFqdns:
                    self.pipeline_fqdns = [
                        properties.PipelineFqdns,
                    ]
            else:
                # properties.PipelineFqdns is of StdStringVector
                # cast to list
                self.pipeline_fqdns = list(properties.PipelineFqdns)

    def _init_connector(self: PssSubarrayComponentManager):
        """Instantiate the Connector object"""
        try:
            self._connector = Connector(
                self.logger,
            )
        except Exception as err:
            self.logger.error("Error in instantiating Connector: " f"{err}")

    def _create_pss_capability(self: PssSubarrayComponentManager):
        """Instantiate the object to handle the PSS beams info"""
        try:
            self.pss_capability = PssCapability(
                self.pipeline_fqdns, self.update_attribute
            )
        except Exception as err:
            self.logger.error(
                "Error in instantiating PssCapability object: " f"{err}"
            )

    def _get_pipelines_proxy(
        self: PssSubarrayComponentManager,
    ) -> Dict[str, DeviceProxy]:
        """
        Return a python dict with the fqdn and proxy of the connected
        pipelines
        """
        return {
            fqdn: proxy
            for fqdn, proxy in self._connector.proxies.items()
            if fqdn in self._assigned_pipelines_fqdns
        }

    def _json_configuration_parser(
        self: PssSubarrayComponentManager,
        argin: dict,
    ) -> Any:
        """
        Configure the class that handles the parsing of the input JSON files
        sent as argument of AssignResources, Configure and Scan.
        """
        self.logger.info("Method not implemented")

    def _abort(
        self: PssSubarrayComponentManager,
        task_callback: Optional[Callable] = None,
    ):
        """Command Abort"""
        try:
            result_code = ResultCode.UNKNOWN
            for proxy_fqdn, proxy in self._get_pipelines_proxy().items():
                self.resources[proxy_fqdn] = None
                lrc_done = threading.Event()
                wrapped_callback = partial(
                    lrc_callback,
                    task_name="abort",
                    task_callback=task_callback,
                    event_done=lrc_done,
                    logger=self.logger,
                )
                lrc_subscriptions = invoke_lrc(
                    wrapped_callback,
                    proxy,
                    "abort",
                    command_args=None,
                    logger=self.logger,
                )
                if not self._wait_for_task_completion(
                    lrc_done,
                    lrc_subscriptions,
                ):
                    error_msg = (
                        f"Timeout expired executing ABORT"
                        f" command on {proxy_fqdn}"
                    )
                    self.logger.warning(error_msg)
                    raise ValueError(error_msg)
            task_status = TaskStatus.COMPLETED
            result_code = ResultCode.OK, "Abort Command completed"
            _call_task_callback(
                task_callback, status=task_status, result=result_code
            )
        except Exception as exc:
            warning_msg = (
                "Internal SW error executing in task ABORT on PSS"
                " Subarray. " + f"Received exception: {exc}"
            )
            self._handle_command_error(warning_msg, task_callback)

    def _restart(
        self: PssSubarrayComponentManager,
        task_name: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """Command Restart / ObsReset"""
        task_callback(status=TaskStatus.IN_PROGRESS)
        try:
            for proxy_fqdn, proxy in self._get_pipelines_proxy().items():
                lrc_done = threading.Event()
                wrapped_callback = partial(
                    lrc_callback,
                    task_name=task_name,
                    task_callback=task_callback,
                    event_done=lrc_done,
                    logger=self.logger,
                )
                lrc_subscriptions = invoke_lrc(
                    wrapped_callback,
                    proxy,
                    "obsreset",
                    command_args=None,
                )
                if not self._wait_for_task_completion(
                    lrc_done, lrc_subscriptions
                ):
                    error_msg = (
                        f"Timeout expired executing {task_name}"
                        f" command on {proxy_fqdn}"
                    )
                    self.logger.warning(error_msg)
                    raise ValueError(error_msg)
            self._update_component_state(obsfault=False)
            task_status = TaskStatus.COMPLETED
            result_code = ResultCode.OK, "Restart command completed"
            self._release_all("restart", {"source": "restart"})
            _call_task_callback(
                task_callback, status=task_status, result=result_code
            )
        except Exception as exc:
            warning_msg = (
                f"Error executing task {task_name} on PSS"
                " Subarray. " + f"Received exception: {exc}"
            )
            self._handle_command_error(warning_msg, task_callback)

        for proxy, evt_id in self._scan_evt_id.items():
            self._connector.unsubscribe_attribute(evt_id, proxy)

    def _assign(
        self: PssSubarrayComponentManager,
        task_name: str,
        argin: dict,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ):
        """Command Assign resources"""
        task_callback(status=TaskStatus.IN_PROGRESS)
        try:
            self.resources = argin  # no json parsing so far
            if not self.resources:
                error_msg = (
                    "Error in parsing the assignresources input json file"
                )
                raise ValueError(error_msg)
            # Check subarray_id
            input_subarray_id = self.resources["common"]["subarray_id"]
            if input_subarray_id != self.subarray_id:
                error_msg = (
                    f"Input subarray id {input_subarray_id} "
                    f"is different from device subarray id {self.subarray_id}"
                )
                raise ValueError(error_msg)
            # Basic check to verify assigned pss beams ids
            pipelines_to_assign = self.resources["pss"]["beams_id"]
            deployed_pipelines_ids = [
                int(fqdn[-4:]) for fqdn in self.pipeline_fqdns
            ]
            if set(pipelines_to_assign).issubset(deployed_pipelines_ids):
                # Connect to PSS pipeline through Connector
                self.logger.debug(
                    f"Call start_communicating with " f"{pipelines_to_assign}"
                )
                return self._start_communicating(
                    pipelines_to_assign, task_callback
                )
            msg = (
                f"Incoherent assignment of resources. The list of "
                f"pipelines to assign {pipelines_to_assign} is not "
                f"part of the deployed pipelines {deployed_pipelines_ids}"
            )
            raise ValueError(msg)
        except Exception as exc:
            warning_msg = (
                f"Error executing task {task_name} on PSS"
                f" Subarray. Received exception: {exc}"
            )
            # the call tho handle_command_error force the obsState to FAULT
            return self._handle_command_error(warning_msg, task_callback)

    def _configure(
        self: PssSubarrayComponentManager,
        task_name: str,
        argin: dict,
        task_callback: Callable = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """Command configure"""
        try:
            self.resources = argin  # no json parsing so far
            if not self.resources:
                error_msg = "Error in parsing the configure input json file"
                raise ValueError(error_msg)

            if not self._scan_preparation(json_input=argin):
                raise ValueError(
                    "Error in parsing the configure input json file"
                )
            task_callback(status=TaskStatus.IN_PROGRESS)
            self._config_id = self.resources["common"]["config_id"]
            for proxy_fqdn, proxy in self._get_pipelines_proxy().items():
                # What happens if pipeline is already on SCANNING?
                if proxy.obsState != ObsState.SCANNING:
                    lrc_done = threading.Event()
                    wrapped_callback = partial(
                        lrc_callback,
                        task_name="configurescan",
                        task_callback=task_callback,
                        event_done=lrc_done,
                        logger=self.logger,
                    )
                    lrc_subscriptions = invoke_lrc(
                        lrc_callback=wrapped_callback,
                        proxy=proxy,
                        command="configureScan",
                        command_args=(json.dumps(argin),),
                    )
                    if not self._wait_for_task_completion(
                        lrc_done, lrc_subscriptions
                    ):
                        error_msg = (
                            f"Timeout expired executing {task_name}"
                            f" command on {proxy_fqdn}"
                        )
                        self.logger.warning(error_msg)
                        raise ValueError(error_msg)
            task_status = TaskStatus.COMPLETED
            result_code = ResultCode.OK, "Config Command completed"
            self._update_component_state(configured=True)
            _call_task_callback(
                task_callback, status=task_status, result=result_code
            )
        except Exception as exc:
            warning_msg = (
                f"Error executing task {task_name} on PSS"
                " Subarray. " + f"Received exception: {exc}"
            )
            self._handle_command_error(warning_msg, task_callback)

    def _store_scan_id(self, json_input: dict) -> None:
        """Extract and store the Scan Id from the scan input json.

        :param json_input: scan input json (dict)
        """
        self._scan_id = json_input["common"]["subarray_id"]
        self.update_attribute("scanID", int(self._scan_id))

    def _scan_preparation(
        self: PssSubarrayComponentManager, json_input: dict
    ) -> bool:
        """Preliminary check for the configure command and
        store scan id.

        :param json_input: scan input json (dict)
        """
        input_ok = True
        if json_input:
            self._store_scan_id(json_input)
            # Retrieve pipelines data from json input (DRAFT)
            for fqdn in self._assigned_pipelines_fqdns:
                beams_list = json_input["pss"]["beam"]
                beam_matches = [
                    beam
                    for beam in beams_list
                    if beam["beam_id"] == int(fqdn[-4:])
                ]
                if len(beam_matches) > 1:
                    self.resources[fqdn] = beam_matches[0]
        else:
            error_msg = "Error in parsing the scan input json file"
            self.logger.error(error_msg)
            input_ok = False
        return input_ok

    def _scan(
        self: PssSubarrayComponentManager,
        task_name: str,
        argin: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Method submitted into thread executor for scan command.

        :param task_name: the name of the task
        :param argin: resources to be assigned to Subarray
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        def check_faults(evt):
            """
            Callback for monitoring pipeline ObsState attribute.
            If the ObsState is FAULT, the Subarray ObsState is set to FAULT.
            """
            if not evt.err and evt.attr_value.value == ObsState.FAULT:
                self.logger.error(
                    f"Pipeline {evt.device.name()} is in FAULT state"
                )
                self.logger.error("Subarray will go in FAULT state")
                self._update_component_state(obsfault=True)
                # do we need to put also healthState to FAILED?

        try:
            # Check json input
            if not argin or argin == {}:
                error_msg = "Error in parsing the scan input json file"
                raise ValueError(error_msg)
            task_callback(status=TaskStatus.IN_PROGRESS)
            for proxy_fqdn, proxy in self._get_pipelines_proxy().items():
                # What happens if pipeline is already on SCANNING?
                event_dict = self._connector.subscribe(
                    proxy, ["obsstate"], check_faults
                )
                self._scan_evt_id[proxy] = event_dict["obsstate"]
                if proxy.obsState != ObsState.SCANNING:
                    lrc_done = threading.Event()
                    wrapped_callback = partial(
                        lrc_callback,
                        task_name=task_name,
                        task_callback=task_callback,
                        event_done=lrc_done,
                        logger=self.logger,
                        cmd_name="Scan",
                    )
                    lrc_subscriptions = invoke_lrc(
                        lrc_callback=wrapped_callback,
                        proxy=proxy,
                        command=task_name,
                        command_args=(json.dumps(argin),),
                    )
                    if not self._wait_for_task_completion(
                        lrc_done, lrc_subscriptions
                    ):
                        error_msg = (
                            f"Timeout expired executing {task_name}"
                            f" command on {proxy_fqdn}"
                        )
                        self.logger.warning(error_msg)
                        raise ValueError(error_msg)
            task_status = TaskStatus.COMPLETED
            result_code = ResultCode.OK, "Scan Command completed"
            # Reflect Subarray obsstate (To be refined)
            subarray_obsstate = self._evaluate_subarray_obsstate(
                self.pipelines_obsstate,
            )
            self.logger.info(
                f"Subarray obsstate is {ObsState(subarray_obsstate).name}"
            )
            self.update_attribute("obsState", subarray_obsstate)
            self._obs_state = subarray_obsstate
            self._update_component_state(scanning=True)
            _call_task_callback(
                task_callback, status=task_status, result=result_code
            )
        # pylint: disable-next=broad-except
        except Exception as exc:
            warning_msg = (
                f"Error executing task {task_name} on PSS"
                " Subarray. " + f"Received exception: {exc}"
            )
            self._handle_command_error(warning_msg, task_callback)

    def _release(
        self: PssSubarrayComponentManager,
        task_name: str,
        argin: dict,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ):
        """Command release resources
        :param task_name: the name of the task
        :param argin: resources to be released from the Subarray
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """
        task_status = TaskStatus.FAILED
        try:
            if not self._assigned_pipelines_fqdns:
                error_msg = "No pipelines assigned to the PSS Subarray"
                raise ValueError(error_msg)
            for fqdn, resource in self.resources.items():
                if (
                    "pss" in fqdn
                    and resource["common"]["subarray_id"] == self.subarray_id
                ):
                    if fqdn not in self._assigned_pipelines_fqdns:
                        error_msg = (
                            f"Impossible to remove {fqdn}: "
                            "not present in deployment"
                        )
                        raise ValueError(error_msg)
                    for fqdn, _ in self._get_pipelines_proxy().items():
                        self._connector.proxies.pop(fqdn)
                        if fqdn in self._assigned_pipelines_fqdns:
                            self._assigned_pipelines_fqdns.pop(fqdn)
                    self._assigned_pipelines_ids = [
                        int(fqdn[-4:])
                        for fqdn in self._assigned_pipelines_fqdns
                    ]
            # Task completed
            task_status = TaskStatus.COMPLETED
            result_code = (
                ResultCode.OK,
                f"Release completed. Assigned pipelines are "
                f"{self._assigned_pipelines_fqdns}",
            )
            self._update_component_state(
                resourced=bool(self._assigned_pipelines_fqdns)
            )
            _call_task_callback(
                task_callback, status=task_status, result=result_code
            )
        except Exception as exc:
            warning_msg = (
                f"Error executing task {task_name} on PSS"
                f" Subarray. Received exception: {exc}"
            )
            self._handle_command_error(warning_msg, task_callback)

    def _release_all(
        self: PssSubarrayComponentManager,
        task_name: str,
        argin: dict,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ):
        """Command release all resources
        :param task_name: the name of the task
        :param argin: resources to be released to Subarray
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending

        """
        task_status = TaskStatus.FAILED
        try:
            for fqdn in self._assigned_pipelines_fqdns:
                # Unsubscribe all attribute events for the pipeline proxy
                self._unsubscribe(fqdn)
                self._connector.proxies.pop(fqdn)
            self.resources = {}
            self._assigned_pipelines_fqdns = []
            self._assigned_pipelines_ids = []
            # Task completed
            task_status = TaskStatus.COMPLETED
            result_code = (
                ResultCode.OK,
                "All assigned pipelines have been released",
            )
            self._update_component_state(resourced=False)
            # If method is called from command restart do not re-update state
            if (
                argin
                and argin["source"] == "restart"
                and self._obs_state in [ObsState.EMPTY]
            ):
                return
            _call_task_callback(
                task_callback, status=task_status, result=result_code
            )
        except Exception as exc:
            warning_msg = (
                f"Error executing task {task_name} on PSS"
                f" Subarray. Received exception: {exc}"
            )
            self._handle_command_error(warning_msg, task_callback)

    def _endscan(
        self: PssSubarrayComponentManager,
        task_name: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """Command to end the scan.
        :param task_name: the name of the task
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        task_callback(status=TaskStatus.IN_PROGRESS)
        try:
            for proxy_fqdn, proxy in self._connector.proxies.items():
                self.resources[proxy_fqdn] = None
                lrc_done = threading.Event()
                wrapped_callback = partial(
                    lrc_callback,
                    task_name=task_name,
                    task_callback=task_callback,
                    event_done=lrc_done,
                    logger=self.logger,
                )
                lrc_subscriptions = invoke_lrc(
                    lrc_callback=wrapped_callback,
                    proxy=proxy,
                    command=task_name,
                )
                if not self._wait_for_task_completion(
                    lrc_done,
                    lrc_subscriptions,
                ):
                    error_msg = (
                        f"Timeout expired executing {task_name}"
                        f" command on {proxy_fqdn}"
                    )
                    self.logger.warning(error_msg)
                    raise ValueError(error_msg)
            task_status = TaskStatus.COMPLETED
            result_code = ResultCode.OK, "End Scan Command completed"
            self._update_component_state(scanning=False, obsfault=False)
            _call_task_callback(
                task_callback, status=task_status, result=result_code
            )
        # pylint: disable-next=broad-except
        except Exception as exc:
            warning_msg = (
                f"Error executing task {task_name} on PSS"
                " Subarray. " + f"Received exception: {exc}"
            )
            self._handle_command_error(warning_msg, task_callback)
        for proxy, evt_id in self._scan_evt_id.items():
            self._connector.unsubscribe_attribute(evt_id, proxy)

    def _gotoidle(
        self: PssSubarrayComponentManager,
        task_name: str,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """Command GoToIdle
        :param task_name: the name of the task
        :param task_callback: method called to update the task result
            related attributes
        :param task_abort_event: flag to signal an abort request pending
        """

        def _lrc_callback(
            status: None, result: list[Any] | None = None, **kwargs
        ):
            task_callback(status=status, result=result)

        try:
            for proxy_fqdn, proxy in self._connector.proxies.items():
                self.resources[proxy_fqdn] = None
                lrc_done = threading.Event()
                wrapped_callback = partial(
                    lrc_callback,
                    task_name=task_name,
                    task_callback=task_callback,
                    event_done=lrc_done,
                    logger=self.logger,
                )
                lrc_subscriptions = invoke_lrc(
                    lrc_callback=wrapped_callback,
                    proxy=proxy,
                    command=task_name,
                )
                if not self._wait_for_task_completion(
                    lrc_done,
                    lrc_subscriptions,
                ):
                    error_msg = (
                        f"Timeout expired executing {task_name}"
                        f" command on {proxy_fqdn}"
                    )
                    self.logger.warning(error_msg)
                    raise ValueError(error_msg)
            task_status = TaskStatus.COMPLETED
            result_code = ResultCode.OK, "GoToIdle Command completed"
            self._update_component_state(configured=False, obsfault=False)
            _call_task_callback(
                task_callback, status=task_status, result=result_code
            )
        except Exception as exc:
            warning_msg = (
                f"Internal SW error executing  in task {task_name} on PSS"
                " Subarray. " + f"Received exception: {exc}"
            )
            self._handle_command_error(warning_msg, task_callback)

    def _handle_command_error(
        self: PssSubarrayComponentManager,
        error_msg: str,
        task_callback: Callable,
    ) -> Tuple[TaskStatus, Tuple[ResultCode, str]]:
        """Routine for handling errors in command execution.
        :param error_msg: input error_msg string
        :param task_callback: method called to update the task result
            related attributes
        """
        self.logger.error(error_msg)
        self.logger.error(traceback.format_exc())
        result_code = ResultCode.FAILED, error_msg
        task_status = TaskStatus.FAILED
        self._update_component_state(obsfault=True)

        return task_callback(status=task_status, result=result_code)

    def _start_communicating(
        self: PssSubarrayComponentManager,
        pipelines_ids: List[int] = None,
        task_callback: Optional[Callable] = None,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """
        Task submitted by start_communicating.

        This method is performed inside AssignResources command.

        :param: task_callback: Task callback passed during the
            submission of the task
        :param: task_abort_event: the abort event (still not implemented)
        """
        pipelines_ids = pipelines_ids or []
        self._assigned_pipelines_fqdns = []
        self._assigned_pipelines_ids = []
        connect_exception = None
        try:
            # Handle pipeline connections
            connection_result = self._establish_pipeline_connections(
                pipelines_ids
            )
            # Update subarray state based on connection results
            task_status, result = self._update_subarray_state(
                connection_result, pipelines_ids
            )
        except Exception as err:
            connect_exception = err
            msg = "Failure in connection to subordinate devices %s", str(
                connect_exception
            )
            self._update_component_state(obsfault=True)
            self.logger.error(msg)
        if task_callback:
            task_callback(
                status=task_status,
                result=result,
                exception=connect_exception,
            )

    def _establish_pipeline_connections(
        self, pipelines_ids: List[int]
    ) -> dict:
        """
        Establishes connections with the specified pipelines.

        :param pipelines_ids: List of pipeline IDs to connect to
        :return: Dictionary containing connection status for each pipeline
        """
        _pipelines_fqdns = self._create_assigned_pipelines(pipelines_ids)
        _communication_status = {
            fqdn: CommunicationStatus.NOT_ESTABLISHED
            for fqdn in _pipelines_fqdns
        }
        futures = [
            self._sub_executor.submit(
                self._connect_and_subscribe,
                fqdn,
            )
            for fqdn in _pipelines_fqdns
        ]
        # synchronize to wait for the end of connection
        concurrent.futures.wait(futures)
        for future in futures:
            device_fqdn, comm_status = future.result()
            _communication_status[device_fqdn] = comm_status
            self.logger.debug(f"communication status {_communication_status}")
            if comm_status == CommunicationStatus.ESTABLISHED:
                self._assigned_pipelines_fqdns.append(device_fqdn)
                self._assigned_pipelines_ids.append(int(device_fqdn[-4:]))
        return _communication_status

    def _update_subarray_state(
        self, communication_status: dict, pipelines_ids: List[int]
    ) -> Tuple[TaskStatus, Tuple[ResultCode, str]]:
        """
        Updates the subarray state based on connection results.

        :param communication_status: Dictionary containing connection status
            for each pipeline
        :param pipelines_ids: List of pipeline IDs that were requested
        :return: Tuple containing task status and result
        """
        (
            subarray_communication_status,
            _,
        ) = self._evaluate_subarray_communication_status(communication_status)
        # Switch to correct HealthState (To be refined)
        subarray_health_state = self._evaluate_subarray_healthstate(
            self.pipelines_healthstate,
        )
        self.logger.info(
            "Subarray health state is "
            f"{HealthState(subarray_health_state).name}"
        )
        self.update_attribute("healthState", subarray_health_state)
        self._health_state = subarray_health_state

        self._update_communication_state(subarray_communication_status)
        if subarray_communication_status == CommunicationStatus.ESTABLISHED:
            if pipelines_ids != self._assigned_pipelines_ids:
                msg = (
                    f"Partial success. Connected pipelines "
                    f"{self._assigned_pipelines_ids}."
                    f"Required pipelines:{pipelines_ids}"
                )
            else:
                msg = (
                    f"PSS Subarray successfully connected "
                    f"to all pipelines {pipelines_ids}"
                )
            result = ResultCode.OK, msg
            self.update_attribute("isCommunicating", self.is_communicating)
            self.logger.info(f"component_state: {self._component_state}")
            self._update_component_state(power=PowerState.ON)
        else:
            msg = f"Failure in connection to pipelines {pipelines_ids}"
            result = ResultCode.FAILED, msg
        task_status = TaskStatus.COMPLETED
        self._update_component_state(
            resourced=bool(self._assigned_pipelines_fqdns)
        )
        return task_status, result

    def _get_subscription_data(
        self: PssSubarrayComponentManager,
        device_fqdn: str,
    ) -> Tuple[List[str], Callable]:
        """
        Retrieves the list of attributes to subscribe to and the
        corresponding callback for handling change events.The subscription
        details vary based on the device family.

        :param device_fqdn: The TANGO FQDN of the device.

        :return: A tuple containing the list of attributes and the
            associated callback function.
        """
        attr_list, callback = [], None
        if "pipeline" in device_fqdn:
            attr_list, callback = (
                self.pss_capability.pipeline_attributes,
                self.pss_capability.update_pipeline_info,
            )
        else:
            self.logger.info(f"No subscriptions for device {device_fqdn}")
        return (attr_list, callback)

    def _connect_and_subscribe(
        self: PssSubarrayComponentManager, device_fqdn: str
    ) -> Tuple[str, CommunicationStatus]:
        """
        Establishes a connection to the specified device and subscribes to its
        relevant attributes.

        :param device_fqdn: The TANGO fully qualified domain name (FQDN) of
                the device.

        :return: A tuple containing the connection status and the device FQDN.
        """

        status = self._connector.connect(device_fqdn)
        if status == CommunicationStatus.ESTABLISHED:
            # retrieve the list of attribute to subscribe and the callback
            # to invoke for the subarray and pipeline devices.
            attribute_list, callback = self._get_subscription_data(device_fqdn)
            proxy = self._connector.get_device(device_fqdn)
            if attribute_list and callback:
                event_dict = self._connector.subscribe(
                    proxy, attribute_list, callback
                )
                self._event_id = {**self._event_id, **event_dict}
        return device_fqdn, status

    def _unsubscribe(
        self: PssSubarrayComponentManager,
        device_fqdn: str,
        attr_list: List[str] = None,
    ):
        """
        Unsubscribe the relevant attributes from the specified device.

        :param device_fqdn: The TANGO fully qualified domain name (FQDN) of
                the device.
        """
        self.logger.debug(f"event_id:{self._event_id}")
        if not attr_list:
            attr_list = list(self._event_id.keys())
        self.logger.debug(
            f"attributes to unsubscribe:{attr_list} on component {device_fqdn}"
        )
        proxy = self._connector.get_device(device_fqdn)
        attrs_to_remove = []
        for attr_name in attr_list:
            try:
                _id = self._event_id[attr_name]
                self.logger.debug(
                    f"Going to unsubscribe attribute {attr_name} event id"
                    f" {_id}"
                )
                self._connector.unsubscribe_attribute(_id, proxy)
                # build the list of the attribute/id couple to remove from the
                # member _event_id dictionary
                attrs_to_remove.append(attr_name)
            # pylint: disable-next=broad-except
            except KeyError as exc:
                self.logger.debug(
                    f"{device_fqdn}: Attribute {exc} already unsubscribed."
                )
            except Exception as exc:
                self.logger.warning(
                    f"Failed to unsubscribe attribute {attr_name}: {exc}"
                )
        # remove the un-subscribed events from the dictionary
        self.logger.debug(f"Attributes to remove: {attrs_to_remove}")
        for attr_name in attrs_to_remove:
            self._event_id.pop(attr_name)

    def _evaluate_subarray_communication_status(
        self: PssSubarrayComponentManager, communication_status_lst
    ) -> Tuple[CommunicationStatus, HealthState]:
        """Evaluate the connection status of the PSS Subarray device.

        :param communications_status_lst: a list with the communication
            status with the subordinate pipeline devices.

        :return: a tuple with the PSS Subarray CommunicationStatus and
            its healthState.The healthState is reported as:

            - FAILED if all the communication status are NOT_ESTABLISHED
            - OK if all the communication status are ESTABLISHED
            - DEGRADED if at least one of the communication status is
                NOT_ESTABLISHED
        """
        health = HealthState.UNKNOWN
        if all(
            status == CommunicationStatus.NOT_ESTABLISHED
            for status in communication_status_lst.values()
        ):
            comm_status, health = (
                CommunicationStatus.NOT_ESTABLISHED,
                HealthState.FAILED,
            )
        elif all(
            status == CommunicationStatus.ESTABLISHED
            for status in communication_status_lst.values()
        ):
            comm_status, health = (
                CommunicationStatus.ESTABLISHED,
                HealthState.OK,
            )
        else:
            comm_status, health = (
                CommunicationStatus.ESTABLISHED,
                HealthState.DEGRADED,
            )
        return comm_status, health

    def _evaluate_subarray_obsstate(
        self: PssSubarrayComponentManager, obsstate_lst
    ) -> ObsState:
        """Evaluate the obsstate of the PSS Subarray device.

        :param obsstate_lst: a list with the obsstates
            of the subordinate pipeline devices.

        :return: the PSS Subarray Obsstate
        """
        # TBD
        return ObsState[obsstate_lst[0]].value

    def _evaluate_subarray_healthstate(
        self: PssSubarrayComponentManager, healthstate_lst
    ) -> HealthState:
        """Evaluate the healthstate of the PSS Subarray device.

        :param healthstate_lst: a list with the healthstates
            of the subordinate pipeline devices.

        :return: the PSS Subarray HealthState
        """
        # TBD
        return HealthState[healthstate_lst[0]].value

    def _create_assigned_pipelines(
        self: PssSubarrayComponentManager,
        assigned_pipelines_ids: List[int],
    ) -> List[str]:
        """Create a list of TANGO fqdns from the assigned pipeline ids

        :param assigned_pipelines_ids: a list of int representing the ids
            of the assigned pipeline devices

        :return: a list of string representing the fqdns of the assigned
            pipeline devices
        """
        for beam_id in assigned_pipelines_ids:
            return [
                fqdn
                for fqdn in self.pipelines_fqdn
                if int(fqdn[-4:]) == beam_id
            ]

    def _wait_for_task_completion(
        self: PssSubarrayComponentManager,
        task_event: threading.Event,
        lrc_subscriptions=None,
    ) -> bool:
        """Wait for task completion, checking if the timeout has expired."""
        while not task_event.wait(timeout=COMMAND_TIMEOUT):
            return False
        return True

    ##############################
    # Class public methods
    ##############################

    def start_communicating(
        self: PssSubarrayComponentManager, callback: Callable | None = None
    ) -> None:
        """
        Subarray communication establishment is performed within
        command AssignResources.
        """
        self._update_component_state(power=PowerState.ON)

    def stop_communicating(self: PssSubarrayComponentManager) -> None:
        """
        Close connection with the software component.

        Close the ssh channel between this instance of the component manager
        and the controlled software component (cheetah).
        The communication state variable is set to DISCONNECTED and
        the adminMode to OFFLINE.
        """
        if self.communication_state == CommunicationStatus.DISABLED:
            return
        self._update_component_state(power=PowerState.UNKNOWN, fault=None)
        self._update_communication_state(CommunicationStatus.DISABLED)
        self.update_attribute("healthstate", HealthState.UNKNOWN)

    def on(
        self: PssSubarrayComponentManager,
        task_callback: TaskCallbackType | None = None,
    ):
        self.logger.info("Method not implemented")
        return TaskStatus.REJECTED, "Subarray is always ON."

    def off(
        self: PssSubarrayComponentManager,
        task_callback: TaskCallbackType | None = None,
    ):
        self.logger.info("Method not implemented")
        return TaskStatus.REJECTED, "Off command is not used"

    def standby(
        self: PssSubarrayComponentManager,
        task_callback: TaskCallbackType | None = None,
    ):
        self.logger.info("Method not implemented")
        return TaskStatus.REJECTED, "Standby command is not used"

    def configure(
        self: PssSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
        **argin: dict,
    ) -> Tuple[TaskStatus, str]:
        """Configure"""
        self._config_id = ""
        return self._task_submitter(
            task_name=self.method_name(),
            task_callback=task_callback,  # command_id
            argin=argin,
        )

    def abort(
        self: PssSubarrayComponentManager,
        task_callback: TaskCallbackType | None = None,
    ) -> Tuple[TaskStatus, str]:
        """Abort"""
        self.logger.debug("Invoked abort tasks")
        self._task_executor._abort_event.set()
        threading.Thread(
            target=self._abort,
            args=(task_callback,),
        ).start()
        return TaskStatus.IN_PROGRESS, "Abort started"

    def restart(
        self: PssSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Restart"""
        self._task_executor._abort_event.clear()
        return self._task_submitter(
            task_name=self.method_name(),
            task_callback=task_callback,  # command_id
        )

    def assign(
        self: PssSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
        **argin: dict,
    ) -> Tuple[TaskStatus, str]:
        """Assign resources"""
        task_status, msg = self._task_submitter(
            task_name=self.method_name(),
            task_callback=task_callback,  # command_id
            argin=argin,
        )
        # self.update_attribute("obsState", self._obs_state)
        return task_status, msg

    def release(
        self: PssSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]],
        **argin: dict,
    ) -> Tuple[TaskStatus, str]:
        """Release resources"""
        return self._task_submitter(
            task_name=self.method_name(),
            task_callback=task_callback,  # command_id
            argin=argin,
        )

    def release_all(
        self: PssSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]],
        **argin: dict,
    ) -> Tuple[TaskStatus, str]:
        """Release all resources"""
        return self._task_submitter(
            task_name=self.method_name(),
            task_callback=task_callback,  # command_id
            argin=argin,
        )

    def scan(
        self: PssSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
        **argin: dict,
    ) -> Tuple[TaskStatus, str]:
        """Scan"""
        return self._task_submitter(
            task_name=self.method_name(),
            task_callback=task_callback,  # command_id
            argin=argin,
        )

    def endscan(
        self: PssSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """End Scan"""
        return self._task_submitter(
            task_name=self.method_name(),
            task_callback=task_callback,  # command_id
        )

    def gotoidle(
        self: PssSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        """Gotoidle command

        :param task_callback: method called to update the task result
            related attributes
        :return: a tuple with *TaskStatus* and an informative message.
        """
        return self._task_submitter(
            task_name=self.method_name(),
            task_callback=task_callback,
        )

    # Alias for gotoidle
    deconfigure = gotoidle
    # Alias for restart
    obsreset = restart
    # Alias for endscan
    end_scan = endscan
    # Alias for release_all
    releaseallresources = release_all

    def reset(
        self: PssSubarrayComponentManager,
        task_callback: Optional[Callable[[], None]] = None,
    ) -> Tuple[TaskStatus, str]:
        return super().reset()

    def method_name(self: PssSubarrayComponentManager):
        """
        :return: name of caller method
        """
        return inspect.currentframe().f_back.f_code.co_name
