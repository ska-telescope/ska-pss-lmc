"""This module provides Test Device class and related utilities for testing-"""
import logging
import time

import tango
from pytest_bdd import given, parsers, then, when
from ska_tango_base.control_model import (
    AdminMode,
    HealthState,
    ObsState,
    SimulationMode,
)
from tango import DevState

from ska_pss_lmc.testing.poller import Poller, Probe, probe_poller

# pylint: disable=logging-fstring-interpolation
# pylint: disable=broad-except
# pylint: disable=eval-used
# pylint: disable=exec-used
# pylint: disable=unspecified-encoding


module_logger = logging.getLogger(__name__)


class TestDevice:
    """A class responsible for creation of device proxies and step definitions
    for bdd tests."""

    def __init__(
        self,
        name: str = None,
    ) -> None:

        self.name = name
        self.proxy = self.get_device_proxy()
        self.logger = module_logger
        self.temp_dev_info = None

    def get_device_proxy(self):
        """ "Connect to the device proxy for all devices listed in Tango
        database."""
        idx = ""
        klass = self.name
        if klass[-4:] in ["0001", "0002", "0003"]:
            idx = klass[-4:]
            klass = klass[:-4]
        database = tango.Database()
        instance_list = database.get_device_exported_for_class(klass)
        for instance in instance_list.value_string:
            try:
                if idx in instance:
                    device = tango.DeviceProxy(instance)
                    class_name = klass
                    self.name = class_name + idx
                    return device
            except tango.DevFailed:
                continue
        raise Exception(f"Could not find {self.name}")

    def initialize_device(self):
        """Initialize device."""
        dev_name = self.proxy.name()
        module_logger.info(f"Reinitialize {dev_name} at :{time.asctime()}")
        retry_count = 0
        ret = False
        while retry_count < 5:
            try:
                self.proxy.init()
                module_logger.info(
                    f"Device {self.name} restarted at :{time.asctime()}"
                    f" retry: {retry_count}"
                )
                ret = True
                break
            except Exception as exc:
                module_logger.warning(
                    f" initialize_device: retry: {retry_count}"
                )
                retry_count += 1
                if retry_count == 5:
                    module_logger.error(
                        f"initialize_device: got exception {exc} "
                    )

                time.sleep(1.5)
        return ret

    def ping_device(self):
        """Verify connection to the device by pinging it."""
        timeout = 30
        total = 0
        step = 0.1
        while True:
            try:
                self.proxy.ping()
                break
            except Exception:
                if total > timeout:
                    assert False, f"Initialziation of {self.name} failed"
                time.sleep(step)
                total += step
                continue

    def restart_device_server(self):
        """Restart device server."""
        device_adm = tango.DeviceProxy(self.proxy.adm_name())
        # Kill the device server, the k8s command retries
        # to restart it up to 60 times
        device_adm.kill()
        # Check that restart has finished
        timeout = 20
        total = 0
        step = 1
        while True:
            try:
                self.proxy.ping()
                break
            except Exception:
                if total > timeout:
                    assert False, f"Initialziation of {self.name} failed"
                time.sleep(step)
                total += step
                continue
        # Even though we wait for the device to be pingable again,
        # it doesn't seem to show up in
        # TangoDB, strange... 60 seconds were empirically determined
        time.sleep(60)

    def send_command(self, command_name):
        """Send command to device."""
        command = getattr(self.proxy, command_name)
        try:
            command()
        except Exception as exc:
            # NOTE: error in command should not block the test execution.
            # Failures have to be caught in further steps
            module_logger.warning(exc)

    def send_command_with_argument(self, command_name, argument):
        """Send command with argument to device."""
        command = getattr(self.proxy, command_name)
        if "[" in argument:
            argument = eval(argument)
        try:
            command(argument)
        except Exception as exc:
            # NOTE: error in command should not block the test execution.
            # Failures have to be caught in further steps
            module_logger.warning(exc)

    def send_command_with_argument_from_file(self, command_name, filename):
        """Send command with argument from file to device."""
        command = getattr(self.proxy, command_name)
        with open(f"tests/test_data/{filename}") as f:
            argument = f.read().replace("\n", "")
        try:
            command(argument)
        except Exception as exc:
            # NOTE: error in command should not block the test execution.
            # Failures have to be caught in further steps
            module_logger.warning(exc)

    def set_exception(self):
        """Set exception."""
        self.proxy.SetException(True)

    def raise_fault(self, fault):
        """Raise fault."""
        self.proxy.raise_fault = fault

    def disable_device(self, disabled):
        """Disable device."""
        if disabled:
            self.proxy.adminmode = 1
        else:
            module_logger.info(
                f"setting admin mode to {0} at {time.asctime()}"
            )
            self.proxy.adminmode = 0

    def get_attribute(self, attribute, value, timeout=5):
        """Get attribute of the device."""
        attribute = attribute.lower()
        if "pipelines" in attribute:
            if isinstance(value, str):
                prober = Probe(
                    self.proxy,
                    attribute,
                    eval(value),
                    f"{attribute} is not {value}."
                    f"It is {self.proxy.read_attribute(attribute).value}",
                )
            Poller(timeout, 0.1).check(prober)
            return

        if "healthstate" in attribute:
            probe_poller(
                self.proxy, attribute, HealthState[value.upper()], time=20
            )

        elif "obsstate" in attribute:
            probe_poller(
                self.proxy, attribute, ObsState[value.upper()], time=20
            )

        elif "adminmode" in attribute:
            probe_poller(
                self.proxy, attribute, AdminMode[value.upper()], time=20
            )

        elif "state" in attribute:
            probe_poller(
                self.proxy,
                attribute,
                getattr(DevState, value.upper()),
                time=20,
            )

        elif "lastscanconfiguration" in attribute:
            if value == "empty":
                probe_poller(self.proxy, attribute, "", time=20)
            else:
                probe_poller(self.proxy, attribute, value, time=20)

        elif "longRunningCommandStatus" in attribute:
            probe_poller(
                self.proxy,
                attribute,
                ("*", value),
                time=20,
            )

        else:
            probe_poller(self.proxy, attribute, eval(value), time=20)

    def set_attribute(self, attribute, value):
        """Set attribute of the device."""
        if "adminmode" in attribute.lower():
            value = AdminMode[value.upper()]
            module_logger.info(f"set admin mode to {value}")
        elif "simulationmode" in attribute.lower():
            value = SimulationMode[value.upper()]
            module_logger.info(
                f"set SimulationMode to {SimulationMode(value).name}"
            )
        elif "testalarm" in attribute.lower():
            _value = False
            if value == "True":
                _value = True
            value = _value
        self.proxy.write_attribute(attribute, value)

    def state(self):
        """Get state."""
        return self.proxy.state()

    def return_device_name(self):
        """Get device name"""
        return self.name

    def simulation_mode(self):
        """Get simulation mode."""
        return self.proxy.read_attribute(SimulationMode)


#  -------------- SETUP --------------

# definition of all controllers, subarrays and beams
# implemented for each testing system in tests/integration/step_defs
# or tests/simulated-systems/step_defs

#  -------------- STEP DEFINITIONS --------------


@given(parsers.parse("{device_name} is fresh initialized"))
def fresh_pss_device(device_name, all_pss_devices, all_pipelines):
    """Initialize fresh controller and subarray."""
    if device_name in all_pipelines:
        fresh_new_pipeline(device_name[-4:], all_pipelines)
        return
    device = all_pss_devices[device_name]

    module_logger.debug(f"Reinit {device_name}")
    device.disable_device(True)
    device.initialize_device()
    device.ping_device()
    if "controller" in device_name.lower():
        check_controller_init_state(device)
    elif "subarray" in device_name.lower():
        check_subarray_init_state(device)
    else:
        module_logger.info(f"Invalid device name: {device_name}")


@given(parsers.parse("PipelineCtrlDevice{number} is fresh initialized"))
def fresh_new_pipeline(number, all_pipelines):
    """Initialize fresh pipeline."""
    module_logger.debug(f"Fresh pipeline {number}")
    devices = []
    for device in all_pipelines.values():
        device_name = device.return_device_name()
        if number not in device_name:
            continue
        module_logger.debug("Reinit pipeline")
        devices.append(device)
    for device in devices:
        device.disable_device(True)
    for device in devices:
        device.initialize_device()
    for device in devices:
        device.ping_device()

    for device in all_pipelines.values():
        device_name = device.return_device_name()
        if number not in device_name:
            continue
        check_pipeline_init_state(device)


def check_pipeline_init_state(device):
    """Check initial state of the pipeline."""
    device.get_attribute("State", "DISABLE")
    device.get_attribute("HealthState", "UNKNOWN")
    device.get_attribute("AdminMode", "OFFLINE")
    device.get_attribute("ObsState", "IDLE")
    module_logger.info("Initialization OK!!")


def check_controller_init_state(device):
    """Check initial state of the pss controller."""
    device.get_attribute("State", "DISABLE")
    device.get_attribute("HealthState", "UNKNOWN")
    device.get_attribute("AdminMode", "OFFLINE")
    module_logger.info("Initialization OK!!")


def check_subarray_init_state(device):
    """Check initial state of the pss subarray."""
    device.get_attribute("State", "DISABLE")
    device.get_attribute("HealthState", "UNKNOWN")
    device.get_attribute("AdminMode", "OFFLINE")
    module_logger.info("Initialization OK!!")


@given(
    parsers.parse(
        "PipelineCtrlDevice{number} is fresh initialized and connected"
    )
)
def fresh_and_connected_pipeline(number, all_pipelines):
    """Initialize fresh pipeline and connect to it."""
    fresh_new_pipeline(number, all_pipelines)
    for device in all_pipelines.values():
        device_name = device.return_device_name()
        # re-enable the device
        device.disable_device(False)
        module_logger.info(f"Connecting {device_name} to Cheetah")
        device.get_attribute("State", "ON")
        device.get_attribute("HealthState", "OK")
        device.get_attribute("AdminMode", "ONLINE")
        device.get_attribute("ObsState", "IDLE")
        module_logger.info(f"Check on {device_name} passed with success!")


@given(parsers.parse("{device_name} set {attribute} to {value}"))
@when(parsers.parse("{device_name} set {attribute} to {value}"))
def set_attribute_of_device(
    device_name, attribute, value, all_pipelines, all_pss_devices
):
    """Set attribute of the device."""
    all_devices = {**all_pss_devices, **all_pipelines}
    device = all_devices[device_name]
    module_logger.info(
        f"Setting {attribute} to {value} for device {device_name}"
    )
    device.set_attribute(attribute, value)


# @when(parsers.parse("PipelineCtrlDevice{number} AdminMode is set to ONLINE"))
# def set_admin_mode(device_name, all_pipelines):
#     """Set admin mode."""
#     device = all_pipelines[device_name]
#     device.disable_device(False)
#     module_logger.info(f"Connecting {device_name} to Cheetah")
@given(
    parsers.parse("{device_name} runs command {command_name} (no argument)")
)
@when(parsers.parse("{device_name} runs command {command_name} (no argument)"))
def send_command(command_name, device_name, all_pipelines, all_pss_devices):
    """Send command to the pss device."""
    all_devices = {**all_pss_devices, **all_pipelines}
    device = all_devices[device_name]
    device.send_command(command_name)


@given(
    parsers.parse(
        "{device_name} runs command {command_name} with argument {argument}"
    )
)
@when(
    parsers.parse(
        "{device_name} runs command {command_name} with argument {argument}"
    )
)
def send_command_with_argument(
    command_name,
    device_name,
    argument,
    all_pipelines,
    all_pss_devices,
):
    """Send command with argument to the pss device."""
    all_devices = {**all_pss_devices, **all_pipelines}
    device = all_devices[device_name]
    device.send_command_with_argument(command_name, argument)


@given(
    parsers.parse(
        "{device_name} runs command {command_name} "
        "(argument from file: {filename})"
    )
)
@when(
    parsers.parse(
        "{device_name} runs command {command_name} "
        "(argument from file: {filename})"
    )
)
def send_command_with_argument_from_file(
    command_name,
    device_name,
    filename,
    all_pipelines,
    all_pss_devices,
):
    """Send command with argument from file to the pss devices."""
    all_devices = {**all_pss_devices, **all_pipelines}
    device = all_devices[device_name]
    device.send_command_with_argument_from_file(command_name, filename)


# @given(parsers.parse("{device_name} {attribute} is {value}"))
@then(parsers.parse("{device_name} {attribute} is {value}"))
@when(parsers.parse("{device_name} {attribute} is {value}"))
def attribute_of_device(
    device_name, attribute, value, all_pipelines, all_pss_devices
):
    """Get attribute of the device."""
    module_logger.info(
        f"Reading attribute {attribute} on device {device_name}"
    )
    if "PipelineCtrlDevice" in device_name:
        device = all_pipelines[device_name]
    else:
        device = all_pss_devices[device_name]
    device.get_attribute(attribute, value)
    module_logger.info(f"Checked! {attribute} is {value} on {device_name}")
