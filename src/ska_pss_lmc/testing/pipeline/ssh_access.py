# -*- coding: utf-8 -*-
#
# Ported from the SKA Low MCCS project
#
#
# Distributed under the terms of the BSD 3-clause new license.
# See LICENSE for more info.
"""Patched SSH Access"""

import time
from itertools import cycle

# pylint: disable=unused-argument

log_snippet = [
    "[log][tid=140424180166400]"
    "[/home/baffa/src/ska/cheetah/cheetah/.."
    "/cheetah/pipeline/detail/BeamLauncher.cpp:148][1602501622]Creating"
    " Beams....",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/.."
    "/cheetah/fldo/detail/Fldo.cpp:44][1602501622]fldo::"
    " CUDA algorithm activated",
    "[warn][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/.."
    "/cheetah/tdas/detail/Tdas.cpp:76][1602501622]No"
    " Time Domain Accelerated Search algorithm has been specified",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/.."
    "/cheetah/pipeline/detail/BeamLauncher.cpp:171][1602501622]Finished"
    " creating pipelines",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/.."
    "/cheetah/pipeline/detail/BeamLauncher.cpp:223][1602501622]Starting"
    " Beam: identifier to distinguish between other similar type blocks",
    "[log][tid=140424071128832][/home/baffa/src/ska/cheetah/cheetah/.."
    "/cheetah/sps/detail/Sps.cpp:110][1602501622]setting"
    " dedispersion buffer size to 2048 spectra",
    "[log][tid=140424071128832][/home/baffa/src/ska/cheetah/cheetah/.."
    "/cheetah/sps/detail/Sps.cpp:113][1602501622]setting"
    " buffer overlap to 1514 spectra",
    "[log][tid=140424071128832][/home/baffa/src/ska/cheetah/cheetah/"
    "sigproc/src/SigProcFileStream.cpp:334][1602501626]resizing"
    " to 1024",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/.."
    "/cheetah/pipeline/detail/BeamLauncher.cpp:90][1602501626]Stopping"
    " Beam: identifier to distinguish between other similar type blocks",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/.."
    "/cheetah/pipeline/detail/BeamLauncher.cpp:264][1602501626]threads"
    " joined",
]

log_snippet_with_error = [
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/pipeline/detail/BeamLauncher.cpp:148][1602501622]Creating"
    " Beams....",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/fldo/detail/Fldo.cpp:44][1602501622]fldo::"
    " CUDA algorithm activated",
    "[warn][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/tdas/detail/Tdas.cpp:76][1602501622]No"
    " Time Domain Accelerated Search algorithm has been specified",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/pipeline/detail/BeamLauncher.cpp:171][1602501622]Finished"
    " creating pipelines",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/pipeline/detail/BeamLauncher.cpp:223][1602501622]Starting"
    " Beam: identifier to distinguish between other similar type blocks",
    "[error][tid=140424071128832][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/sps/detail/Sps.cpp:110][1602501622]setting"
    " dedispersion buffer size to 2048 spectra",
    "[log][tid=140424071128832][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/sps/detail/Sps.cpp:113][1602501622]setting"
    " buffer overlap to 1514 spectra",
    "[log][tid=140424071128832][/home/baffa/src/ska/cheetah/cheetah/sigproc/"
    "src/SigProcFileStream.cpp:334][1602501626]resizing"
    " to 1024",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/../"
    "cheetah/pipeline/detail/BeamLauncher.cpp:90][1602501626]Stopping"
    " Beam: identifier to distinguish between other similar type blocks",
    "[log][tid=140424180166400][/home/baffa/src/ska/cheetah/cheetah/"
    "../cheetah/tdas/detail/Tdas.cpp:76][1602501622]No",
]


class SshAccessPatched:
    """
    Mock class for the communication channel with the cheetah pipeline.
    """

    def __init__(self):
        self._pid = -1
        self._is_running = False
        self.log_data = log_snippet

    def set_log_data(self, input_data):
        self.log_data = input_data

    def is_running(self):
        return self._is_running

    def connect(self):
        return True

    def kill(self):
        self._pid = None
        self._is_running = False

    def disconnect(self):
        return True

    def start(self, cmd):
        self._pid = 8081
        self._is_running = True

    def write_config(self, file):
        return True

    def delete_config(self):
        pass

    @property
    def cheetah_pid(self):
        return self._pid

    def get_logs(self):
        logs = cycle(log_snippet)
        for line in logs:
            yield line
            if not self._is_running:
                break
            time.sleep(0.5)

    def mock_get_logs_with_error(self):
        self.set_log_data(log_snippet_with_error)
        logs = cycle(self.log_data)
        for line in logs:
            yield line
            if not self._is_running:
                break
            time.sleep(0.5)
