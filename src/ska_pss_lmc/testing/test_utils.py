# -*- coding: utf-8 -*-
#
# This file is part of the SKA PSS LMC project
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

"""This module implements miscellaneous test utils and helpers."""
import json
import logging
import os

module_logger = logging.getLogger(__name__)

# pylint: disable=logging-fstring-interpolation


def load_json_file(
    filename,
    string=False,
    remove=None,
):
    """Return the config dictionary or string given an input file with a json
    script."""
    file_to_load = os.getcwd() + "/tests/test_data/" + filename
    try:
        with open(file_to_load, encoding="UTF-8") as json_file:
            configuration_dict = json.loads(json_file.read().replace("\n", ""))
    except FileNotFoundError:
        # this is a patch to don't let integration tests fail in collection
        configuration_dict = {}
    if remove:
        if not isinstance(remove, list):
            raise Exception("'remove' is not a list")
        for item in remove:
            module_logger.warning(f"Removing {item}")
            configuration_dict.pop(item)

    output = configuration_dict
    if string:
        output = json.dumps(configuration_dict)
    return output
