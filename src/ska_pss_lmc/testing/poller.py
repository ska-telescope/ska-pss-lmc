"""This module provides polling utilities for testing-"""
import logging
import time

import numpy
from tango import DeviceProxy, DevState

from ska_pss_lmc.device_proxy import PssDeviceProxy

module_logger = logging.getLogger(__name__)

# pylint: disable=too-few-public-methods
# pylint: disable=logging-fstring-interpolation
# pylint: disable=redefined-builtin
# pylint: disable=redefined-outer-name


class Timeout:
    """Internal class to define a timeout check."""

    def __init__(self, duration):
        self.endtime = time.time() + duration

    def has_expired(self):
        """Check timeout."""
        return time.time() > self.endtime


class Probe:
    """Helper class that perform the attributes/command
    status/result verification"""

    def __init__(self, proxy, attr_name, expected_state, message):
        """"""
        self.proxy = proxy
        self.attr_name = attr_name
        self.expected_state = expected_state
        self.message = message
        self.current_state = DevState.DISABLE

    def get_attribute(self):
        """Get the attribute name"""
        return self.attr_name

    def sample(self):
        """extract the state of client and store it."""
        if self.proxy:
            if isinstance(self.proxy, (DeviceProxy, PssDeviceProxy)):
                device_attr = self.proxy.read_attribute(self.attr_name)
                self.current_state = device_attr.value
            else:
                self.current_state = getattr(self.proxy, self.attr_name)
        else:
            self.current_state = self.attr_name

    def is_satisfied(self):
        """Check if the state satisfies this test condition."""
        if isinstance(self.current_state, numpy.ndarray):
            return (self.expected_state == self.current_state).all()
        if isinstance(self.current_state, tuple):
            # Special case for longRunningCommandStatus
            if "longRunningCommandStatus".lower() in self.attr_name.lower():
                # Only compare the second element if expected_state
                # is a tuple with wildcard
                if (
                    isinstance(self.expected_state, tuple)
                    and len(self.expected_state) == 2
                    and self.expected_state[0] == "*"
                ):
                    return self.current_state[1] == self.expected_state[1]
            return self.expected_state == self.current_state
        return self.expected_state == self.current_state


class Poller:
    """Class that allows to poll the status of an attribute, command ..."""

    def __init__(self, timeout, interval):
        self.timeout = timeout
        self.interval = interval

    def check(self, probe: Probe):
        """Repeatedly check if the probe is satisfied.

        Assert false when the timeout expires.
        """
        timer = Timeout(self.timeout)
        probe.sample()
        while not probe.is_satisfied():
            if timer.has_expired():
                module_logger.debug(
                    f"Check Timeout on:{probe.get_attribute()}"
                )

                return False
            time.sleep(self.interval)
            probe.sample()
        module_logger.debug(f"Check success on: {probe.get_attribute()}")
        return True


def probe_poller(object, attribute, expected_value, time, interval=0.1):
    """_summary_

    :param object: the instance
    :type object: class or DeviceProxy
    :param attribute: the attribute name
    :type attribute: str
    :param expected_value: the exptected attribute value
    :type expected_value: any
    :param time: the time to poll
    :type time: float
    :param interval: polling time: defaults to 0.1.
    :type time: float
    """

    message = f"{attribute} is not {expected_value}. It is "

    probe = Probe(object, attribute, expected_value, message)
    status = Poller(time, interval).check(probe)

    if (
        # isinstance(object, tango.DeviceProxy)
        # and isinstance(expected_value, tango._tango.DevState)
        # and
        attribute.lower()
        == "state"
    ):
        message += f"{getattr(object, attribute)()}"
    else:
        message += f"{getattr(object, attribute)}"

    if not status:
        assert status, message

    assert status
